package geca.dareliconsulting.smarttoutch.com.geca.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Utils;
import geca.dareliconsulting.smarttoutch.com.geca.view.ScaleSmartImageView;

public class FullScreenPictureActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_picture);

        String url = getIntent().getStringExtra("url");
        if (url != null) {
            ScaleSmartImageView imageView = (ScaleSmartImageView)findViewById(R.id.imageView);
            imageView.setImageBitmap(Utils.loadSmallBitmapFromFile(url));
        }
    }

    public void backAction(View view) {
        finish();
    }
}
