package geca.dareliconsulting.smarttoutch.com.geca.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.viewpagerindicator.CirclePageIndicator;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.adapter.IntroPagerAdapter;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Constants;

public class IntroActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new IntroPagerAdapter(this));
        CirclePageIndicator indicator = (CirclePageIndicator)findViewById(R.id.indicator);
        final float density = getResources().getDisplayMetrics().density;
        indicator.setRadius(5 * density);
        indicator.setPageColor(Color.GRAY);
        indicator.setFillColor(Color.RED);
        indicator.setViewPager(viewPager);
    }

    public void contactAction(View view) {
        Intent mailer = new Intent(Intent.ACTION_SEND);
        mailer.setType("text/plain");
        mailer.putExtra(Intent.EXTRA_EMAIL, new String[]{"contact@coppelis.com"});
        startActivity(Intent.createChooser(mailer, getString(R.string.send_email_using)));
    }

    public void skipAction (View view) {
        SharedPreferences prefs = getSharedPreferences(Constants.SHARED_PREFS_NAME, MODE_PRIVATE);
        Boolean isConnected = prefs.getBoolean("isConnected", false);
        if(isConnected) {
            startActivity(new Intent(this, MainActivity.class));
        }
        else {
            startActivity(new Intent(this, LoginActivity.class));
        }
        finish();
    }
}
