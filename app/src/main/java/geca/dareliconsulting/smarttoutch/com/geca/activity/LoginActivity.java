package geca.dareliconsulting.smarttoutch.com.geca.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Constants;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Utils;
/*
 the Activity Authentificate of the User Auditor s
 */

public class LoginActivity extends Activity implements Utils.OnDataSystemLoadedListener, Utils.OnTechnicianLoadedListener {

    EditText iUserName;
    EditText iPassword;
    ProgressDialog progressDialog;
    SharedPreferences sharedPreferences;
    Switch vpnSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        iUserName = ((EditText) this.findViewById(R.id.txt_email_login));
        iPassword = ((EditText) this.findViewById(R.id.txt_password_login));
        sharedPreferences = getSharedPreferences(Constants.SHARED_PREFS_NAME, MODE_PRIVATE);
        vpnSwitch = (Switch)findViewById(R.id.switchVPN);
        vpnSwitch.setChecked(Constants.isSVNEnabled(this));
        vpnSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //Constants.enableDisableSVN(LoginActivity.this, isChecked);
            }
        });
    }

    void ipDialog(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("IP");
        alertDialog.setMessage("Entrer IP:");

        final EditText input = new EditText(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        alertDialog.setView(input);
        alertDialog.setPositiveButton(getResources().getString(R.string.login),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Constants.SERVER_HOST= "http://"+input.getText().toString();
                        login();
                        dialog.cancel();

                    }
                });

        alertDialog.setNegativeButton(getResources().getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
    }
    public void loginAction(View v) {
        final String username = iUserName.getText().toString().trim();
        final String password = iPassword.getText().toString().trim();
        if (username.isEmpty()) {
            Toast.makeText(LoginActivity.this, this.getString(R.string.entrerLogin), Toast.LENGTH_SHORT).show();
            return;
        }

        if (password.isEmpty()) {
            Toast.makeText(LoginActivity.this, this.getString(R.string.entrerMDP), Toast.LENGTH_SHORT).show();
            return;
        }
        if (getSharedPreferences(Constants.SHARED_PREFS_NAME, MODE_PRIVATE).getString("username", "").equals(username) && getSharedPreferences(Constants.SHARED_PREFS_NAME, MODE_PRIVATE).getString("password", "").equals(password)) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        } else {
           // ipDialog();
            login();
        }
    }

    void login(){
        final String username = iUserName.getText().toString().trim();
        final String password = iPassword.getText().toString().trim();
        progressDialog = ProgressDialog.show(LoginActivity.this, getString(R.string.msgAttente), getString(R.string.msgConnexion), true);
        progressDialog.setCancelable(false);
        RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
        String url = Constants.API_LOGIN_URL(this) + "?username=" + username + "&password=" + password;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            Log.i("login  Success", "========>" + response);
                            JSONObject root = new JSONObject(response);
                            String accessToken = root.getString("access_token");
                            String refreshToken = root.getString("refresh_token");
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("access_token", accessToken);
                            editor.putString("refresh_token", refreshToken);
                            editor.putString("username", username);
                            editor.putBoolean("isConnected", true);
                            editor.putString("password", password);
                            editor.apply();
                            getUserByToken(accessToken,username);
                        } catch (JSONException e) {
                            Log.e(Constants.TAG, "Parsing exception : " + e);
                            Toast.makeText(LoginActivity.this, getString(R.string.echecConnexion), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(LoginActivity.this, getString(R.string.echecConnexionI), Toast.LENGTH_SHORT).show();
                    }
                }
        );
        queue.add(stringRequest);
    }
    void loadDataSystem() {
        progressDialog = ProgressDialog.show(LoginActivity.this, getString(R.string.msgAttente), getString(R.string.msgRecuperationEnCours), true);
        progressDialog.setCancelable(false);
        Utils.loadDataSytsem(this, this);
    }

    void loadTechnicien() {
        Utils.loadTechnicians(this, this);
    }

    @Override
    public void dataSystemLoadingCompleted(boolean status) {
        if (status) {
            loadTechnicien();
        } else {
            progressDialog.dismiss();
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("user_id", "");
            editor.putString("access_token", "");
            editor.putString("refresh_token", "");
            editor.putString("username", "");
            editor.putString("password", "");
            editor.apply();
            Toast.makeText(LoginActivity.this, getString(R.string.echecRecuperation), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void technicianLoadingCompleted(boolean success) {
        progressDialog.dismiss();
        if (success) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        } else {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("user_id", "");
            editor.putString("access_token", "");
            editor.putString("refresh_token", "");
            editor.putString("username", "");
            editor.putString("password", "");
            editor.apply();
            Toast.makeText(LoginActivity.this, getString(R.string.echecRecuperation), Toast.LENGTH_SHORT).show();
        }
    }

    public void getUserByToken(final String access_token, final String username) {
        RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
        String url = Constants.API_BASE_URL(this) + "load_user_by_username?username=" + username+"&access_token="+access_token;
        Log.d(Constants.TAG, "url : " + url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.i(Constants.TAG, "user " + response);
                        try {
                            JSONObject root = new JSONObject(response);
                            String id = root.getString("id");
                            String username = root.getString("username");
                            String firstname = root.getString("name");
                            String lastname = root.getString("surname");
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("user_id", id);
                            editor.putString("username", username);
                            editor.putString("firstname", firstname);
                            editor.putString("lastname", lastname);
                            editor.apply();
                            loadDataSystem();
                        } catch (JSONException e) {
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("user_id", "");
                            editor.putString("access_token", "");
                            editor.putString("refresh_token", "");
                            editor.putString("username", "");
                            editor.putString("password", "");
                            editor.putString("firstname", "");
                            editor.putString("lastname", "");
                            editor.apply();
                            Log.e(Constants.TAG, "Parsing exception : " + e);
                            Toast.makeText(LoginActivity.this, getString(R.string.echecConnexion), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("user_id", "");
                editor.putString("access_token", "");
                editor.putString("refresh_token", "");
                editor.putString("username", "");
                editor.putString("password", "");
                editor.apply();
                Toast.makeText(LoginActivity.this, getString(R.string.echecConnexionI), Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(stringRequest);
    }
}
