package geca.dareliconsulting.smarttoutch.com.geca.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Constants;
import geca.dareliconsulting.smarttoutch.com.geca.utils.ServiceGPS;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewGroup content = (ViewGroup) findViewById(R.id.frame_container);
        getLayoutInflater().inflate(R.layout.activity_main, content, true);
        displayView(1);
        requestPermission(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ServiceGPS serviceGPS = new ServiceGPS(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
            if (resultCode == Activity.RESULT_OK) {
                ArrayList<String> mSelectPath = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);

                if (mSelectPath != null && mSelectPath.size() > 0) {
                    String p = mSelectPath.get(0);
                    Intent intent;
                    if (requestCode == Constants.CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_CERTIF) {
                        intent = new Intent(Constants.CERTIF_PHOTO_RECEIVER);
                        intent.putExtra("photo", p);
                        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                    }
                    if (requestCode == Constants.CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_VISITE_TECHNIQUE) {
                        intent = new Intent(Constants.VISITE_TECHNIQUE_PHOTO_RECEIVER);
                        intent.putExtra("photo", p);
                        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                    }
                    if (requestCode == Constants.CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_RECOVERY) {
                        intent = new Intent(Constants.RECOVERY_PHOTO_RECEIVER);
                        intent.putExtra("photo1", p);
                        if(mSelectPath.size()>1) {
                            String p1 = mSelectPath.get(1);
                            intent.putExtra("photo2", p1);
                        }else
                            intent.putExtra("photo2", "");
                        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                    }

                }

            }

    }


    @Override
    public void onBackPressed() {

    }

    private static void requestPermission(final Context context){
        // Resuming the periodic location updates
        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                // Provide an additional rationale to the user if the permission was not granted
                // and the user would benefit from additional context for the use of the permission.
                // For example if the user has previously denied the permission.

                new AlertDialog.Builder(context)
                        .setMessage("Une permission pour stocker des données dans votre appareil est requise.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions((Activity) context,
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        1);
                            }
                        }).show();

            } else {
                // permission has not been granted yet. Request it directly.
                ActivityCompat.requestPermissions((Activity) context,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1);
            }
        }

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.ACCESS_FINE_LOCATION)
                    || ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                // Provide an additional rationale to the user if the permission was not granted
                // and the user would benefit from additional context for the use of the permission.
                // For example if the user has previously denied the permission.

                new AlertDialog.Builder(context)
                        .setMessage("Une permission pour avoir vos données GPS est requise.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions((Activity) context,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},
                                        3);
                            }
                        }).show();

            } else {
                // permission has not been granted yet. Request it directly.
                ActivityCompat.requestPermissions((Activity) context,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},
                        3);
            }
        }
    }

///...

    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this,
                            getResources().getString(R.string.permission_storage_success),
                            Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(this,
                            getResources().getString(R.string.permission_storage_failure),
                            Toast.LENGTH_SHORT).show();
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                }
                return;
            }
            case 3: {
                if (grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this,
                            getResources().getString(R.string.permission_location_success),
                            Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(this,
                            getResources().getString(R.string.permission_storage_failure),
                            Toast.LENGTH_SHORT).show();
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                }
                return;
            }
        }
    }
}
