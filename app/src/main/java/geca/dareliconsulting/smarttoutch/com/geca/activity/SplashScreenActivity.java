package geca.dareliconsulting.smarttoutch.com.geca.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Constants;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Utils;


public class SplashScreenActivity extends Activity {
    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        ((TextView)findViewById(R.id.textViewAppName)).setTypeface(Utils.getHelveticaFont(this));
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                SharedPreferences prefs = getSharedPreferences(Constants.SHARED_PREFS_NAME, MODE_PRIVATE);
                Boolean isConnected = prefs.getBoolean("isConnected", false);
                if(isConnected) {
                    //ipDialog();
                    try {
                        Utils.refreshDatasystem(SplashScreenActivity.this);
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                    startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
                    finish();
                }
                else {
                    startActivity(new Intent(SplashScreenActivity.this, IntroActivity.class));
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);

    }
    void ipDialog(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("IP");
        alertDialog.setMessage("Entrer IP:");

        final EditText input = new EditText(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        alertDialog.setView(input);
        alertDialog.setPositiveButton(getResources().getString(R.string.login),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Constants.SERVER_HOST = "http://" + input.getText().toString();

                        try {
                            Utils.refreshDatasystem(SplashScreenActivity.this);
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                        startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
                        finish();
                        dialog.cancel();

                    }
                });

        alertDialog.setNegativeButton(getResources().getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
    }
    @Override
    public void onBackPressed() {

    }

    public void websiteAction(View v) {
        String url = "http://www.dareli-consulting.com";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }
}
