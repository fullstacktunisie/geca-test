package geca.dareliconsulting.smarttoutch.com.geca.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.model.RequestTraining;

/**
 * Created by Smart on 22/05/2015.
 */
public class AccompagnementAdapter extends ArrayAdapter<RequestTraining> {

    private final Activity context;
    protected ArrayList<RequestTraining> listRequestTrainings;
    protected ArrayList<RequestTraining> filterRequestTrainings;

    public View view;

    public AccompagnementAdapter(Activity context, ArrayList<RequestTraining> mylist) {
        super(context, -1, mylist);
        this.context = context;
        this.listRequestTrainings = mylist;
        filterRequestTrainings = new ArrayList<>();
        this.filterRequestTrainings.addAll(mylist);

    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.requesttraining_list_row, null, true);

        TextView formateur = (TextView) rowView.findViewById(R.id.formateur);
        TextView period = (TextView) rowView.findViewById(R.id.period);
        TextView issynchrone = (TextView) rowView.findViewById(R.id.issynchrone);

        RequestTraining requestTraining = listRequestTrainings.get(position);
        formateur.setText(requestTraining.getNametrainer());
        period.setText(requestTraining.getDuration());

        if (requestTraining.isSynchron()) {
            issynchrone.setText("Oui");
        } else {
            issynchrone.setText("Non");
        }
        if (position % 2 == 1) {
            rowView.setBackgroundColor(context.getResources().getColor(R.color.background_actionbar));
        } else {
            rowView.setBackgroundColor(context.getResources().getColor(R.color.white));
        }
        return rowView;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        listRequestTrainings.clear();
        if (charText.length() == 0) {
            listRequestTrainings.addAll(filterRequestTrainings);
        } else {
            for (RequestTraining requestTraining : filterRequestTrainings) {
                if (requestTraining.getNametrainer().toLowerCase(Locale.getDefault()).contains(charText)) {
                    listRequestTrainings.add(requestTraining);
                } else if (requestTraining.getDuration().toLowerCase(Locale.getDefault()).contains(charText)) {
                    listRequestTrainings.add(requestTraining);
                } else if (charText.equals("Oui") && requestTraining.isSynchron()) {
                    listRequestTrainings.add(requestTraining);
                } else if (charText.equals("Non") && !requestTraining.isSynchron()) {
                    listRequestTrainings.add(requestTraining);
                }
            }
        }
        notifyDataSetChanged();
    }
}
