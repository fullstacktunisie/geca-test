package geca.dareliconsulting.smarttoutch.com.geca.adapter;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.pdf.PdfRenderer;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatRadioButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.fragment.PicturePaintDialogFragment;
import geca.dareliconsulting.smarttoutch.com.geca.model.Skill;
import geca.dareliconsulting.smarttoutch.com.geca.model.TypeSkill;
import geca.dareliconsulting.smarttoutch.com.geca.model.WorkUnit;
import geca.dareliconsulting.smarttoutch.com.geca.singleton.Singleton;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Constants;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Utils;
import geca.dareliconsulting.smarttoutch.com.geca.view.MultiSpinner;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;

/**
 * Created by Smart on 28/04/2015.
 */
public class CertifDataSetAdapter extends ArrayAdapter<Object> implements PicturePaintDialogFragment.OnPictureChangedListener, MultiSpinner.MultiSpinnerListener {


    private final Activity context;
    protected ArrayList<Object> dataset;
    private String[] listNotes;
    private WorkUnit currentCameraWorkunit;

    public CertifDataSetAdapter(Activity context, ArrayList<Object> dataset) {
        super(context, -1, dataset);
        this.context = context;
        this.dataset = dataset;
        this.currentCameraWorkunit = new WorkUnit();
        listNotes = context.getResources().getStringArray(R.array.list_work_unit_certif);
        if (Singleton.getInstance().mPhotoReceiverRecovery != null) {
            LocalBroadcastManager.getInstance(context).unregisterReceiver(Singleton.getInstance().mPhotoReceiverRecovery);
        }
        Singleton.getInstance().mPhotoReceiverRecovery = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // Extract data included in the Intent
                String photo1 = intent.getStringExtra("photo1");
                String photo2 = intent.getStringExtra("photo2");
                Log.e("receiver recovery", "Got photo: " + photo1 + " " + photo2);
                try {
                    currentCameraWorkunit.getAnswer().setRecoverypictures("");
                    currentCameraWorkunit.getAnswer().addRecoverypicture(Utils.saveBitmapToFile(photo1));
                    if (!photo2.equals(""))
                        currentCameraWorkunit.getAnswer().addRecoverypicture(Utils.saveBitmapToFile(photo2));
                    if (currentCameraWorkunit.getAnswer().getRecoverypictures().size() == 2) {
                        currentCameraWorkunit.getAnswer().setNowRecovery(true);
                        currentCameraWorkunit.getAnswer().setHasRecovery(false);
                    } else {
                        currentCameraWorkunit.getAnswer().setNowRecovery(false);
                        currentCameraWorkunit.getAnswer().setHasRecovery(false);
                    }
                } catch (Exception e) {
                }
                notifyDataSetChanged();
            }
        };
        if (Singleton.getInstance().mPhotoReceiver != null) {
            LocalBroadcastManager.getInstance(context).unregisterReceiver(Singleton.getInstance().mPhotoReceiver);
        }
        Singleton.getInstance().mPhotoReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // Extract data included in the Intent
                String photo = intent.getStringExtra("photo");
                Log.e("receiver", "Got photo: " + photo);
                try {
                    currentCameraWorkunit.getAnswer().setPicture(Utils.saveBitmapToFile(photo));

                } catch (Exception e) {
                }
                notifyDataSetChanged();
            }
        };
        LocalBroadcastManager.getInstance(context).registerReceiver(Singleton.getInstance().mPhotoReceiver, new IntentFilter(Constants.CERTIF_PHOTO_RECEIVER));
        LocalBroadcastManager.getInstance(context).registerReceiver(Singleton.getInstance().mPhotoReceiverRecovery, new IntentFilter(Constants.RECOVERY_PHOTO_RECEIVER));
    }


    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View convertView = new View(context);

        if (dataset.get(position) instanceof TypeSkill) {
            TypeSkill item = (TypeSkill) dataset.get(position);
            convertView = inflater.inflate(R.layout.rowtypeskills, null, true);

            TextView text = (TextView) convertView.findViewById(R.id.nametypeskill);
            text.setText(item.getTypeSkillLabel());

            ImageView collapse = (ImageView) convertView.findViewById(R.id.collapse);
            if (item.isToCollapse())
                collapse.setImageResource(R.drawable.expand);
            else
                collapse.setImageResource(R.drawable.collapse);

            if (!item.isNotApplicable()) {
                collapse.setTag(item);
                collapse.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TypeSkill item = ((TypeSkill) v.getTag());
                        boolean collapse = item.isToCollapse();
                        item.setToCollapse(!collapse);
                        for (Skill skill : item.getSkills()) {
                            skill.setTypeToCollapse(!collapse);
                            for (WorkUnit workunit : skill.getWorkUnits()) {
                                workunit.setToCollapse(!collapse);
                            }
                        }
                        notifyDataSetChanged();
                    }
                });
            }

            Switch sIsnotApply = (Switch) convertView.findViewById(R.id.noapplicable);
            sIsnotApply.setTag(item);
            sIsnotApply.setChecked(!item.isNotApplicable());
            sIsnotApply.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    ((TypeSkill) buttonView.getTag()).setNotApplicable(!isChecked);
                    for (Skill skill : ((TypeSkill) buttonView.getTag()).getSkills()) {
                        skill.setNotApplicable(!isChecked);
                        skill.setTypeSkillNotApplicable(!isChecked);
                        for (WorkUnit workunit : skill.getWorkUnits()) {
                            workunit.setNotApplicable(!isChecked);
                            if (workunit.getAnswer() != null) {
                                workunit.getAnswer().setTypeSkillNotApplicable(!isChecked);
                                workunit.getAnswer().setSkillNotApplicable(!isChecked);
                            }
                        }
                    }
                    notifyDataSetChanged();
                }
            });
            Switch iSwithAlert = (Switch) convertView.findViewById(R.id.alertsecurity);
            iSwithAlert.setTag(item);
            iSwithAlert.setChecked(item.isWithAlertSecurity());
            iSwithAlert.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        ((TypeSkill) buttonView.getTag()).setWithAlertSecurity(true);
                    } else {
                        ((TypeSkill) buttonView.getTag()).setWithAlertSecurity(false);
                    }
                }
            });
        } else if (dataset.get(position) instanceof Skill) {
            final Skill item = (Skill) dataset.get(position);
            if (item.isTypeSkillNotApplicable() || item.isTypeToCollapse()) {
                convertView = inflater.inflate(R.layout.rownull, null, false);
                item.setScore(-1);
            } else {
                convertView = inflater.inflate(R.layout.rowskill, null, false);
                ImageView collapse = (ImageView) convertView.findViewById(R.id.collapse);
                if (item.isToCollapse())
                    collapse.setImageResource(R.drawable.expand);
                else
                    collapse.setImageResource(R.drawable.collapse);
                if (!item.isNotApplicable()) {
                    Log.e("collapse", "applicable");
                    collapse.setTag(item);
                    collapse.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Skill item = ((Skill) v.getTag());
                            item.setToCollapse(!item.isToCollapse());
                            for (WorkUnit workunit : item.getWorkUnits()) {
                                workunit.setToCollapse(item.isToCollapse());
                            }
                            notifyDataSetChanged();
                        }
                    });
                }
                final Switch sIsnotApply = (Switch) convertView.findViewById(R.id.skillnotapply);
                if (item.isTypeSkillNotApplicable()) {
                    convertView.findViewById(R.id.transparentView).setVisibility(View.VISIBLE);
                    sIsnotApply.setEnabled(false);
                    sIsnotApply.setFocusable(false);
                    sIsnotApply.setClickable(false);
                } else {
                    convertView.findViewById(R.id.transparentView).setVisibility(View.GONE);
                }
                Utils.enableDisableView(convertView, !item.isTypeSkillNotApplicable());
                TextView text = (TextView) convertView.findViewById(R.id.skillname);

                sIsnotApply.setTag(item);
                sIsnotApply.setChecked(!item.isNotApplicable());
                sIsnotApply.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        ((Skill) buttonView.getTag()).setNotApplicable(!isChecked);
                        for (WorkUnit workunit : ((Skill) buttonView.getTag()).getWorkUnits()) {
                            workunit.setNotApplicable(!isChecked);
                            if (workunit.getAnswer() != null)
                                workunit.getAnswer().setSkillNotApplicable(!isChecked);
                        }
                        notifyDataSetChanged();
                    }
                });
                Spinner spinnerNote = (Spinner) convertView.findViewById(R.id.spinnerViewNote);
                convertView.findViewById(R.id.textViewNote).setVisibility(View.GONE);
                convertView.findViewById(R.id.note).setVisibility(View.GONE);
                convertView.findViewById(R.id.spinnerholder).setVisibility(View.VISIBLE);

                String[] listSpin = context.getResources().getStringArray(R.array.list_spin);
                ArrayAdapter<String> adapterNotes = new ArrayAdapter<String>(context, R.layout.spinner_item, listSpin);
                spinnerNote.setAdapter(adapterNotes);
                if (item.getScore() != -1) {
                    spinnerNote.setSelection(item.getScore()+1);
                }

                spinnerNote.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        if (sIsnotApply.isChecked())
                            item.setScore(i - 1);
                        else
                            item.setScore(-1);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                text.setText(item.getLabel());
               /* int score = 0;
                if (item.getWorkUnits().size() > 0) {
                    boolean firstScore = false;
                    for (WorkUnit workUnit : item.getWorkUnits()) {
                        if (workUnit.isCertification() && workUnit.getAnswer() != null && !firstScore) {
                            score = workUnit.getAnswer().getScore();
                            firstScore = true;
                        } else if (workUnit.isCertification() && workUnit.getAnswer() != null && workUnit.getAnswer().getScore() != 0)
                            score = Math.min(score, workUnit.getAnswer().getScore());
                    }
                }
                if (score == 0) {
                    textNote.setText(listNotes[score]);
                    textNote.setBackgroundResource(R.color.gray);
                } else if (score == 1) {
                    textNote.setText(listNotes[score]);
                    textNote.setBackgroundResource(R.color.wrong);
                } else if (score == 2) {
                    textNote.setText(listNotes[score]);
                    textNote.setBackgroundResource(R.color.green);
                } else {
                    textNote.setText(listNotes[score]);
                    textNote.setBackgroundResource(R.color.blue);
                }
                textNote.setText(listNotes[score]);*/
            }
        } else if (dataset.get(position) instanceof WorkUnit) {
            final WorkUnit item = (WorkUnit) dataset.get(position);
            Log.e("collapse", "workunit" + item.isToCollapse());
            if (item.isNotApplicable() || item.isToCollapse()) {
                convertView = inflater.inflate(R.layout.rownull, null, false);
            } else {
                convertView = inflater.inflate(R.layout.rowworkunit, null, true);
                convertView.findViewById(R.id.certification).setVisibility(View.GONE);

                TextView edittextworkunitcomment = ((EditText) convertView.findViewById(R.id.edittextworkunitcomment));
                TextView impactType = (TextView) convertView.findViewById(R.id.impactType);
                TextView impactTypeText = (TextView) convertView.findViewById(R.id.impactTypeText);
                impactTypeText.setVisibility(View.GONE);
                impactType.setVisibility(View.GONE);
                impactType.setText(item.getImpactType());
                RadioGroup radioRecoveryGroup = (RadioGroup) convertView.findViewById(R.id.radioRecovery);
                radioRecoveryGroup.setVisibility(View.GONE);
                final Spinner spinnerNotes = (Spinner) convertView.findViewById(R.id.spinner_typenote);
                ImageButton imageButton = (ImageButton) convertView.findViewById(R.id.imageButtonCamera);
                ImageView imageRecovery1 = (ImageView) convertView.findViewById(R.id.imgBefore);
                ImageView imageRecovery2 = (ImageView) convertView.findViewById(R.id.imgAfter);
                imageRecovery1.setVisibility(View.GONE);
                imageRecovery2.setVisibility(View.GONE);
                final MultiSpinner actionPlan = (MultiSpinner) convertView.findViewById(R.id.multispinner);
                AppCompatCheckBox fitCheck = (AppCompatCheckBox) convertView.findViewById(R.id.fit);
                fitCheck.setVisibility(View.GONE);
                TextView error = (TextView) convertView.findViewById(R.id.error);
                if (item.getAnswer() != null && item.getAnswer().isError())
                    error.setVisibility(View.VISIBLE);
                else
                    error.setVisibility(View.GONE);

                if (item.isNotApplicable()) {
                    convertView.findViewById(R.id.transparentView).setVisibility(View.VISIBLE);
                    edittextworkunitcomment.setEnabled(false);
                    edittextworkunitcomment.setFocusable(false);

                    imageButton.setEnabled(false);
                    imageButton.setFocusable(false);
                    imageButton.setClickable(false);

                    radioRecoveryGroup.setEnabled(false);
                    radioRecoveryGroup.setClickable(false);
                    radioRecoveryGroup.setFocusable(false);

                    spinnerNotes.setFocusable(false);
                    spinnerNotes.setEnabled(false);
                    spinnerNotes.setClickable(false);

                    actionPlan.setFocusable(false);
                    actionPlan.setEnabled(false);
                    actionPlan.setClickable(false);

                    convertView.setClickable(false);
                } else {
                    convertView.findViewById(R.id.transparentView).setVisibility(View.GONE);
                }
                Utils.enableDisableView(convertView, !item.isNotApplicable());
                TextView text = (TextView) convertView.findViewById(R.id.textviewworkuniteDescription);
                text.setText(item.getLabel());

                //FLAG impossibilité technique
                if (item.getAnswer() != null) {
                    fitCheck.setChecked(item.getAnswer().isFit());
                    fitCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if (item.getAnswer() != null)
                                item.getAnswer().setFit(isChecked);
                        }
                    });

                    ArrayAdapter<String> adapterNotes = new ArrayAdapter<String>(context, R.layout.spinner_item, listNotes);
                    spinnerNotes.setAdapter(adapterNotes);
                    spinnerNotes.setSelection(item.getAnswer().getScore());
                    spinnerNotes.post(new Runnable() {
                        public void run() {
                            spinnerNotes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    item.getAnswer().setScore(position);
                                    if (position == 1)
                                        Toast.makeText(context, context.getString(R.string.selectOneAndComment), Toast.LENGTH_LONG).show();
                                    notifyDataSetChanged();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                        }
                    });
                }
                //Test on score
                LinearLayout allWU = (LinearLayout) convertView.findViewById(R.id.allWorkUnit);
                if (item.getAnswer() != null && item.getAnswer().getScore() != 1) {
                    allWU.setVisibility(View.GONE);
                } else if (item.getAnswer() != null) {
                    allWU.setVisibility(View.VISIBLE);
                    if (item.getAnswer().getActionPlans().size() > 0) {
                        StringBuffer spinnerBuffer = new StringBuffer();
                        //Action Plan
                        for (int i : item.getAnswer().getActionPlans()) {
                            spinnerBuffer.append(Singleton.getInstance().getActionPlans()[i]);
                            spinnerBuffer.append(", ");
                        }
                        // Remove trailing comma
                        if (spinnerBuffer.length() > 2) {
                            spinnerBuffer.setLength(spinnerBuffer.length() - 2);
                        }

                        // display new text
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                                android.R.layout.simple_spinner_item,
                                new String[]{spinnerBuffer.toString()});
                        actionPlan.setAdapter(adapter);
                    }
                    actionPlan.setMultiSpinnerListener(this, item);
                    convertView.findViewById(R.id.actionPlan).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            currentCameraWorkunit = item;
                        }
                    });
                    // comment text

                    edittextworkunitcomment.setText(item.getAnswer().getComment());
                    edittextworkunitcomment.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            item.getAnswer().setComment(s.toString());
                        }
                    });
                    //------------------------------
                    if (item.getAnswer().getRecoverypictures().size() != 0) {
                        imageRecovery1.setImageBitmap(Utils.loadSmallBitmapFromFile(item.getAnswer().getRecoverypictures().get(0)));
                        imageRecovery1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                currentCameraWorkunit = item;
                                if (currentCameraWorkunit.getAnswer().getRecoverypictures().size() != 0) {
                                    FragmentManager fm = context.getFragmentManager();
                                    PicturePaintDialogFragment picturePaintDialogFragment = PicturePaintDialogFragment.newInstance(currentCameraWorkunit.getAnswer().getRecoverypictures().get(0));
                                    picturePaintDialogFragment.onPictureChangedListener = CertifDataSetAdapter.this;
                                    picturePaintDialogFragment.setDeletevisibility();
                                    picturePaintDialogFragment.show(fm, "PicturePaintDialogFragment");
                                }
                            }
                        });
                        if (item.getAnswer().getRecoverypictures().size() > 1) {
                            imageRecovery2.setImageBitmap(Utils.loadSmallBitmapFromFile(item.getAnswer().getRecoverypictures().get(1)));
                            imageRecovery2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    currentCameraWorkunit = item;
                                    if (currentCameraWorkunit.getAnswer().getRecoverypictures().size() > 1) {
                                        FragmentManager fm = context.getFragmentManager();
                                        PicturePaintDialogFragment picturePaintDialogFragment = PicturePaintDialogFragment.newInstance(currentCameraWorkunit.getAnswer().getRecoverypictures().get(1));
                                        picturePaintDialogFragment.onPictureChangedListener = CertifDataSetAdapter.this;
                                        picturePaintDialogFragment.setDeletevisibility();
                                        picturePaintDialogFragment.show(fm, "PicturePaintDialogFragment");
                                    }
                                }
                            });
                        } else {
                            imageRecovery2.setVisibility(View.INVISIBLE);
                        }
                    }
                    if (item.getAnswer().isNowRecovery()) {
                        radioRecoveryGroup.check(R.id.radioNow);
                    } else if (item.getAnswer().isHasRecovery())
                        radioRecoveryGroup.check(R.id.radioAfter);
                    else
                        radioRecoveryGroup.check(R.id.radioNone);

                    if (!item.isPossibleRecovery()) {
                        radioRecoveryGroup.getChildAt(1).setClickable(false);
                        radioRecoveryGroup.getChildAt(1).setEnabled(false);
                        radioRecoveryGroup.getChildAt(1).setFocusable(false);
                        ((AppCompatRadioButton) radioRecoveryGroup.getChildAt(1)).setTextColor(context.getResources().getColor(R.color.gray));
                    }


                    if (item.getAnswer() != null && item.getAnswer().getScore() != 1) {
                        radioRecoveryGroup.getChildAt(1).setClickable(false);
                        radioRecoveryGroup.getChildAt(1).setEnabled(false);
                        radioRecoveryGroup.getChildAt(1).setFocusable(false);
                        ((AppCompatRadioButton) radioRecoveryGroup.getChildAt(1)).setTextColor(context.getResources().getColor(R.color.gray));
                        radioRecoveryGroup.getChildAt(2).setClickable(false);
                        radioRecoveryGroup.getChildAt(2).setEnabled(false);
                        radioRecoveryGroup.getChildAt(2).setFocusable(false);
                        ((AppCompatRadioButton) radioRecoveryGroup.getChildAt(2)).setTextColor(context.getResources().getColor(R.color.gray));
                    }

                    radioRecoveryGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                                                      @Override
                                                                      public void onCheckedChanged(RadioGroup group, int checkedId) {
                                                                          if (checkedId == R.id.radioNone) {
                                                                              item.getAnswer().setNowRecovery(false);
                                                                              item.getAnswer().setHasRecovery(false);
                                                                          } else if (checkedId == R.id.radioNow) {
                                                                              currentCameraWorkunit = item;
                                                                              int selectedMode = MultiImageSelectorActivity.MODE_MULTI;
                                                                              Intent intent = new Intent(context, MultiImageSelectorActivity.class);
                                                                              intent.putExtra(MultiImageSelectorActivity.EXTRA_SHOW_CAMERA, true);
                                                                              intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_MODE, selectedMode);
                                                                              intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_COUNT, 2);
                                                                              //Selected photo to put in the list
                                                                              if (item.getAnswer().getRecoverypictures().size() == 1)
                                                                                  intent.putExtra(MultiImageSelectorActivity.EXTRA_DEFAULT_SELECTED_LIST, item.getAnswer().getRecoverypictures());
                                                                              context.startActivityForResult(intent, Constants.CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_RECOVERY);
                                                                              group.check(R.id.radioNone);
                                                                          } else {
                                                                              item.getAnswer().setNowRecovery(false);
                                                                              item.getAnswer().setHasRecovery(true);
                                                                              Toast.makeText(context, context.getString(R.string.selectComment), Toast.LENGTH_LONG).show();
                                                                          }
                                                                      }
                                                                  }

                    );
                }

                //-------------------------------------------------
                if (item.getAnswer() != null) {
                    if (item.getAnswer().getPicture() != null && !item.getAnswer().getPicture().isEmpty()) {
                        imageButton.setImageBitmap(Utils.loadSmallBitmapFromFile(item.getAnswer().getPicture()));
                    } else {
                        imageButton.setImageResource(R.drawable.camera);
                    }
                    imageButton.setTag(item);
                    imageButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            currentCameraWorkunit = item;
                            if (currentCameraWorkunit.getAnswer().getPicture() != null) {
                                FragmentManager fm = context.getFragmentManager();
                                PicturePaintDialogFragment fullScreenPictureDialogFragment = PicturePaintDialogFragment.newInstance(currentCameraWorkunit.getAnswer().getPicture());
                                fullScreenPictureDialogFragment.onPictureChangedListener = CertifDataSetAdapter.this;
                                fullScreenPictureDialogFragment.show(fm, "PicturePaintDialogFragment");
                            } else {
                                if (getImagesCount() < 10) {
                                    int selectedMode = MultiImageSelectorActivity.MODE_SINGLE;
                                    Intent intent = new Intent(context, MultiImageSelectorActivity.class);
                                    intent.putExtra(MultiImageSelectorActivity.EXTRA_SHOW_CAMERA, true);
                                    intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_MODE, selectedMode);
                                    context.startActivityForResult(intent, Constants.CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_CERTIF);
                                } else {
                                    Toast.makeText(context, context.getString(R.string.atteinteNbreMax), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    });
                }
                if (position == getCount() - 1) {
                    convertView.findViewById(R.id.dummyPadding).setVisibility(View.VISIBLE);
                } else {
                    convertView.findViewById(R.id.dummyPadding).setVisibility(View.GONE);
                }
            }
        }
        Utils.enableDisableView(convertView, Singleton.getInstance().getCurrentNote().getStatus().equals("IN_PROGRESS"));

        return convertView;
    }

    private int getImagesCount() {
        int imagesCount = 0;
        for (Object o : dataset) {
            if ((o instanceof WorkUnit) && (((WorkUnit) o).getAnswer().getPicture() != null) && !((WorkUnit) o).getAnswer().getPicture().isEmpty()) {
                imagesCount++;
            }
        }
        return imagesCount;
    }

    @Override
    public void onPictureChanged() {
        notifyDataSetChanged();
    }

    @Override
    public void onPictureDeleted() {
        currentCameraWorkunit.getAnswer().setPicture(null);
        notifyDataSetChanged();
    }

    @Override
    public boolean onItemsSelected(boolean[] selected, WorkUnit item) {

        item.getAnswer().setActionPlans("");
        for (int s = 0; s < selected.length; s++) {
            if (selected[s])
                item.getAnswer().addActionPlan(s);
        }
        if (item.getAnswer().getScore() == 1 && item.getAnswer().getActionPlans().size() < 1) {
            return false;
        }
        return true;
    }
}
