package geca.dareliconsulting.smarttoutch.com.geca.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.model.WorkUnit;

/**
 * Created by Smart on 13/05/2015.
 */
public class FitWUAdapter extends ArrayAdapter<WorkUnit> {


    private final Activity context;
    protected ArrayList<WorkUnit> list;

    public View view;

    public FitWUAdapter(Activity context, ArrayList<WorkUnit> mylist) {
        super(context, R.layout.row_fit_wu, mylist);
        this.context = context;
        this.list = mylist;

    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.row_fit_wu, null, true);

        TextView nbRecover = (TextView) rowView.findViewById(R.id.tv_name);
        nbRecover.setText(String.valueOf(list.get(position).getLabel()));
        return rowView;
    }

}
