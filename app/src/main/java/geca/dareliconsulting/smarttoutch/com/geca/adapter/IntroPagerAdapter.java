package geca.dareliconsulting.smarttoutch.com.geca.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import geca.dareliconsulting.smarttoutch.com.geca.R;

/**
 * Created by Smarttouch on 30/05/16.
 */
public class IntroPagerAdapter extends PagerAdapter {

    private Context mContext;
    private int[] layouts = {
            R.layout.layout_intro_1,
            R.layout.layout_intro_2,
            R.layout.layout_intro_3,
            R.layout.layout_intro_4,
            R.layout.layout_intro_5,
            R.layout.layout_intro_6,
            R.layout.layout_intro_7
    };

    public IntroPagerAdapter(Context context) {
        mContext = context;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(layouts[position], collection, false);
        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return layouts.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

}