package geca.dareliconsulting.smarttoutch.com.geca.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;

import java.util.ArrayList;
import java.util.Locale;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.model.Answer;
import geca.dareliconsulting.smarttoutch.com.geca.model.Note;

/**
 * Created by Smart on 28/04/2015.
 */
public class ListNoteAdapter extends BaseSwipeAdapter {


    private final Activity context;
    protected ArrayList<Note> listNotes;
    protected ArrayList<Note> filterNotes;

    public View view;

    public ListNoteAdapter(Activity context, ArrayList<Note> mylist) {
        this.context = context;
        this.listNotes = mylist;
        filterNotes = new ArrayList<>();
        this.filterNotes.addAll(mylist);

    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    @Override
    public View generateView(final int position, ViewGroup parent) {
        View v = LayoutInflater.from(context).inflate(R.layout.note_list_row, null);
        return v;
    }

    @Override
    public void fillValues(int position, View convertView) {
        TextView notelabel = (TextView) convertView.findViewById(R.id.note_label);
        TextView type = (TextView) convertView.findViewById(R.id.type);
        TextView status = (TextView) convertView.findViewById(R.id.status);
        TextView synchronised = (TextView) convertView.findViewById(R.id.synchronised);
        Note note = listNotes.get(position);
        notelabel.setText(note.getLabel());
        if (note.getType().equals("VISITE_TECHNIQUE")) {
            type.setText(context.getString(R.string.visiteTechnique));
        } else if (note.getType().equals("CERTIFICATION")) {
            type.setText(context.getString(R.string.certification));
        }else if (note.getType().equals("VISITE_SECURITE_ENVIRONNEMENT")) {
            type.setText(context.getString(R.string.SAFETY));
        } else {
            type.setText(context.getString(R.string.quiz));
        }

        if (note.isSynchronised()) {
            synchronised.setText(context.getString(R.string.yes));
        } else {
            synchronised.setText(context.getString(R.string.no));
        }
        if (note.getStatus().equals("VALIDE")) {
            status.setText(context.getString(R.string.validee));
        } else if (note.getStatus().equals("ABANDON")) {
            status.setText(context.getString(R.string.abandonnee));
        } else {
            status.setText(context.getString(R.string.enCours));
        }

        if (position % 2 == 0) {
            convertView.setBackgroundColor(context.getResources().getColor(R.color.background_actionbar));
        } else {
            convertView.setBackgroundColor(context.getResources().getColor(R.color.white));
        }
        convertView.findViewById(R.id.layoutDelete).setTag(note);
        convertView.findViewById(R.id.layoutDelete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SwipeLayout swipeLayout = (SwipeLayout) view.getParent();
                swipeLayout.close(false);
                Note note1 = (Note)view.getTag();
                listNotes.remove(note1);
                filterNotes.remove(note1);
                Answer.deleteAll(Answer.class, "note_id = ?", "" + note1.getId());
                note1.delete();
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getCount() {
        return listNotes.size();
    }

    @Override
    public Note getItem(int position) {
        return listNotes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        listNotes.clear();
        if (charText.length() == 0) {
            listNotes.addAll(filterNotes);
        } else {
            for (Note note : filterNotes) {
                if (note.getLabel().toLowerCase(Locale.getDefault()).contains(charText)) {
                    listNotes.add(note);
                } else if (note.getType().toLowerCase(Locale.getDefault()).contains(charText)) {
                    listNotes.add(note);
                } else if (charText.equals(context.getString(R.string.yes)) && note.isSynchronised()) {
                    listNotes.add(note);
                } else if (charText.equals(context.getString(R.string.no)) && !note.isSynchronised()) {
                    listNotes.add(note);
                }
            }
        }
        notifyDataSetChanged();
    }
}
