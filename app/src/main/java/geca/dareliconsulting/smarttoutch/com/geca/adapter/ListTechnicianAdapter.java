package geca.dareliconsulting.smarttoutch.com.geca.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.model.Technician;

/**
 * Created by Smart on 23/04/2015.
 */
public class ListTechnicianAdapter extends ArrayAdapter<Technician> {


    private final Activity context;
    protected ArrayList<Technician> listtechniciens;
    protected ArrayList<Technician> filtertechnicienas;

    public View view;

    public ListTechnicianAdapter(Activity context, ArrayList<Technician> mylist) {
        super(context, R.layout.technician_list_row, mylist);
        this.context = context;
        this.filtertechnicienas = new ArrayList<Technician>();
        this.filtertechnicienas.addAll(mylist);
        this.listtechniciens = mylist;

    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.technician_list_row, null, true);

        TextView name = (TextView) rowView.findViewById(R.id.nametechnican);
        TextView surname = (TextView) rowView.findViewById(R.id.idstechnican);
        TextView matricule = (TextView) rowView.findViewById(R.id.status);
        name.setText(listtechniciens.get(position).getName());
        surname.setText(listtechniciens.get(position).getSurName());
        matricule.setText(listtechniciens.get(position).getRegistrationNumber());
        if (position % 2 == 0) {
            rowView.setBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            rowView.setBackgroundColor(context.getResources().getColor(R.color.background_actionbar));
        }
        return rowView;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault()).trim();
        listtechniciens.clear();
        if (charText.length() == 0) {
            listtechniciens.addAll(filtertechnicienas);
        } else {
            for (Technician technician : filtertechnicienas) {
                if (technician.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    listtechniciens.add(technician);
                } else if (technician.getSurName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    listtechniciens.add(technician);
                } else if (technician.getRegistrationNumber().toLowerCase(Locale.getDefault()).contains(charText)) {

                    listtechniciens.add(technician);
                }
            }
        }
        notifyDataSetChanged();
    }

}
