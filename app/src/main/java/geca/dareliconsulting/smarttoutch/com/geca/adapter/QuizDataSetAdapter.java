package geca.dareliconsulting.smarttoutch.com.geca.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.model.Question;
import geca.dareliconsulting.smarttoutch.com.geca.model.Skill;
import geca.dareliconsulting.smarttoutch.com.geca.model.TypeSkill;
import geca.dareliconsulting.smarttoutch.com.geca.singleton.Singleton;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Utils;

/**
 * Created by Smart on 28/04/2015.
 */
public class QuizDataSetAdapter extends ArrayAdapter<Object> {


    private final Activity context;
    protected ArrayList<Object> dataSet;

    public QuizDataSetAdapter(Activity context, ArrayList<Object> mylist) {
        super(context, -1, mylist);
        this.context = context;
        this.dataSet = mylist;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View convertView = new View(context);
        if (dataSet.get(position) instanceof TypeSkill) {
            TypeSkill item = (TypeSkill) dataSet.get(position);
            convertView = inflater.inflate(R.layout.row_type_skill_quiz, null, true);
            TextView typeSkillLabel = (TextView) convertView.findViewById(R.id.textViewTypeSkillLabel);
            typeSkillLabel.setText(item.getTypeSkillLabel());
        } else if (dataSet.get(position) instanceof Skill) {
            Skill item = (Skill) dataSet.get(position);
            convertView = inflater.inflate(R.layout.row_skill_quiz, null, true);
            TextView skillName = (TextView) convertView.findViewById(R.id.skillName);
            skillName.setText(item.getLabel());
        } else if (dataSet.get(position) instanceof Question) {

            final Question item = (Question) dataSet.get(position);
            convertView = inflater.inflate(R.layout.row_question_quiz, null, true);

            TextView questionText = (TextView) convertView.findViewById(R.id.textViewQuestion);
            TextView commentText = (TextView) convertView.findViewById(R.id.textViewComment);
            final ImageView correctImage = (ImageView) convertView.findViewById(R.id.imageViewCorrectAnswer);
            final LinearLayout commentLayout = (LinearLayout) convertView.findViewById(R.id.layoutComment);

            questionText.setText("Question : " + item.getQuestionText());
            commentText.setText(item.getComment());

            final CheckBox checkBoxYes = (CheckBox) convertView.findViewById(R.id.checkBoxQuestionYes);
            final CheckBox checkBoxNo = (CheckBox) convertView.findViewById(R.id.checkBoxQuestionNo);

            Log.i("question", item.getQuestionText() + " " + item.isQuestionIsTrue());

            if (item.getAnswer().isAnswered()) {
                Log.i("item.getAnswer Answered", "" + item.getAnswer().isAnswered());
                checkBoxYes.setEnabled(false);
                checkBoxYes.setFocusable(false);
                checkBoxNo.setFocusable(false);
                checkBoxNo.setEnabled(false);
                checkBoxNo.setClickable(false);
                checkBoxYes.setClickable(false);

                commentLayout.setVisibility(View.VISIBLE);

                if (item.getAnswer().isCorrect()) {

                    correctImage.setVisibility(View.VISIBLE);
                    if (item.isQuestionIsTrue()) {
                        checkBoxYes.setChecked(true);
                        checkBoxNo.setChecked(false);
                    } else {
                        checkBoxNo.setChecked(true);
                        checkBoxYes.setChecked(false);
                    }
                } else {

                    correctImage.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_delete));
                    if (!item.isQuestionIsTrue()) {
                        checkBoxYes.setChecked(true);
                        checkBoxNo.setChecked(false);
                    } else {
                        checkBoxNo.setChecked(true);
                        checkBoxYes.setChecked(false);
                    }
                }
            } else {
                checkBoxNo.setChecked(false);
                checkBoxYes.setChecked(false);
                checkBoxYes.setEnabled(true);

                checkBoxNo.setEnabled(true);

                checkBoxYes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (item.isQuestionIsTrue()) {
                            item.getAnswer().setCorrect(true);
                        } else {
                            item.getAnswer().setCorrect(false);
                        }

                        item.getAnswer().setIsAnswered(true);
                        notifyDataSetChanged();
                    }
                });
                checkBoxNo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (!item.isQuestionIsTrue()) {
                            item.getAnswer().setCorrect(true);
                        } else {
                            item.getAnswer().setCorrect(false);

                        }

                        item.getAnswer().setIsAnswered(true);
                        notifyDataSetChanged();
                    }
                });
                commentLayout.setVisibility(View.GONE);
            }
        }
        Utils.enableDisableView(convertView, Singleton.getInstance().getCurrentNote().getStatus().equals("IN_PROGRESS"));
        return convertView;
    }
}
