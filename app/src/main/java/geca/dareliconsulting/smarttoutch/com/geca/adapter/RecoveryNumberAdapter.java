package geca.dareliconsulting.smarttoutch.com.geca.adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.model.RecoveryNumber;

/**
 * Created by Smart on 13/05/2015.
 */
public class RecoveryNumberAdapter extends ArrayAdapter<RecoveryNumber> {


    private final Activity context;
    protected ArrayList<RecoveryNumber> list;

    public View view;

    public RecoveryNumberAdapter(Activity context, ArrayList<RecoveryNumber> mylist) {
        super(context, R.layout.row_recovery_nb_item, mylist);
        this.context = context;
        this.list = mylist;

    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.row_recovery_nb_item, null, true);

        TextView nbRecover = (TextView) rowView.findViewById(R.id.nbRecovery);
        TextView delayRecovery = (TextView) rowView.findViewById(R.id.delayRecovery);
        if(position==0){
            nbRecover.setText(context.getString(R.string.recoveryNumber));
            delayRecovery.setText(context.getString(R.string.recoveryDelay));
            nbRecover.setTypeface(null, Typeface.BOLD);
            delayRecovery.setTypeface(null, Typeface.BOLD);
        }else{
            nbRecover.setText(String.valueOf(list.get(position).getNumber()));
            delayRecovery.setText(String.valueOf(list.get(position).getDelay()));
            nbRecover.setTypeface(null, Typeface.NORMAL);
            delayRecovery.setTypeface(null, Typeface.NORMAL);
        }
        return rowView;
    }

}
