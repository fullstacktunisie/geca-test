package geca.dareliconsulting.smarttoutch.com.geca.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.model.Technician;

/**
 * Created by Smart on 13/05/2015.
 */
public class SearchTechnicanAdapter extends ArrayAdapter<Technician> {


    private final Activity context;
    protected ArrayList<Technician> listtechniciens;
    protected ArrayList<Technician> filtertechnicienas;

    public View view;

    public SearchTechnicanAdapter(Activity context, ArrayList<Technician> mylist) {
        super(context, android.R.layout.simple_list_item_single_choice, mylist);
        this.context = context;
        this.filtertechnicienas = new ArrayList<Technician>();
        this.filtertechnicienas.addAll(mylist);
        this.listtechniciens = mylist;

    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.row_search_item, null, true);

        TextView name = (TextView) rowView.findViewById(R.id.nametechnican);
        TextView surname = (TextView) rowView.findViewById(R.id.idstechnican);
        TextView matricule = (TextView) rowView.findViewById(R.id.status);
        CheckBox text1 = (CheckBox) rowView.findViewById(R.id.singleitemCheckBox);
        name.setText(listtechniciens.get(position).getName());
        surname.setText(listtechniciens.get(position).getSurName());
        matricule.setText(listtechniciens.get(position).getRegistrationNumber());

        text1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    listtechniciens.get(position).setIsselected(true);
                else
                    listtechniciens.get(position).setIsselected(false);
            }
        });


        if (position % 2 == 0) {
            rowView.setBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            rowView.setBackgroundColor(context.getResources().getColor(R.color.background_actionbar));
        }


        return rowView;
    }


    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault()).trim();
        listtechniciens.clear();
        if (charText.length() == 0) {
            listtechniciens.addAll(filtertechnicienas);
        } else {
            int i = 0;
            for (Technician technician : filtertechnicienas) {
                if (technician.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    listtechniciens.add(technician);
                } else if (technician.getSurName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    listtechniciens.add(technician);
                } else if (technician.getRegistrationNumber().toLowerCase(Locale.getDefault()).contains(charText)) {

                    listtechniciens.add(technician);
                }
            }
        }
        notifyDataSetChanged();
    }

}
