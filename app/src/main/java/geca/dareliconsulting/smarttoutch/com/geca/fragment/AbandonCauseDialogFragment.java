package geca.dareliconsulting.smarttoutch.com.geca.fragment;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.model.AbandonReason;

public class AbandonCauseDialogFragment extends DialogFragment {

    Spinner spinner;

    public OnAbandonCauseInteractionListener onAbandonCauseInteractionListener;

    public AbandonCauseDialogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.dialog_abandon_cause, container, false);

        spinner = (Spinner) view.findViewById(R.id.spinnerAbandonCause);
        ArrayAdapter<AbandonReason> adapter = new ArrayAdapter<AbandonReason>(getActivity(), R.layout.spinner_item, AbandonReason.listAll(AbandonReason.class));
        spinner.setAdapter(adapter);
        Button buttonSave = (Button) view.findViewById(R.id.buttonAbandon);
        Button buttonCancel = (Button) view.findViewById(R.id.buttonCancel);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAbandonCauseInteractionListener.onConfirmAction((AbandonReason) spinner.getSelectedItem());
                dismiss();
            }
        });
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return view;
    }

    public interface OnAbandonCauseInteractionListener {
        // TODO: Update argument type and name
        void onConfirmAction(AbandonReason abandonReason);
    }

}
