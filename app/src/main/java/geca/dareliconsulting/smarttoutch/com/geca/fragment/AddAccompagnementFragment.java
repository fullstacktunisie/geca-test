package geca.dareliconsulting.smarttoutch.com.geca.fragment;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.model.Competence;
import geca.dareliconsulting.smarttoutch.com.geca.model.RequestTraining;
import geca.dareliconsulting.smarttoutch.com.geca.model.Technician;
import geca.dareliconsulting.smarttoutch.com.geca.model.Training;
import geca.dareliconsulting.smarttoutch.com.geca.model.TrainingObject;
import geca.dareliconsulting.smarttoutch.com.geca.model.TrainingType;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Constants;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Utils;

public class AddAccompagnementFragment extends Fragment implements SearchDialogFragment.OnSearchInteractionListener {

    List<Training> trainings;
    List<Competence> competences;
    List<TrainingType> types=new ArrayList<>();
    List<TrainingObject> objects=new ArrayList<>();
    Spinner spinnerTraining, spinnerCompetence, spinnerType, spinnerObject;
    EditText editTextDuration, editTextComment, editTrainer, editTextNumTraining, editTextObject;
    TextView textViewDate, edittextlisttechniciens, textviewTitle;
    Button searchTechnicians, valide;
    RequestTraining requestTraining = new RequestTraining();

    private DatePickerDialog fromDatePickerDialog;
    private SimpleDateFormat dateFormatter;
    Date noteDate = null;
    private static final String ARG_ID = "id";
    String listidtechnices = new String();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            requestTraining = RequestTraining.findById(RequestTraining.class, getArguments().getLong(ARG_ID));
        }
    }

    public AddAccompagnementFragment() {
    }


    public static AddAccompagnementFragment newInstance(Long id) {
        AddAccompagnementFragment fragment = new AddAccompagnementFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_accompagnement, container, false);
        loadViews(rootView);
        addLayoutListener();
        return rootView;
    }


    @Override
    public void onSearchListFinished(ArrayList<Technician> technicians) {

        if (technicians != null) {
            String mytechnicien = "";
            JSONArray jListTechnicans = new JSONArray();
            for (int i = 0; i < technicians.size(); i++) {
                mytechnicien += technicians.get(i).getName();
                jListTechnicans.put(technicians.get(i).getId());
                if(i<technicians.size()-1)
                    mytechnicien += ",";
            }

            listidtechnices = jListTechnicans.toString();
            edittextlisttechniciens.setText(mytechnicien);
        }
    }

    void loadViews(View view) {
        spinnerTraining = (Spinner) view.findViewById(R.id.spinnerTraining);
        spinnerCompetence = (Spinner) view.findViewById(R.id.spinnerCompetence);
        spinnerType = (Spinner) view.findViewById(R.id.spinnerType);
        spinnerObject = (Spinner) view.findViewById(R.id.spinnerObject);
        editTrainer = (EditText) view.findViewById(R.id.editTrainer);
        editTextDuration = (EditText) view.findViewById(R.id.editTextDuration);
        editTextObject = (EditText) view.findViewById(R.id.editType);
        textViewDate = (TextView) view.findViewById(R.id.textViewDate);
        editTextComment = (EditText) view.findViewById(R.id.editTextComment);
        editTextNumTraining = (EditText) view.findViewById(R.id.editTrainingNumber);
        edittextlisttechniciens = (TextView) view.findViewById(R.id.listtechniciens);
        textviewTitle = (TextView) view.findViewById(R.id.textviewTitle);
        Typeface tf = Utils.getHelveticaFont(getActivity());
        textviewTitle.setTypeface(tf);
        searchTechnicians = (Button) view.findViewById(R.id.searchTechnicians);
        valide = (Button) view.findViewById(R.id.valideaccomp);
        trainings = Training.listAll(Training.class);
        ArrayAdapter<Training> adapterTraining = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, trainings);
        spinnerTraining.setAdapter(adapterTraining);
        competences = Competence.listAll(Competence.class);
        ArrayAdapter<Competence> adapterCompetence = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, competences);
        spinnerCompetence.setAdapter(adapterCompetence);
        types=TrainingType.listAll(TrainingType.class);
        ArrayAdapter<TrainingType> adapterTypes = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, types);
        spinnerType.setAdapter(adapterTypes);
        if(types.size()>0)
            spinnerType.setSelection(0);
        objects=TrainingObject.listAll(TrainingObject.class);
        ArrayAdapter<TrainingObject> adapterObjects = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, objects);
        spinnerObject.setAdapter(adapterObjects);

        spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position==0)
                    editTextObject.setVisibility(View.VISIBLE);
                else
                    editTextObject.setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void addLayoutListener() {
        dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        Calendar newCalendar = Calendar.getInstance();
        textViewDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                fromDatePickerDialog.show();
            }
        });
        fromDatePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        noteDate = newDate.getTime();
                        textViewDate.setText(dateFormatter.format(noteDate));
                    }

                },
                newCalendar.get(Calendar.YEAR),
                newCalendar.get(Calendar.MONTH),
                newCalendar.get(Calendar.DAY_OF_MONTH)
        );


        searchTechnicians.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                SearchDialogFragment searchDialogFragment = SearchDialogFragment.newInstance("MULTIPLE");
                searchDialogFragment.onSearchInteractionListener = AddAccompagnementFragment.this;
                searchDialogFragment.show(fm, "SearchDialogFragment");
            }
        });

        valide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });
    }


    void save() {
        if (spinnerTraining.getSelectedItemPosition() == AdapterView.INVALID_POSITION || spinnerCompetence.getSelectedItemPosition() == AdapterView.INVALID_POSITION || spinnerType.getSelectedItemPosition() == AdapterView.INVALID_POSITION ||
                (spinnerObject.getSelectedItemPosition() == AdapterView.INVALID_POSITION && editTextObject.getText().toString().isEmpty()) || textViewDate.getText().toString().trim().equals("")
                || editTrainer.getText().toString().trim().equals("") || edittextlisttechniciens.getText().toString().trim().equals("")
                || listidtechnices == null || listidtechnices.trim().equals("") || editTextNumTraining.getText().toString().trim().equals("")|| editTextDuration.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity().getApplicationContext(), getActivity().getString(R.string.ChampsObligatoire), Toast.LENGTH_SHORT).show();
            return;
        } else {
            Training training = ((Training) spinnerTraining.getSelectedItem());
            Competence competence = ((Competence) spinnerCompetence.getSelectedItem());
            TrainingType type=(TrainingType) spinnerType.getSelectedItem();

            requestTraining.setListTechniciens(listidtechnices);
            requestTraining.setSkillId(competence.getServerId());
            requestTraining.setTrainingId(training.getServerId());
            requestTraining.setNametrainer(editTrainer.getText().toString().trim());
            requestTraining.setDuration(editTextDuration.getText().toString().trim());
            requestTraining.setComment(editTextComment.getText().toString().trim());
            requestTraining.setType(type.getServerId());
            if(spinnerType.getSelectedItemPosition()==0 && !editTextObject.getText().toString().isEmpty()) {
                (new TrainingObject(0, editTextObject.getText().toString().trim())).save();
                requestTraining.setTrainingObject(editTextObject.getText().toString().trim());
            }else
                requestTraining.setTrainingObject(String.valueOf(((TrainingObject) spinnerObject.getSelectedItem()).getServerId()));
            requestTraining.setNumTraining(Integer.parseInt(editTextNumTraining.getText().toString().trim()));
            requestTraining.setSynchron(false);
            try {
                requestTraining.setDatestart((dateFormatter.parse(textViewDate.getText().toString().trim())));
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (requestTraining.getId() == null) {
                requestTraining.setSynchron(requestTraining.isSynchron());
                requestTraining.save();
                Toast.makeText(getActivity().getApplicationContext(), getActivity().getString(R.string.msgFicheAjouteeSuccess), Toast.LENGTH_SHORT).show();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                MainFragment mainFragment = new MainFragment();
                fragmentTransaction.replace(R.id.frame_container, mainFragment);
                fragmentTransaction.commit();
            } else {
                requestTraining.save();
                Toast.makeText(getActivity().getApplicationContext(), getActivity().getString(R.string.msgUpdateSuccess), Toast.LENGTH_SHORT).show();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                MainFragment mainFragment = new MainFragment();
                fragmentTransaction.replace(R.id.frame_container, mainFragment);
                fragmentTransaction.commit();
            }

        }
    }

    void loadViewsContent() {

        if (requestTraining == null) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.msgFicheAccompagnement), Toast.LENGTH_LONG).show();
            return;
        }
        if (requestTraining.getId() != null) {
            if (requestTraining.getSkillId() != 0) {
                int selectedCompetenceIndex = -1;
                for (int i = 0; i < Competence.listAll(Competence.class).size(); i++) {
                    Competence competence = Competence.listAll(Competence.class).get(i);
                    if (requestTraining.getSkillId() == competence.getServerId()) {
                        selectedCompetenceIndex = i;
                        break;
                    }
                }
                spinnerCompetence.setSelection(selectedCompetenceIndex, true);
            }
            if (requestTraining.getType() != 0) {
                int selectedTypeIndex = -1;
                for (int i = 0; i < TrainingType.listAll(TrainingType.class).size(); i++) {
                    TrainingType type = TrainingType.listAll(TrainingType.class).get(i);
                    if (requestTraining.getType() == type.getServerId()) {
                        selectedTypeIndex = i;
                        break;
                    }
                }
                spinnerType.setSelection(selectedTypeIndex, true);
            }

            if (!requestTraining.getTrainingObject().equals("")) {
                int selectedObjectIndex = -1;
                for (int i = 0; i < TrainingObject.listAll(TrainingObject.class).size(); i++) {
                    TrainingObject object = TrainingObject.listAll(TrainingObject.class).get(i);
                    if (requestTraining.getTrainingObject().equals(object.getServerId())) {
                        selectedObjectIndex = i;
                        break;
                    }else if(requestTraining.getTrainingObject().equals(object.getName())){
                        selectedObjectIndex = -1;
                        editTextObject.setText(object.getName());
                        break;
                    }
                }
                spinnerObject.setSelection(selectedObjectIndex, true);
            }

            if (requestTraining.getTrainingId() != 0) {
                int selectedTrainingIndex = -1;
                for (int i = 0; i < Training.listAll(Training.class).size(); i++) {
                    Training training = Training.listAll(Training.class).get(i);
                    if (requestTraining.getTrainingId() == training.getServerId()) {
                        selectedTrainingIndex = i;
                        break;
                    }
                }
                spinnerTraining.setSelection(selectedTrainingIndex, true);
            }
            editTrainer.setText(requestTraining.getNametrainer());
            editTextDuration.setText(requestTraining.getDuration());
            textViewDate.setText((dateFormatter.format(requestTraining.getDatestart())));
            editTextComment.setText(requestTraining.getComment());
            editTextNumTraining.setText(String.valueOf(requestTraining.getNumTraining()));
            if (requestTraining.getListTechniciens() != null) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(requestTraining.getListTechniciens());
                    String mytechnicien = "";
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Technician technician = Technician.findById(Technician.class, jsonArray.getLong(i));
                        if (technician != null) {
                            mytechnicien = mytechnicien + "," + technician.getName();
                        }
                    }
                    listidtechnices = requestTraining.getListTechniciens();
                    edittextlisttechniciens.setText(mytechnicien);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        } else {
            editTrainer.setText((getActivity().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE).getString("firstname", "") + " " + getActivity().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE).getString("lastname", "")).trim());
        }
    }

    @Override
    public void onResume() {

        super.onResume();
        loadViewsContent();
    }
}
