package geca.dareliconsulting.smarttoutch.com.geca.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.model.Note;
import geca.dareliconsulting.smarttoutch.com.geca.model.Profession;
import geca.dareliconsulting.smarttoutch.com.geca.model.ProfessionFamily;
import geca.dareliconsulting.smarttoutch.com.geca.model.Sector;
import geca.dareliconsulting.smarttoutch.com.geca.model.Skill;
import geca.dareliconsulting.smarttoutch.com.geca.model.TypeSkill;
import geca.dareliconsulting.smarttoutch.com.geca.model.WorkUnit;
import geca.dareliconsulting.smarttoutch.com.geca.singleton.Singleton;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Constants;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Utils;

public class AddNoteFragment extends Fragment {

    public Spinner noteTypeSpinner;
    public Spinner sectorSpinner;
    public Spinner professionFamilySpinner;
    public Spinner professionsSpinner;
    public Button addNoteButton;
    public String[] noteTypes;
    public TextView TextViewAddFiche;
    public View rootView;

    public AddNoteFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_add_note, container, false);
        try {
            Utils.refreshDatasystem(getActivity());
        } catch (Exception e) {
        }

        // Spinners
        noteTypeSpinner = ((Spinner) rootView.findViewById(R.id.spinner_typenote));
        sectorSpinner = ((Spinner) rootView.findViewById(R.id.spinner_Domaine));
        professionFamilySpinner = ((Spinner) rootView.findViewById(R.id.spinner_famille_metier));
        professionsSpinner = ((Spinner) rootView.findViewById(R.id.spinner_metier));
        noteTypes = getResources().getStringArray(R.array.type_note);
        // Title Style
        TextViewAddFiche = ((TextView) rootView.findViewById(R.id.TextViewAddFiche));
        Typeface tf = Utils.getHelveticaFont(getActivity());
        TextViewAddFiche.setTypeface(tf);
        // spinner type note
        ArrayAdapter<String> adapterTypeFiche = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, noteTypes);
        noteTypeSpinner.setAdapter(adapterTypeFiche);

        // spinner sector
        ArrayAdapter<Sector> sectorArrayAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, Singleton.getInstance().getSectors());
        sectorSpinner.setAdapter(sectorArrayAdapter);
        sectorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Object o = sectorSpinner.getItemAtPosition(position);
                Sector newsData = (Sector) o;
                ArrayAdapter<ProfessionFamily> professionFamilyArrayAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, newsData.getListactivityFamilies());
                professionFamilySpinner.setAdapter(professionFamilyArrayAdapter);
            }

            @Override

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        professionFamilySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Object o = professionFamilySpinner.getItemAtPosition(position);
                ProfessionFamily newsData = (ProfessionFamily) o;
                ArrayAdapter<Profession> professionArrayAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, newsData.getProfessions());
                professionsSpinner.setAdapter(professionArrayAdapter);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        addNoteButton = ((Button) rootView.findViewById(R.id.add_note_step1));
        addNoteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Profession selectedProfession = (Profession) professionsSpinner.getSelectedItem();

                if (noteTypeSpinner.getSelectedItemPosition() == 0) {
                    Toast.makeText(getActivity(), getActivity().getString(R.string.msgChoixTypeFiche), Toast.LENGTH_LONG).show();
                    return;
                }
                if (sectorSpinner.getSelectedItemPosition() == AdapterView.INVALID_POSITION) {
                    Toast.makeText(getActivity(), getActivity().getString(R.string.msgChoixDomaine), Toast.LENGTH_LONG).show();
                    return;
                }
                if (professionFamilySpinner.getSelectedItemPosition() == AdapterView.INVALID_POSITION) {
                    Toast.makeText(getActivity(), getActivity().getString(R.string.msgChoixFamilleMetier), Toast.LENGTH_LONG).show();
                    return;
                }
                if (professionsSpinner.getSelectedItemPosition() == AdapterView.INVALID_POSITION) {
                    Toast.makeText(getActivity(), getActivity().getString(R.string.msgChoixMetier), Toast.LENGTH_LONG).show();
                    return;
                }
                switch (noteTypeSpinner.getSelectedItemPosition()) {
                    case 1: {
                        Note note = new Note();
                        note.setAuditorId(getActivity().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE).getString("user_id", ""));
                        note.setSectorId(((Sector) (sectorSpinner.getSelectedItem())).getId());
                        note.setProfessionFamilyId(((ProfessionFamily) (professionFamilySpinner.getSelectedItem())).getId());
                        note.setProfessionId(((Profession) (professionsSpinner.getSelectedItem())).getIdServer());
                        note.setType("VISITE_TECHNIQUE");
                        note.setTypeSkills(((Profession) (professionsSpinner.getSelectedItem())).getTypeSkills());
                        Singleton.getInstance().setCurrentNote(note);
                        FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.frame_container, new VisiteTechniqueFragment()).commit();
                        break;
                    }
                    case 2: {
                        Note note = new Note();
                        note.setAuditorId(getActivity().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE).getString("user_id", ""));
                        note.setSectorId(((Sector) (sectorSpinner.getSelectedItem())).getId());
                        note.setProfessionFamilyId(((ProfessionFamily) (professionFamilySpinner.getSelectedItem())).getId());
                        note.setProfessionId(((Profession) (professionsSpinner.getSelectedItem())).getIdServer());
                        note.setType("CERTIFICATION");
                        note.setTypeSkills(((Profession) (professionsSpinner.getSelectedItem())).getTypeSkills());
                        Singleton.getInstance().setCurrentNote(note);
                        boolean workUnitExists=false;
                        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
                            for (Skill skill : typeSkill.getSkills()) {
                                for (WorkUnit workUnit : skill.getWorkUnits()) {
                                    Log.e("wu",workUnit.getId()+" "+workUnit.isCertification());
                                    if(workUnit.isCertification()) {
                                        workUnitExists=true;
                                        break;
                                    }
                                }
                            }
                        }
                        if(workUnitExists) {
                            FragmentManager fragmentManager = getFragmentManager();
                            fragmentManager.beginTransaction().replace(R.id.frame_container, new CertificationFragment()).commit();
                        }else{
                            PopUpNoCertificationsWU();
                        }
                        break;
                    }
                    case 3: {
                        Note note = new Note();
                        note.setAuditorId(getActivity().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE).getString("user_id", ""));
                        note.setSectorId(((Sector) (sectorSpinner.getSelectedItem())).getId());
                        note.setProfessionFamilyId(((ProfessionFamily) (professionFamilySpinner.getSelectedItem())).getId());
                        note.setProfessionId(((Profession) (professionsSpinner.getSelectedItem())).getIdServer());
                        note.setType("QUIZ");
                        note.setTypeSkills(((Profession) (professionsSpinner.getSelectedItem())).getTypeSkills());
                        note.setCreated((new Date()).getTime());
                        note.setUpdated((new Date()).getTime());
                        Singleton.getInstance().setCurrentNote(note);
                        FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.frame_container, new QuizFragment()).commit();
                        break;
                    }
                    case 4: {
                        Note note = new Note();
                        note.setAuditorId(getActivity().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE).getString("user_id", ""));
                        note.setSectorId(((Sector) (sectorSpinner.getSelectedItem())).getId());
                        note.setProfessionFamilyId(((ProfessionFamily) (professionFamilySpinner.getSelectedItem())).getId());
                        note.setProfessionId(((Profession) (professionsSpinner.getSelectedItem())).getIdServer());
                        note.setType("VISITE_SECURITE_ENVIRONNEMENT");
                        note.setTypeSkills(((Profession) (professionsSpinner.getSelectedItem())).getTypeSkills());
                        Singleton.getInstance().setCurrentNote(note);
                        FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.frame_container, new VisiteTechniqueFragment()).commit();
                        break;
                    }
                    default:
                        break;
                }
            }
        });
        return rootView;
    }





    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }



    // SHOW POP UP MSG
    void PopUpNoCertificationsWU() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getActivity());
        alertDialogBuilder.setTitle("Info");
        alertDialogBuilder
                .setMessage(getActivity().getString(R.string.msgNoWUCertification))
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
