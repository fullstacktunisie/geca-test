package geca.dareliconsulting.smarttoutch.com.geca.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.model.Agence;
import geca.dareliconsulting.smarttoutch.com.geca.model.BusinessManager;
import geca.dareliconsulting.smarttoutch.com.geca.model.CompanyProvider;
import geca.dareliconsulting.smarttoutch.com.geca.model.Fonction;
import geca.dareliconsulting.smarttoutch.com.geca.model.Technician;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Utils;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;

/**
 * Created by iMac1 on 12/05/15.
 */
public class AddRessourceFragment extends Fragment implements FullScreenPictureDialogFragment.OnFragmentInteractionListener {

    TextView ititleressource;
    EditText edittextnametechnician;
    EditText edittextsurnametechnician;
    EditText edittextnumbersecuritytechnician;
    EditText edittextnumbermatriculetechnician;

    TextView textviewdatestarttechnician;
    TextView textviewdateendtechnician;

    EditText edittextemailtechnician;
    EditText edittextmobilenumbertechnician;

    Spinner spinnerfonctiontechnician;
    Spinner spinnercompanytechnician;
    Spinner spinneragencetechnician;
    Spinner spinnerresponsableaffaire;

    ImageView imageviewpictechnician;
    Button buttonaddpictechnician;
    Button buttontechnicianannul;
    Button addtechnicianregister;

    EditText edittextformation;
    EditText edittextexperience;

    private boolean hasPhoto = false;

    static final int CAMERA_INTENT = 4444;
    private static final int REQUEST_IMAGE = 2;
    private ArrayList<String> mSelectPath;
    private SimpleDateFormat dateFormatter;
    Date DateStart = null;
    Date DateEnd = null;
    private DatePickerDialog fromDatePickerDialogStart;
    private DatePickerDialog fromDatePickerDialogEnd;
    private static final String ARG_ID = "id";
    Technician technician = new Technician();

   /* View.OnClickListener cameraListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            FragmentManager fm = getFragmentManager();
            CameraDialogFragment cameraDialogFragment = CameraDialogFragment.newInstance();
            cameraDialogFragment.takePictureInteractionListener = AddRessourceFragment.this;
            cameraDialogFragment.show(fm, "CameraDialogFragment");
        }
    };*/

    public static AddRessourceFragment newInstance(Long id) {
        AddRessourceFragment fragment = new AddRessourceFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_ID, id);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            technician = Technician.findById(Technician.class, getArguments().getLong(ARG_ID));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_ressource, container, false);
        loadViews(view);
        addLayoutListener();
        dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        addtechnicianregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savetechnican();
            }


        });
        buttontechnicianannul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                RessourcesFragment ressourcesFragment = new RessourcesFragment();
                fragmentTransaction.replace(R.id.frame_container, ressourcesFragment);
                fragmentTransaction.commit();
            }
        });

        return view;
    }

    void loadViews(View view) {
        // Edit Text
        edittextexperience = (EditText) view.findViewById(R.id.edittextexperience);
        edittextformation = (EditText) view.findViewById(R.id.edittextformation);
        edittextnametechnician = (EditText) view.findViewById(R.id.edittextnametechnician);
        edittextnumbermatriculetechnician = (EditText) view.findViewById(R.id.edittextnumbermatriculetechnician);
        edittextsurnametechnician = (EditText) view.findViewById(R.id.edittextsurnametechnician);
        edittextnumbersecuritytechnician = (EditText) view.findViewById(R.id.edittextnumbersecuritytechnician);
        textviewdatestarttechnician = (TextView) view.findViewById(R.id.textviewdatestarttechnician);
        textviewdateendtechnician = (TextView) view.findViewById(R.id.textviewdateendtechnician);
        edittextemailtechnician = (EditText) view.findViewById(R.id.edittextemailtechnician);
        edittextmobilenumbertechnician = (EditText) view.findViewById(R.id.edittextmobilenumbertechnician);
        buttonaddpictechnician = (Button) view.findViewById(R.id.buttonaddpictechnician);
        imageviewpictechnician = (ImageView) view.findViewById(R.id.imageviewpictechnician);
        ititleressource = ((TextView) view.findViewById(R.id.ressourcestitle));
        Typeface tf = Utils.getHelveticaFont(getActivity());
        ititleressource.setTypeface(tf);
        // Spinners
        spinnerfonctiontechnician = (Spinner) view.findViewById(R.id.spinnerfonctiontechnician);
        ArrayAdapter<Fonction> adapteFunction = new ArrayAdapter<Fonction>(getActivity(), R.layout.spinner_item, Fonction.listAll(Fonction.class));
        spinnerfonctiontechnician.setAdapter(adapteFunction);

        spinnercompanytechnician = (Spinner) view.findViewById(R.id.spinnercompanytechnician);
        ArrayAdapter<CompanyProvider> adaptecompany = new ArrayAdapter<CompanyProvider>(getActivity(), R.layout.spinner_item, CompanyProvider.listAll(CompanyProvider.class));
        spinnercompanytechnician.setAdapter(adaptecompany);

        spinneragencetechnician = (Spinner) view.findViewById(R.id.spinneragencetechnician);
        ArrayAdapter<Agence> adapteragence = new ArrayAdapter<Agence>(getActivity(), R.layout.spinner_item, Agence.listAll(Agence.class));
        spinneragencetechnician.setAdapter(adapteragence);

        spinnerresponsableaffaire = (Spinner) view.findViewById(R.id.spinnerresponsableaffaire);
        ArrayAdapter<BusinessManager> adapterresponsable = new ArrayAdapter<BusinessManager>(getActivity(), R.layout.spinner_item, BusinessManager.listAll(BusinessManager.class));
        spinnerresponsableaffaire.setAdapter(adapterresponsable);

        // Buttons
        buttontechnicianannul = (Button) view.findViewById(R.id.buttontechnicianannul);
        addtechnicianregister = (Button) view.findViewById(R.id.addtechnicianregister);
    }

    private void addLayoutListener() {

        buttonaddpictechnician.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
// single mode
                int selectedMode = MultiImageSelectorActivity.MODE_SINGLE;
                selectedMode = MultiImageSelectorActivity.MODE_SINGLE;

                Intent intent = new Intent(getActivity(), MultiImageSelectorActivity.class);
                intent.putExtra(MultiImageSelectorActivity.EXTRA_SHOW_CAMERA, true);

                intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_MODE, selectedMode);
                if (mSelectPath != null && mSelectPath.size() > 0) {
                    intent.putExtra(MultiImageSelectorActivity.EXTRA_DEFAULT_SELECTED_LIST, mSelectPath);
                }
                startActivityForResult(intent, REQUEST_IMAGE);
            }
        });
        final Calendar newCalendarStart = Calendar.getInstance();
        final Calendar newCalendarEnd = Calendar.getInstance();

        /*
        method callback input Date start
         */
        textviewdatestarttechnician.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                fromDatePickerDialogStart.show();
            }
        });



        /*
        Method CallBack in Input date end
         */
        textviewdateendtechnician.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                fromDatePickerDialogEnd.show();
            }
        });


        /*
  DatePiker  Object  Dialog start  Date
         */
        fromDatePickerDialogStart = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        newCalendarStart.set(year, monthOfYear, dayOfMonth);
                        DateStart = newCalendarStart.getTime();
                        textviewdatestarttechnician.setText(dateFormatter.format(DateStart));
                    }

                },
                newCalendarStart.get(Calendar.YEAR),
                newCalendarStart.get(Calendar.MONTH),
                newCalendarStart.get(Calendar.DAY_OF_MONTH)
        );
        /*
         Date Piker Object end Date
         */
        fromDatePickerDialogEnd = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        newCalendarEnd.set(year, monthOfYear, dayOfMonth);
                        DateEnd = newCalendarEnd.getTime();
                        textviewdateendtechnician.setText(dateFormatter.format(DateEnd));
                    }

                },
                newCalendarEnd.get(Calendar.YEAR),
                newCalendarEnd.get(Calendar.MONTH),
                newCalendarEnd.get(Calendar.DAY_OF_MONTH)
        );

        imageviewpictechnician.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (technician != null && technician.getLocalImage() != null) {
                    /*Intent intent = new Intent(getActivity(), FullScreenPictureActivity.class);
                    intent.putExtra("url", technician.getLocalImage());
                    startActivity(intent);*/

                    FragmentManager fm = getFragmentManager();
                    FullScreenPictureDialogFragment fullScreenPictureDialogFragment = FullScreenPictureDialogFragment.newInstance(technician.getLocalImage());
                    fullScreenPictureDialogFragment.onFragmentInteractionListener = AddRessourceFragment.this;
                    fullScreenPictureDialogFragment.show(fm, "FullScreenPictureDialogFragment");

                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                mSelectPath = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);

                for (String p : mSelectPath) {
                    Log.i("TEST", p);
                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    Bitmap bitmap = BitmapFactory.decodeFile(p, bmOptions);

                    if (bitmap != null) {
                        hasPhoto = true;
                        technician.setLocalImage(Utils.saveBitmapToFile(bitmap));
                        imageviewpictechnician.setImageBitmap(Utils.loadSmallBitmapFromFile(technician.getLocalImage()));
                    }
                }

            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        loadViewsContent();
    }

    void loadViewsContent() {

        if (technician == null) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.msgTechNull), Toast.LENGTH_LONG).show();
            return;
        }
        if (technician.getId() != null) {
            textviewdatestarttechnician.setText(technician.getStartDate());
            textviewdateendtechnician.setText(technician.getEndDate());
            edittextnametechnician.setText(technician.getName());
            edittextnumbermatriculetechnician.setText(technician.getRegistrationNumber());
            edittextsurnametechnician.setText(technician.getSurName());
            edittextnumbersecuritytechnician.setText(technician.getSocialSecurityNumber());
            edittextemailtechnician.setText(technician.getEmail());
            edittextmobilenumbertechnician.setText(technician.getPhone());
            edittextformation.setText(technician.getFormation());
            edittextexperience.setText(technician.getExperience());
            try {
                imageviewpictechnician.setImageBitmap(Utils.loadSmallBitmapFromFile(technician.getLocalImage()));
            } catch (NullPointerException e) {
            } catch (IllegalArgumentException e) {
            }


            if (technician.getFunctionId() != 0) {
                int selectedFonctionIndex = -1;
                for (int i = 0; i < Fonction.listAll(Fonction.class).size(); i++) {
                    Fonction fonction = Fonction.listAll(Fonction.class).get(i);
                    if (technician.getFunctionId() == fonction.getIdFunction()) {
                        selectedFonctionIndex = i;
                        break;
                    }
                }
                spinnerfonctiontechnician.setSelection(selectedFonctionIndex, true);
            }

            if (technician.getBusinessResponsibleId() != 0) {
                int selectedRespondableIndex = -1;
                for (int i = 0; i < BusinessManager.listAll(BusinessManager.class).size(); i++) {
                    BusinessManager responsibleBusiness = BusinessManager.listAll(BusinessManager.class).get(i);
                    if (technician.getBusinessResponsibleId() == responsibleBusiness.getServerId()) {
                        selectedRespondableIndex = i;
                        break;
                    }
                }
                spinnerresponsableaffaire.setSelection(selectedRespondableIndex, true);
            }

            if (technician.getCompanyId() != 0) {
                int selectedCampnayIndex = -1;
                for (int i = 0; i < CompanyProvider.listAll(CompanyProvider.class).size(); i++) {
                    CompanyProvider companyProvider = CompanyProvider.listAll(CompanyProvider.class).get(i);
                    if (technician.getCompanyId() == companyProvider.getIdserveur()) {
                        selectedCampnayIndex = i;
                        break;
                    }
                }
                spinnercompanytechnician.setSelection(selectedCampnayIndex, true);
            }

            if (technician.getAgencyId() != 0) {
                int selectedAgenceIndex = -1;
                for (int i = 0; i < Agence.listAll(Agence.class).size(); i++) {
                    Agence agence = Agence.listAll(Agence.class).get(i);
                    if (technician.getAgencyId() == agence.getIdserveur()) {
                        selectedAgenceIndex = i;
                        break;
                    }
                }
                spinneragencetechnician.setSelection(selectedAgenceIndex, true);

            }
        }
    }

    void savetechnican() {
        String name = edittextnametechnician.getText().toString().trim();
        String surname = edittextsurnametechnician.getText().toString().trim();
        String startDate = textviewdatestarttechnician.getText().toString().trim();
        String endDate = textviewdateendtechnician.getText().toString().trim();

        if (!hasPhoto) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.msgChoixPhoto), Toast.LENGTH_LONG).show();
            return;
        }

        if (spinnerfonctiontechnician.getSelectedItemPosition() == AdapterView.INVALID_POSITION) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.msgChoixFctTec), Toast.LENGTH_LONG).show();
            return;
        }
        if (spinneragencetechnician.getSelectedItemPosition() == AdapterView.INVALID_POSITION) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.msgChoixAgence), Toast.LENGTH_LONG).show();
            return;
        }
        if (spinnercompanytechnician.getSelectedItemPosition() == AdapterView.INVALID_POSITION) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.msgChoixSociete), Toast.LENGTH_LONG).show();
            return;
        }
        if (spinnerresponsableaffaire.getSelectedItemPosition() == AdapterView.INVALID_POSITION) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.msgChoixResponsable), Toast.LENGTH_LONG).show();
            return;
        }

        if (name.equals("")) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.msgChoixNomTechnicien), Toast.LENGTH_SHORT).show();
            return;
        }
        if (surname.equals("")) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.msgChoixPrenomTechnicien), Toast.LENGTH_SHORT).show();
            return;
        }
        if (startDate.equals("")) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.msgChoixDateEntree), Toast.LENGTH_SHORT).show();
            return;
        }

        if (edittextmobilenumbertechnician.toString().equals("")) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.msgChoixNumero), Toast.LENGTH_SHORT).show();
            return;
        }

        if (startDate.equals("")) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.msgChoixDateEntree), Toast.LENGTH_SHORT).show();
            return;
        }
        if (!edittextemailtechnician.getText().toString().trim().equals("")) {
            if (!Utils.isValidEmailAddress(edittextemailtechnician.getText().toString().trim())) {
                Toast.makeText(getActivity(), getActivity().getString(R.string.msgChoixEmail), Toast.LENGTH_SHORT).show();
                return;
            }
        }

        technician.setStartDate(startDate);
        if (!endDate.equals("")) {
            technician.setEndDate(endDate);
        }
        technician.setName(name);
        technician.setSurName(surname);
        technician.setPhone(edittextmobilenumbertechnician.getText().toString());
        technician.setCompanyId(((CompanyProvider) spinnercompanytechnician.getSelectedItem()).getIdserveur());
        technician.setFunction(((Fonction) spinnerfonctiontechnician.getSelectedItem()).getFunctionLabel());
        technician.setFunctionId(((Fonction) spinnerfonctiontechnician.getSelectedItem()).getIdFunction());
        technician.setAgencyId(((Agence) spinneragencetechnician.getSelectedItem()).getIdserveur());
        technician.setBusinessResponsibleId(((BusinessManager) spinnerresponsableaffaire.getSelectedItem()).getServerId());
        technician.setRegistrationNumber(edittextnumbermatriculetechnician.getText().toString().trim());
        technician.setSocialSecurityNumber(edittextnumbersecuritytechnician.getText().toString().trim());
        technician.setEmail(edittextemailtechnician.getText().toString().trim());
        technician.setExperience(edittextexperience.getText().toString());
        technician.setFormation(edittextformation.getText().toString());
        if (technician.getId() == null) {
            technician.setEdit(false);
            technician.save();

            Toast.makeText(getActivity(), getActivity().getString(R.string.msgSucAjoutTech), Toast.LENGTH_SHORT).show();
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            RessourcesFragment ressourcesFragment = new RessourcesFragment();
            fragmentTransaction.replace(R.id.frame_container, ressourcesFragment);
            fragmentTransaction.commit();

        } else {
            technician.setEdit(true);
            technician.save();
            Toast.makeText(getActivity(), getActivity().getString(R.string.msgSucUpdateDate), Toast.LENGTH_SHORT).show();
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            RessourcesFragment ressourcesFragment = new RessourcesFragment();
            fragmentTransaction.replace(R.id.frame_container, ressourcesFragment);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onFragmentInteraction() {

        technician.setLocalImage(null);
        imageviewpictechnician.setImageDrawable(null);
    }


}




