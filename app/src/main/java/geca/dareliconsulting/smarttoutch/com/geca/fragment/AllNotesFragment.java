package geca.dareliconsulting.smarttoutch.com.geca.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.adapter.ListNoteAdapter;
import geca.dareliconsulting.smarttoutch.com.geca.model.Note;
import geca.dareliconsulting.smarttoutch.com.geca.singleton.Singleton;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Utils;

public class AllNotesFragment extends Fragment {

    public TextView titleAllFiche;
    public EditText searchnote;
    public ListView listViewFiche;
    List<Note> notes = new ArrayList<Note>();
    ListNoteAdapter listNotesAdapter;

    public AllNotesFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_all_notes, container, false);
        loadview(view);
        notes = Note.listAll(Note.class);
        listNotesAdapter = new ListNoteAdapter(getActivity(), (ArrayList<Note>) notes);
        listViewFiche.setAdapter(listNotesAdapter);
        listViewFiche.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Singleton.getInstance().setCurrentNote(notes.get(position));
                if (notes.get(position).getType().equals("VISITE_TECHNIQUE")) {
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.frame_container, new VisiteTechniqueFragment()).commit();
                } else if (notes.get(position).getType().equals("CERTIFICATION")) {
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.frame_container, new CertificationFragment()).commit();
                } else if (notes.get(position).getType().equals("QUIZ")) {
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.frame_container, new QuizFragment()).commit();
                }
                else if (notes.get(position).getType().equals("VISITE_SECURITE_ENVIRONNEMENT")) {
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.frame_container, new VisiteTechniqueFragment()).commit();
                }
            }
        });

        searchnote.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = searchnote.getText().toString().toLowerCase(Locale.getDefault());
                listNotesAdapter.filter(text);
            }
        });


        return view;
    }


    void loadview(View view) {
        titleAllFiche = ((TextView) view.findViewById(R.id.notetitle));
        Typeface tf = Utils.getHelveticaFont(getActivity());
        titleAllFiche.setTypeface(tf);
        // Search Fiche
        searchnote = ((EditText) view.findViewById(R.id.edittextsearchnote));
        // Listview Fiches
        listViewFiche = ((ListView) view.findViewById(R.id.listviewnotes));
    }


}
