package geca.dareliconsulting.smarttoutch.com.geca.fragment;


import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.adapter.RecoveryNumberAdapter;
import geca.dareliconsulting.smarttoutch.com.geca.model.Fonction;
import geca.dareliconsulting.smarttoutch.com.geca.model.MyProfession;
import geca.dareliconsulting.smarttoutch.com.geca.model.RecoveryNumber;
import geca.dareliconsulting.smarttoutch.com.geca.model.Technician;
import geca.dareliconsulting.smarttoutch.com.geca.model.WorkUnit;
import geca.dareliconsulting.smarttoutch.com.geca.singleton.Singleton;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Constants;
import geca.dareliconsulting.smarttoutch.com.geca.utils.ListViewUtils;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class CertifConclusionFragment extends Fragment implements SignatureDialogFragment.OnSignatureInteractionListener {
    TextView textViewGlobalScore, textViewFormerName, textViewTechName;
    TextView textViewBilanNC, textViewUONC, textViewUOC, textViewUOEx, textViewGravity, textViewTxRecoveryNow, textViewNcMineur, textViewNcMajeur, textViewNcSomme, textViewRecoveryMineur, textViewRecoveryMajeur, textViewRecoverySomme,
            textViewRecoveryNowMineur, textViewRecoveryNowMajeur, textViewRecoveryNowSomme, textViewCostClientMineur, textViewCostClientMajeur,
            textViewCostClientSomme, textViewCostInternMineur, textViewCostInternMajeur, textViewCostInternSomme, textViewCostRecoveryNowMineur,
            textViewCostRecoveryNowMajeur, textViewCostRecoveryNowSomme, textViewPenalityPrice, textViewTotalCost, textViewTIENC, textViewRecoveryTitle, textViewFitTitle, textViewFITNb;
    ListView recoveryList, fitList;


    Spinner spinnerFomerScore, spinnerRealFunction, spinnerCibleFunction;
    EditText editTextComment1, editTextComment2, editTextNextAudit;
    Switch switchAccordCertif, switchSecurityAlert;
    ImageButton imageButtonEditFormerSign, imageButtonDeleteFormerSign, imageButtonEditTechSign, imageButtonDeleteTechSign;
    ImageView formerSignView, techSignView;
    Button buttonValidate;
    String[] listNotes;
    static final int SIGNATURE_FORMER = 111;
    static final int SIGNATURE_TECH = 222;
    int currentSignature;
    String picSignAuditor, picSignTechnician;

    public OnCertifConclusionFragmentListener onCertifConclusionFragmentListener;

    public interface OnCertifConclusionFragmentListener {
        void onValidateAction();
    }

    public CertifConclusionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_certif_conclusion, container, false);
        loadview(view);
        loadViewsContent();
        addLayoutListener();
        Utils.enableDisableView(view, Singleton.getInstance().getCurrentNote().getStatus().equals("IN_PROGRESS"));
        if (Singleton.getInstance().getCurrentNote().getAuditorSignature() != null)
            picSignAuditor = Singleton.getInstance().getCurrentNote().getAuditorSignature();

        if (Singleton.getInstance().getCurrentNote().getTechnicianSignature() != null)
            picSignTechnician = Singleton.getInstance().getCurrentNote().getTechnicianSignature();
        return view;
    }

    @Override
    public void onSignatureFinished(Bitmap bitmap) {
        switch (currentSignature) {
            case SIGNATURE_FORMER: {
                formerSignView.setImageBitmap(bitmap);
                picSignAuditor = Utils.saveBitmapToFile(bitmap);
                break;
            }
            case SIGNATURE_TECH: {
                techSignView.setImageBitmap(bitmap);
                picSignTechnician = Utils.saveBitmapToFile(bitmap);
                break;
            }
        }
    }

    public void saveFiche() {
        Singleton.getInstance().getCurrentNote().setAuditorGlobalScore(spinnerFomerScore.getSelectedItemPosition());
        Singleton.getInstance().getCurrentNote().setGranted(switchAccordCertif.isChecked());
        Singleton.getInstance().getCurrentNote().setMotifGrant(editTextComment1.getText().toString().trim());
        //Singleton.getInstance().getCurrentNote().setTechnicianRealFunction(((Fonction) spinnerRealFunction.getSelectedItem()).getIdFunction());
        Singleton.getInstance().getCurrentNote().setTechnicianEvolutionProfession(((MyProfession) spinnerCibleFunction.getSelectedItem()).getIdServer());
        Singleton.getInstance().getCurrentNote().setTechnicianEvolutionProfessionComment(editTextComment2.getText().toString().trim());
        Singleton.getInstance().getCurrentNote().setAuditorSignature(picSignAuditor);
        Singleton.getInstance().getCurrentNote().setTechnicianSignature(picSignTechnician);
        Singleton.getInstance().getCurrentNote().setSecurityAlert(switchSecurityAlert.isChecked() ? "true" : "false");
        if (!editTextNextAudit.getText().toString().equals(""))
            Singleton.getInstance().getCurrentNote().setNextAudit(Integer.parseInt(editTextNextAudit.getText().toString()));
    }

    private void addLayoutListener() {
        imageButtonEditFormerSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                SignatureDialogFragment signatureDialogFragment = new SignatureDialogFragment();
                signatureDialogFragment.onSignatureInteractionListener = CertifConclusionFragment.this;
                signatureDialogFragment.show(fm, "SignatureDialogFragment");
                currentSignature = SIGNATURE_FORMER;
            }
        });
        imageButtonEditTechSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                SignatureDialogFragment signatureDialogFragment = new SignatureDialogFragment();
                signatureDialogFragment.onSignatureInteractionListener = CertifConclusionFragment.this;
                signatureDialogFragment.show(fm, "SignatureDialogFragment");
                currentSignature = SIGNATURE_TECH;
            }
        });
        imageButtonDeleteFormerSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                formerSignView.setImageBitmap(null);
                picSignAuditor = null;
            }
        });
        imageButtonDeleteTechSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                techSignView.setImageBitmap(null);
                picSignTechnician = null;
            }
        });
        buttonValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onCertifConclusionFragmentListener != null) {
                    onCertifConclusionFragmentListener.onValidateAction();
                }
            }
        });
    }

    void loadview(View view) {
        textViewGlobalScore = (TextView) view.findViewById(R.id.textviewGlobalScore);
        textViewFormerName = (TextView) view.findViewById(R.id.textviewNameFormer);
        textViewTechName = (TextView) view.findViewById(R.id.textviewNameTechnicien);

        textViewBilanNC = (TextView) view.findViewById(R.id.textViewBilanNC);
        textViewUONC = (TextView) view.findViewById(R.id.textViewNbUONC);
        textViewUOC = (TextView) view.findViewById(R.id.textViewNbUOC);
        textViewUOEx = (TextView) view.findViewById(R.id.textViewNbUOEx);
        textViewGravity = (TextView) view.findViewById(R.id.textViewGravity);
        textViewTxRecoveryNow = (TextView) view.findViewById(R.id.textViewTxRecoveryNow);
        textViewNcMineur = (TextView) view.findViewById(R.id.textViewNCMineur);
        textViewNcMajeur = (TextView) view.findViewById(R.id.textViewNbNCMajeur);
        textViewNcSomme = (TextView) view.findViewById(R.id.textViewNCSomme);
        textViewRecoveryMineur = (TextView) view.findViewById(R.id.textViewRecoveryMineur);
        textViewRecoveryMajeur = (TextView) view.findViewById(R.id.textViewRecoveryMajeur);
        textViewRecoverySomme = (TextView) view.findViewById(R.id.textViewRecoverySomme);
        textViewRecoveryNowMineur = (TextView) view.findViewById(R.id.textViewRecoveryNowMineur);
        textViewRecoveryNowMajeur = (TextView) view.findViewById(R.id.textViewRecoveryNowMajeur);
        textViewRecoveryNowSomme = (TextView) view.findViewById(R.id.textViewrecoveryNowSomme);
        textViewCostClientMineur = (TextView) view.findViewById(R.id.textViewCostClientMineur);
        textViewCostClientMajeur = (TextView) view.findViewById(R.id.textViewNbCostClientMajeur);
        textViewCostClientSomme = (TextView) view.findViewById(R.id.textViewCostClientSomme);
        textViewCostInternMineur = (TextView) view.findViewById(R.id.textViewCostInternMineur);
        textViewCostInternMajeur = (TextView) view.findViewById(R.id.textViewCostInternMajeur);
        textViewCostInternSomme = (TextView) view.findViewById(R.id.textViewCostInternSomme);
        textViewCostRecoveryNowMineur = (TextView) view.findViewById(R.id.textViewCostRecoveryNowMineur);
        textViewCostRecoveryNowMajeur = (TextView) view.findViewById(R.id.textViewCostRecoveryNowMajeur);
        textViewCostRecoveryNowSomme = (TextView) view.findViewById(R.id.textViewcostRecoveryNowSomme);
        textViewPenalityPrice = (TextView) view.findViewById(R.id.textViewPenalityPrice);
        textViewTotalCost = (TextView) view.findViewById(R.id.textViewTotalPrice);
        textViewTIENC = (TextView) view.findViewById(R.id.textViewTIENC);
        textViewRecoveryTitle = (TextView) view.findViewById(R.id.recoveryListTitle);
        textViewFitTitle = (TextView) view.findViewById(R.id.FITTitle);
        textViewFITNb = (TextView) view.findViewById(R.id.textViewFitNb);
        textViewTIENC = (TextView) view.findViewById(R.id.textViewTIENC);
        recoveryList = (ListView) view.findViewById(R.id.recoveryList);
        fitList = (ListView) view.findViewById(R.id.fitList);

        spinnerFomerScore = (Spinner) view.findViewById(R.id.spinnerFormateurScore);
        spinnerRealFunction = (Spinner) view.findViewById(R.id.spinnerRealFunction);
        spinnerCibleFunction = (Spinner) view.findViewById(R.id.spinnerJobEvolution);
        editTextComment1 = (EditText) view.findViewById(R.id.editTextComment1);
        editTextComment1.setVisibility(View.GONE);
        editTextComment2 = (EditText) view.findViewById(R.id.editTextComment2);
        editTextNextAudit = (EditText) view.findViewById(R.id.editTextNextAudit);
        switchAccordCertif = (Switch) view.findViewById(R.id.switchCertifidentificationAccorde);
        switchSecurityAlert = (Switch) view.findViewById(R.id.alertsecurity);
        imageButtonEditFormerSign = (ImageButton) view.findViewById(R.id.imageButtonEditSignFormer);
        imageButtonDeleteFormerSign = (ImageButton) view.findViewById(R.id.imageButtonRemoveSignFormer);
        imageButtonEditTechSign = (ImageButton) view.findViewById(R.id.imageButtonEditSignTechnicien);
        imageButtonDeleteTechSign = (ImageButton) view.findViewById(R.id.imageButtonRemoveSignTechnicien);
        formerSignView = (ImageView) view.findViewById(R.id.viewSignFormer);
        techSignView = (ImageView) view.findViewById(R.id.viewSignTechnicien);
        buttonValidate = (Button) view.findViewById(R.id.buttonValidateCertif);
        listNotes = getActivity().getResources().getStringArray(R.array.list_work_unit_certif);
        ArrayAdapter<String> adapterNotes = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, listNotes);
        spinnerFomerScore.setAdapter(adapterNotes);
        spinnerFomerScore.setSelection(Singleton.getInstance().getCurrentNote().getGlobalScore());
        switchAccordCertif.setChecked(Singleton.getInstance().getCurrentNote().isGranted());
        ArrayAdapter<Fonction> adapterRealFunction = new ArrayAdapter<Fonction>(getActivity(), R.layout.spinner_item, Fonction.listAll(Fonction.class));
        spinnerRealFunction.setAdapter(adapterRealFunction);
        ArrayAdapter<MyProfession> adapterEvolutionFunction = new ArrayAdapter<MyProfession>(getActivity(), R.layout.spinner_item, MyProfession.listAll(MyProfession.class));
        spinnerCibleFunction.setAdapter(adapterEvolutionFunction);
    }

    void loadViewsContent() {
        if (Singleton.getInstance().getCurrentNote().getGlobalScore() == 0) {
            textViewGlobalScore.setText(listNotes[Singleton.getInstance().getCurrentNote().getGlobalScore()]);
            textViewGlobalScore.setBackground(getActivity().getResources().getDrawable(R.color.gray));
        } else if (Singleton.getInstance().getCurrentNote().getGlobalScore() == 1) {
            textViewGlobalScore.setText(listNotes[Singleton.getInstance().getCurrentNote().getGlobalScore()]);
            textViewGlobalScore.setBackground(getActivity().getResources().getDrawable(R.color.Unrealized));
        } else if (Singleton.getInstance().getCurrentNote().getGlobalScore() == 2) {
            textViewGlobalScore.setText(listNotes[Singleton.getInstance().getCurrentNote().getGlobalScore()]);
            textViewGlobalScore.setBackground(getActivity().getResources().getDrawable(R.color.InProgress));
        } else {
            textViewGlobalScore.setText(listNotes[Singleton.getInstance().getCurrentNote().getGlobalScore()]);
            textViewGlobalScore.setBackground(getActivity().getResources().getDrawable(R.color.Realized));
        }
        editTextComment1.setText(Singleton.getInstance().getCurrentNote().getMotifGrant());
        editTextComment2.setText(Singleton.getInstance().getCurrentNote().getTechnicianEvolutionProfessionComment());
        editTextNextAudit.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getNextAudit()));
        switchSecurityAlert.setChecked(Singleton.getInstance().getCurrentNote().getSecurityAlert() != null && Singleton.getInstance().getCurrentNote().getSecurityAlert().equalsIgnoreCase("true"));

        int realFunction = 0;
        int evolutionProfession = 0;
        int indexFunction = -1;
        int indexProfesion = -1;
        if (Singleton.getInstance().getCurrentNote().getTechnicianRealFunction() != 0) {
            for (Fonction fonction : Fonction.listAll(Fonction.class)) {
                if (fonction.getIdFunction() == Singleton.getInstance().getCurrentNote().getTechnicianRealFunction()) {
                    realFunction = indexFunction;
                }
            }
            spinnerRealFunction.setSelection(realFunction);
        }
        if (Singleton.getInstance().getCurrentNote().getTechnicianEvolutionProfession() != 0) {
            for (MyProfession myProfession : MyProfession.listAll(MyProfession.class)) {
                if (myProfession.getIdServer() == Singleton.getInstance().getCurrentNote().getTechnicianEvolutionProfession()) {
                    evolutionProfession = indexProfesion;
                }
                indexProfesion++;
            }
            spinnerCibleFunction.setSelection(evolutionProfession);
        }
        spinnerRealFunction.setSelection(realFunction);
        spinnerCibleFunction.setSelection(evolutionProfession);

        String name = getActivity().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE).getString("firstname", "") + " " + getActivity().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE).getString("lastname", "");
        textViewFormerName.setText(name);
        Technician technician = Technician.findById(Technician.class, Singleton.getInstance().getCurrentNote().getTechnicianId());
        textViewTechName.setText(technician.getName() + " " + technician.getSurName());

        float bilan = 0;
        if (Singleton.getInstance().getCurrentNote().getControledWU() != 0)
            bilan = Singleton.getInstance().getCurrentNote().getNCWU() * 100 / Singleton.getInstance().getCurrentNote().getControledWU();
        textViewBilanNC.setText(String.format("%.2f", bilan) + "%");
        textViewUONC.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getNCWU()));
        textViewUOC.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getCWU()));
        textViewUOEx.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getExWU()));
        float gravity = 0;
        if (Singleton.getInstance().getCurrentNote().getControledWU() != 0)
            gravity = Singleton.getInstance().getCurrentNote().getNCWUMaj() * 100 / Singleton.getInstance().getCurrentNote().getControledWU();
        textViewGravity.setText(String.format("%.2f", gravity) + "%");
        float txRecovery = 0;
        if (Singleton.getInstance().getCurrentNote().getNCWU() != 0)
            txRecovery = Singleton.getInstance().getCurrentNote().getRecoveryNowWU() * 100 / Singleton.getInstance().getCurrentNote().getNCWU();
        textViewTxRecoveryNow.setText(String.format("%.2f", txRecovery) + "%");
        textViewPenalityPrice.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getPriceOfPenality()) + " €");
        textViewNcMineur.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getNCWUMin()));
        textViewNcMajeur.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getNCWUMaj()));
        textViewNcSomme.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getNCWU()));
        textViewRecoveryMineur.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getRecoveryWUMin()));
        textViewRecoveryMajeur.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getRecoveryWUMaj()));
        textViewRecoverySomme.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getRecoveryWU()));
        textViewRecoveryNowMineur.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getRecoveryNowWUMin()));
        textViewRecoveryNowMajeur.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getRecoveryNowWUMaj()));
        textViewRecoveryNowSomme.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getRecoveryNowWU()));
        textViewCostClientMineur.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getCostClientWUMin()) + " €");
        textViewCostClientMajeur.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getCostClientWUMaj()) + " €");
        textViewCostClientSomme.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getCostClientWU()) + " €");
        textViewCostInternMineur.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getCostInternWUMin()) + " €");
        textViewCostInternMajeur.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getCostInternWUMaj()) + " €");
        textViewCostInternSomme.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getCostInternWU()) + " €");
        textViewCostRecoveryNowMineur.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getCostRecoveryNowWUMin()) + " €");
        textViewCostRecoveryNowMajeur.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getCostRecoveryNowWUMaj()) + " €");
        textViewCostRecoveryNowSomme.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getCostRecoveryNowWU()) + " €");
        textViewTotalCost.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getTotalCost()) + " €");
        float TIENC = 0;
        /*if(Float.parseFloat(Singleton.getInstance().getCurrentNote().getSiteAmount())!=0)
            TIENC=Singleton.getInstance().getCurrentNote().getTotalCost()*100 / Float.parseFloat(Singleton.getInstance().getCurrentNote().getSiteAmount());
        */
        textViewTIENC.setText(String.format("%.2f", TIENC) + "%");

        //Recovery Delay
        if (Singleton.getInstance().getCurrentNote().getRecoveryNumbers().size() > 0) {
            textViewRecoveryTitle.setVisibility(View.VISIBLE);
            recoveryList.setVisibility(View.VISIBLE);
            ArrayList<RecoveryNumber> recoveries = new ArrayList<>();
            recoveries.add(null);
            recoveries.addAll(Singleton.getInstance().getCurrentNote().getRecoveryNumbers());
            RecoveryNumberAdapter adapter = new RecoveryNumberAdapter(getActivity(), recoveries);
            recoveryList.setAdapter(adapter);
            ListViewUtils.setListViewHeightBasedOnChildren(recoveryList);
        } else {
            textViewRecoveryTitle.setVisibility(View.GONE);
            recoveryList.setVisibility(View.GONE);
        }
        //FIT List
        if (Singleton.getInstance().getCurrentNote().getFITWU() > 0) {
            textViewFitTitle.setVisibility(View.VISIBLE);
            fitList.setVisibility(View.VISIBLE);
            ArrayList<WorkUnit> fits = Singleton.getInstance().getCurrentNote().getAllFIT();
            ArrayList<String> names = new ArrayList<>();
            for (WorkUnit wu : fits)
                names.add(wu.getLabel());
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1, names);
            //FitWUAdapter adapter = new FitWUAdapter(getActivity(), fits);
            fitList.setAdapter(adapter);
            ListViewUtils.setListViewHeightBasedOnChildren(fitList);
        } else {
            textViewFitTitle.setVisibility(View.GONE);
            fitList.setVisibility(View.GONE);
        }
        textViewFITNb.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getFITWU()));

        if (Singleton.getInstance().getCurrentNote().getAuditorSignature() != null && !Singleton.getInstance().getCurrentNote().getAuditorSignature().isEmpty()) {
            formerSignView.setImageBitmap(Utils.loadFullBitmapFromFile(Singleton.getInstance().getCurrentNote().getAuditorSignature()));
        } else {
            formerSignView.setImageBitmap(null);
        }
        if (Singleton.getInstance().getCurrentNote().getTechnicianSignature() != null && !Singleton.getInstance().getCurrentNote().getTechnicianSignature().isEmpty()) {
            techSignView.setImageBitmap(Utils.loadFullBitmapFromFile(Singleton.getInstance().getCurrentNote().getTechnicianSignature()));
        } else {
            techSignView.setImageBitmap(null);
        }
    }
}
