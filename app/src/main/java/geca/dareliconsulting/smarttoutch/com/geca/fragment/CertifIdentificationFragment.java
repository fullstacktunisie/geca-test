package geca.dareliconsulting.smarttoutch.com.geca.fragment;


import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.reginald.editspinner.EditSpinner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.model.Customer;
import geca.dareliconsulting.smarttoutch.com.geca.model.Note;
import geca.dareliconsulting.smarttoutch.com.geca.model.OperationDetail;
import geca.dareliconsulting.smarttoutch.com.geca.model.OperationType;
import geca.dareliconsulting.smarttoutch.com.geca.model.Technician;
import geca.dareliconsulting.smarttoutch.com.geca.singleton.Singleton;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Constants;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class CertifIdentificationFragment extends Fragment implements SearchDialogFragment.OnSearchInteractionListener {
    TextView textViewNoteId, textViewNoteState, textViewNoteSynched, textViewProfession, textViewDate, textViewAuditor, textViewTechFirstname, textViewTechLastname, textViewTechMatricule, textViewTechFonction;
    EditText editTextNoteName;
    Spinner spinnerClient, spinnerClient2;
    ImageView imageViewTechPhoto;
    private SimpleDateFormat dateFormatter;
    Button searchTechnician;
    private DatePickerDialog fromDatePickerDialog;
    Date noteDate = null;
    Technician technician;
    private Spinner noteOperationTypeSpinner;
    EditSpinner autoCompleteOperationDetail;
    private List<Customer> list;

    public CertifIdentificationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_certif_identification, container, false);
        dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        loadViews(view);
        addLayoutListener();
        Utils.enableDisableView(view, Singleton.getInstance().getCurrentNote().getStatus().equals("IN_PROGRESS"));
        return view;
    }

    void loadViews(View view) {
        textViewNoteId = (TextView) view.findViewById(R.id.textViewNoteId);
        editTextNoteName = (EditText) view.findViewById(R.id.editTextNoteName);
        textViewNoteState = (TextView) view.findViewById(R.id.textViewNoteState);
        textViewNoteSynched = (TextView) view.findViewById(R.id.textViewNoteSynched);
        textViewProfession = (TextView) view.findViewById(R.id.textViewProfession);
        textViewDate = (TextView) view.findViewById(R.id.textViewDate);
        textViewAuditor = (TextView) view.findViewById(R.id.textViewAuditor);
        spinnerClient = (Spinner) view.findViewById(R.id.spinnerClient);
        spinnerClient2 = (Spinner) view.findViewById(R.id.spinnerClient2);
        searchTechnician = (Button) view.findViewById(R.id.searchTechnician);
        textViewTechFirstname = (TextView) view.findViewById(R.id.textViewTechFirstname);
        textViewTechLastname = (TextView) view.findViewById(R.id.textViewTechLastname);
        textViewTechMatricule = (TextView) view.findViewById(R.id.textViewTechMatricule);
        textViewTechFonction = (TextView) view.findViewById(R.id.textViewTechFonction);
        imageViewTechPhoto = (ImageView) view.findViewById(R.id.imageViewTechPhoto);
        list = Customer.listAll(Customer.class);
        list.add(0, new Customer());
        ArrayAdapter<Customer> adapterCustomers = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, list);
        spinnerClient.setAdapter(adapterCustomers);
        ArrayAdapter<Customer> adapterCustomers2 = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, list);
        spinnerClient2.setAdapter(adapterCustomers2);

        noteOperationTypeSpinner = ((Spinner) view.findViewById(R.id.spinner_operation_typenote));
        autoCompleteOperationDetail = (EditSpinner) view.findViewById(R.id.autocomplete_operation_detail);

        // spinner operation type
        List<OperationType> opTypes = OperationType.listAll(OperationType.class);
        List<OperationDetail> opDetail = OperationDetail.listAll(OperationDetail.class);
        ArrayAdapter<OperationType> operationTypeArrayAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, opTypes);
        ArrayAdapter<OperationDetail> operationDetailArrayAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, opDetail);
        for (OperationDetail operationDetail : opDetail) {
            Log.e("TAG", operationDetail.getDescription());
        }
        noteOperationTypeSpinner.setAdapter(operationTypeArrayAdapter);
        autoCompleteOperationDetail.setAdapter(operationDetailArrayAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        loadViewsContent();
    }

    void loadViewsContent() {
        Note note = Singleton.getInstance().getCurrentNote();
        Log.d(Constants.TAG, "tech id" + note.getTechnicianId());
        if (Singleton.getInstance().getCurrentNote() == null) {
            Toast.makeText(getActivity(), "Note is null", Toast.LENGTH_LONG).show();
            return;
        }
        if (Singleton.getInstance().getCurrentNote().getId() != null) {
            textViewNoteId.setText("ID " + Singleton.getInstance().getCurrentNote().getId());
        }
        if (Singleton.getInstance().getCurrentNote().getStatus().equals("IN_PROGRESS")) {
            textViewNoteState.setText(getActivity().getString(R.string.enCours));
        } else if (Singleton.getInstance().getCurrentNote().getStatus().equals("VALIDE")) {
            textViewNoteState.setText(getActivity().getString(R.string.validee));
        } else {
            textViewNoteState.setText(getActivity().getString(R.string.abandonnee));
        }
        textViewNoteSynched.setText(Singleton.getInstance().getCurrentNote().isSynchronised() ? "Oui" : "Non");
        textViewProfession.setText(Singleton.getInstance().getCurrentNoteProfession().getLabel());
        //editTextAmountChantier.setText(Singleton.getInstance().getCurrentNote().getSiteAmount());
        String name = getActivity().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE).getString("firstname", "") + " " + getActivity().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE).getString("lastname", "");
        textViewAuditor.setText(name);
        if (Singleton.getInstance().getCurrentNote().getDate() != null) {
            textViewDate.setText(dateFormatter.format(Singleton.getInstance().getCurrentNote().getDate()));
        } else {
            noteDate = new Date();
            textViewDate.setText(dateFormatter.format(noteDate));
        }
        if (Singleton.getInstance().getCurrentNote().getTechnicianId() != null) {
            technician = Technician.findById(Technician.class, Singleton.getInstance().getCurrentNote().getTechnicianId());
            if (technician != null) {
                textViewTechFirstname.setText(technician.getName());
                textViewTechLastname.setText(technician.getSurName());
                textViewTechMatricule.setText(technician.getRegistrationNumber());
                textViewTechFonction.setText(technician.getFunction());
                try {
                    imageViewTechPhoto.setImageBitmap(Utils.loadSmallBitmapFromFile(technician.getLocalImage()));
                } catch (NullPointerException e) {
                } catch (IllegalArgumentException e) {
                }
            }
        }

        if (Singleton.getInstance().getCurrentNote().getCustomerId() != 0) {
            int customerIndex = -1;
            for (int i = 0; i < Customer.listAll(Customer.class).size(); i++) {
                Customer customer = Customer.listAll(Customer.class).get(i);
                Log.e("TAG", customer.getIdserveur() + "");
                if (customer.getIdserveur() == Singleton.getInstance().getCurrentNote().getCustomerId()) {
                    customerIndex = i;
                    break;
                }
            }
            spinnerClient.setSelection(customerIndex+1, true);
        }

        if (Singleton.getInstance().getCurrentNote().getCustomerId2() != 0) {
            int customerIndex = -1;
            for (int i = 0; i < Customer.listAll(Customer.class).size(); i++) {
                Customer customer = Customer.listAll(Customer.class).get(i);
                Log.e("TAG", customer.getIdserveur() + "");
                if (customer.getIdserveur() == Singleton.getInstance().getCurrentNote().getCustomerId2()) {
                    customerIndex = i;
                    break;
                }
            }
            spinnerClient2.setSelection(customerIndex+1, true);
        }

        if (Singleton.getInstance().getCurrentNote().getOperationsDetails() != null) {
            autoCompleteOperationDetail.setText(Singleton.getInstance().getCurrentNote().getOperationsDetails());
        }

        noteOperationTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerClient.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                updateNoteName();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerClient2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                updateNoteName();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        updateNoteName();
    }

    private void updateNoteName() {
        if (!Singleton.getInstance().getCurrentNote().getStatus().equals("IN_PROGRESS")) {
            editTextNoteName.setText(Singleton.getInstance().getCurrentNote().getLabel());
            return;
        }
        String noteName = "Cert";
        if (!TextUtils.isEmpty(textViewProfession.getText())) {
            noteName += "_" + textViewProfession.getText().toString();
        }

        if (technician != null) {
            noteName += "_" + technician.getName() + "-" + technician.getSurName();
        }

        if (textViewDate.getText() != null) {
            noteName += "_" + textViewDate.getText().toString().replace("/", "-");
        }

        if (spinnerClient.getSelectedItemPosition() != Spinner.INVALID_POSITION) {
            noteName += "_" + ((Customer) spinnerClient.getSelectedItem()).getCustomerName();
        }

        if (spinnerClient2.getSelectedItemPosition() != Spinner.INVALID_POSITION) {
            noteName += "_" + ((Customer) spinnerClient2.getSelectedItem()).getCustomerName();
        }


        editTextNoteName.setText(noteName);
    }

    private void addLayoutListener() {
        Calendar newCalendar = Calendar.getInstance();
        textViewDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                fromDatePickerDialog.show();
            }
        });
        fromDatePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        noteDate = newDate.getTime();
                        textViewDate.setText(dateFormatter.format(noteDate));
                        updateNoteName();
                    }

                },
                newCalendar.get(Calendar.YEAR),
                newCalendar.get(Calendar.MONTH),
                newCalendar.get(Calendar.DAY_OF_MONTH)
        );
        searchTechnician.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                SearchDialogFragment searchDialogFragment = new SearchDialogFragment();
                searchDialogFragment.onSearchInteractionListener = CertifIdentificationFragment.this;
                searchDialogFragment.show(fm, "SignatureDialogFragment");
            }
        });
    }

    public boolean saveNote() {
        if (noteOperationTypeSpinner.getSelectedItemPosition() == AdapterView.INVALID_POSITION || editTextNoteName.getText().toString().equals("") || textViewDate.getText().toString().equals("") || spinnerClient.getSelectedItemPosition() == AdapterView.INVALID_POSITION || technician == null) {
            Toast.makeText(getActivity().getApplicationContext(), getActivity().getString(R.string.msgChampsObl), Toast.LENGTH_SHORT).show();
            return false;
        } else {

            String noteLabel = editTextNoteName.getText().toString();
            Customer customer = (Customer) spinnerClient.getSelectedItem();

            Customer customer2 = (Customer) spinnerClient2.getSelectedItem();
            Singleton.getInstance().getCurrentNote().setLabel(noteLabel);
            Singleton.getInstance().getCurrentNote().setCustomerId(customer.getIdserveur());
            Singleton.getInstance().getCurrentNote().setCustomerId2(customer2.getIdserveur());
            Singleton.getInstance().getCurrentNote().setOperationTypeId(((OperationType) (noteOperationTypeSpinner.getSelectedItem())).getServerId());
            Singleton.getInstance().getCurrentNote().setOperationsDetails(autoCompleteOperationDetail.getText().toString());
            try {
                Singleton.getInstance().getCurrentNote().setDate(dateFormatter.parse(textViewDate.getText().toString().trim()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Singleton.getInstance().getCurrentNote().setTechnicianId(technician.getId());
            Singleton.getInstance().getCurrentNote().setTechnicianServerId(technician.getServerId());
            Singleton.getInstance().getCurrentNote().setSynchronised(false);
            return true;
        }
    }

    @Override
    public void onSearchListFinished(ArrayList<Technician> technicians) {
        if (technicians != null) {
            for (int i = 0; i < technicians.size(); i++) {
                this.technician = technicians.get(i);
                textViewTechFirstname.setText(technician.getName());
                textViewTechLastname.setText(technician.getSurName());
                textViewTechMatricule.setText(technician.getRegistrationNumber());
                textViewTechFonction.setText(technician.getFunction());
                try {
                    imageViewTechPhoto.setImageBitmap(Utils.loadSmallBitmapFromFile(technician.getLocalImage()));
                } catch (NullPointerException | IllegalArgumentException e) {
                    e.printStackTrace();
                }
            }
        }
        updateNoteName();
    }

}
