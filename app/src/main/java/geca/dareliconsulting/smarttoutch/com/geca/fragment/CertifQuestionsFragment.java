package geca.dareliconsulting.smarttoutch.com.geca.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.adapter.CertifDataSetAdapter;
import geca.dareliconsulting.smarttoutch.com.geca.model.Answer;
import geca.dareliconsulting.smarttoutch.com.geca.model.Skill;
import geca.dareliconsulting.smarttoutch.com.geca.model.TypeSkill;
import geca.dareliconsulting.smarttoutch.com.geca.model.WorkUnit;
import geca.dareliconsulting.smarttoutch.com.geca.singleton.Singleton;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Constants;

/**
 * A simple {@link Fragment} subclass.
 */
public class CertifQuestionsFragment extends Fragment {

    ArrayList<Answer> answersList;
    ArrayList<Object> dataset;
    ListView listView;
    String alertsecurity = "false";
    CertifDataSetAdapter adapter;


    public CertifQuestionsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_certif_questions, container, false);
        answersList = new ArrayList<>();
        Log.d("", "onCreateView: " + Singleton.getInstance().getCurrentNoteProfession().getTypeSkills().size());
        dataset = new ArrayList<>();
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            if (Singleton.getInstance().getCurrentNote().getListTypeSkillNotApplicable() != null) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(Singleton.getInstance().getCurrentNote().getListTypeSkillNotApplicable());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        if (jsonArray.getLong(i) == typeSkill.getIdTypeSkill()) {
                            typeSkill.setNotApplicable(true);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            if (Singleton.getInstance().getCurrentNote().getListTypeSkillWithSecurityAlert() != null) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(Singleton.getInstance().getCurrentNote().getListTypeSkillWithSecurityAlert());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        if (jsonArray.getLong(i) == typeSkill.getIdTypeSkill()) {
                            typeSkill.setWithAlertSecurity(true);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            boolean firstType = true;
            for (Skill skill : typeSkill.getSkills()) {
                if (Singleton.getInstance().getCurrentNote().getListSkillisNotApplicable() != null) {
                    JSONArray jsonArray = null;
                    try {
                        jsonArray = new JSONArray(Singleton.getInstance().getCurrentNote().getListSkillisNotApplicable());
                        for (int i = 0; i < jsonArray.length(); i++) {
                            if (jsonArray.getLong(i) == skill.getId()) {
                                skill.setNotApplicable(true);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                //dataset.add(skill);
                boolean first = true;
                for (WorkUnit workUnit : skill.getWorkUnits()) {
                    //if (workUnit.getAnswer() == null) {
                    if (Singleton.getInstance().getCurrentNote().getId() != null) {
                        List<Answer> answers = Answer.find(Answer.class, "note_id = ? and skill_id = ? and type_skill_id = ? and work_unit_id = ?", "" + Singleton.getInstance().getCurrentNote().getId(), "" + skill.getId(), "" + typeSkill.getIdTypeSkill(), "" + workUnit.getId());
                        if (answers != null && answers.size() > 0) {
                            workUnit.setAnswer(answers.get(0));
                        }
                    }
                    if (workUnit.isCertification()) {
                        if (workUnit.getAnswer() == null) {
                            Answer answer = new Answer();
                            answer.setHasRecovery(false);
                            answer.setComment("");
                            answer.setScore(0);
                            answer.setSkillId(skill.getId());
                            answer.setTypeSkillId(typeSkill.getIdTypeSkill());
                            answer.setWorkUnitId(workUnit.getId());
                            workUnit.setAnswer(answer);
                        } else {
                            if (workUnit.getAnswer().isTypeSkillNotApplicable()) {
                                typeSkill.setNotApplicable(true);
                            }
                            if (workUnit.getAnswer().isSkillNotApplicable()) {
                                skill.setNotApplicable(true);
                            }
                        }
                        if (firstType) {
                            dataset.add(typeSkill);
                            firstType = false;
                        }
                        if (first) {
                            dataset.add(skill);
                            first = false;
                        }
                        dataset.add(workUnit);
                    }
                }
            }
        }
        Log.e("dataset ", "onCreateView: "+dataset.size() );
        listView = (ListView) view.findViewById(R.id.listView);
        /*View footer = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dummy_listview_footer, null);
        listView.addFooterView(footer);*/
        adapter = new CertifDataSetAdapter(getActivity(), dataset);
        listView.setAdapter(adapter);
        return view;
    }

    public void saveAnswersInDB() {
        Singleton.getInstance().getCurrentNote().save();
        int score = 3;
        JSONArray jsonArrayTypeSkillNotApplicable = new JSONArray();
        JSONArray jsonArrayTypeSkillWithSecurityAlert = new JSONArray();
        boolean hasWorkunits = false;
        for (Object o : dataset) {
            if ((o instanceof TypeSkill) && ((TypeSkill) o).isWithAlertSecurity()) {
                alertsecurity = "true";
                jsonArrayTypeSkillWithSecurityAlert.put(((TypeSkill) o).getIdTypeSkill());
                if (((TypeSkill) o).isNotApplicable()) {
                    jsonArrayTypeSkillNotApplicable.put(((TypeSkill) o).getIdTypeSkill());
                }
            }

            if ((o instanceof WorkUnit) && !((WorkUnit) o).isNotApplicable()) {
                ((WorkUnit) o).getAnswer().setNoteId(Singleton.getInstance().getCurrentNote().getId());
                ((WorkUnit) o).getAnswer().save();
                if (((WorkUnit) o).getAnswer().getScore() != 0) {
                    score = Math.min(score, ((WorkUnit) o).getAnswer().getScore());
                    hasWorkunits = true;
                }
            }

        }
        if (hasWorkunits) {
            Singleton.getInstance().getCurrentNote().setGlobalScore(score);
            Singleton.getInstance().getCurrentNote().setListTypeSkillNotApplicable(jsonArrayTypeSkillNotApplicable.toString());
            Singleton.getInstance().getCurrentNote().setListTypeSkillWithSecurityAlert(jsonArrayTypeSkillWithSecurityAlert.toString());
            Singleton.getInstance().getCurrentNote().save();
        }
        Log.d(Constants.TAG, "answers count : " + Answer.count(Answer.class, null, null));
    }

    public void saveNoteScore() {
        int score = 0;
        int size = 0;
        for (Object o : dataset) {
            if (o instanceof Skill) {
                if(!((Skill) o).isNotApplicable()) {
                    score += ((Skill) o).getScore();
                    size++;
                }
            }
        }
        int total = Math.round(score / size);
        Singleton.getInstance().getCurrentNote().setGlobalScore(total);
    }

    public void notifyErrors() {
        adapter.notifyDataSetChanged();
    }

    public boolean isValid() {
        boolean isValid = true;
        for (Object o : dataset) {
            if (o instanceof Skill) {
                if (!((Skill) o).isNotApplicable() && ((Skill) o).getScore() == -1)
                    isValid = false;
            }
        }
        return isValid;
    }


}
