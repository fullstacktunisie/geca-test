package geca.dareliconsulting.smarttoutch.com.geca.fragment;


import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.Date;
import java.util.List;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.activity.MainActivity;
import geca.dareliconsulting.smarttoutch.com.geca.model.AbandonReason;
import geca.dareliconsulting.smarttoutch.com.geca.model.Answer;
import geca.dareliconsulting.smarttoutch.com.geca.model.Note;
import geca.dareliconsulting.smarttoutch.com.geca.singleton.Singleton;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class CertificationFragment extends BaseFragment implements CertifConclusionFragment.OnCertifConclusionFragmentListener, AbandonCauseDialogFragment.OnAbandonCauseInteractionListener {

    FragmentManager fm;
    FragmentTransaction fragmentTransaction;
    public CertifIdentificationFragment certifIdentificationFragment;
    public CertifQuestionsFragment certifQuestionsFragment;
    public CertifConclusionFragment certifConclusionFragment;
    public int count = 0;

    public CertificationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = super.onCreateView(inflater, container, savedInstanceState);
        textViewTitle.setText("Fiche de certification");
        count = 0;
        fm = getFragmentManager();
        fragmentTransaction = fm.beginTransaction();
        certifIdentificationFragment = new CertifIdentificationFragment();
        fragmentTransaction.replace(R.id.content, certifIdentificationFragment, "certifIdentificationFragment");
        fragmentTransaction.commit();
        addLayoutListener();
        imageViewLeft.setVisibility(View.INVISIBLE);
        imageViewRight.setVisibility(View.VISIBLE);
        if (Singleton.getInstance().getCurrentNote().getId() != null) {
            textvDuplicate.setVisibility(View.VISIBLE);
        }
        return view;
    }

    private void addLayoutListener() {
        imageViewLeft.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                switch (count) {
                    case 1: {
                        count = count - 1;
                        fm = getFragmentManager();
                        fragmentTransaction = fm.beginTransaction();
                        if (certifIdentificationFragment == null) {
                            certifIdentificationFragment = new CertifIdentificationFragment();
                        }
                        fragmentTransaction.replace(R.id.content, certifIdentificationFragment, "certifIdentificationFragment");
                        fragmentTransaction.commit();
                        imageViewLeft.setVisibility(View.INVISIBLE);
                        imageViewRight.setVisibility(View.VISIBLE);
                        break;
                    }
                    case 2: {
                        count = count - 1;
                        fragmentTransaction = fm.beginTransaction();
                        if (certifQuestionsFragment == null) {
                            certifQuestionsFragment = new CertifQuestionsFragment();
                        }
                        fragmentTransaction.replace(R.id.content, certifQuestionsFragment, "certifQuestionsFragment");
                        fragmentTransaction.commit();
                        imageViewLeft.setVisibility(View.VISIBLE);
                        imageViewRight.setVisibility(View.VISIBLE);
                        break;
                    }
                    default:
                        break;
                }
            }
        });
        imageViewRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (count) {
                    case 0:
                        if (certifIdentificationFragment.saveNote()) {
                            count = count + 1;
                            fragmentTransaction = fm.beginTransaction();
                            if (certifQuestionsFragment == null) {
                                certifQuestionsFragment = new CertifQuestionsFragment();
                            }
                            fragmentTransaction.replace(R.id.content, certifQuestionsFragment, "certifQuestionsFragment");
                            fragmentTransaction.commit();
                            imageViewLeft.setVisibility(View.VISIBLE);
                            imageViewRight.setVisibility(View.VISIBLE);
                        }
                        break;

                    case 1:
                        try {
                            certifQuestionsFragment.saveNoteScore();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        boolean actions = Singleton.getInstance().getCurrentNote().validActionPlans();
                        boolean comments = Singleton.getInstance().getCurrentNote().validComments();

                        boolean checked = certifQuestionsFragment.isValid();

                        if (!checked) {
                            new AlertDialog.Builder(getActivity()).setMessage(getString(R.string.selectOneAndComment1))
                                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    }).show();
                        } else if (actions && comments) {
                            certifQuestionsFragment.notifyErrors();
                            count = count + 1;
                            imageViewLeft.setVisibility(View.VISIBLE);
                            imageViewRight.setVisibility(View.INVISIBLE);
                            fragmentTransaction = fm.beginTransaction();
                            if (certifConclusionFragment == null) {
                                certifConclusionFragment = new CertifConclusionFragment();
                                certifConclusionFragment.onCertifConclusionFragmentListener = CertificationFragment.this;
                            }
                            fragmentTransaction.replace(R.id.content, certifConclusionFragment, "certifConclusionFragment");
                            fragmentTransaction.commit();
                        } else {
                            new AlertDialog.Builder(getActivity()).setMessage(getString(R.string.selectOneAndComment))
                                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    }).show();
                            certifQuestionsFragment.notifyErrors();
                        }
                        break;
                    default:
                        break;
                }
            }

        });
    }

    public boolean saveNoteInDb(String state) {
        if (certifIdentificationFragment.saveNote()) {
            try {
                if (certifQuestionsFragment != null) {
                    certifQuestionsFragment.saveNoteScore();
                    certifQuestionsFragment.saveAnswersInDB();
                }
                if (certifConclusionFragment != null) {
                    certifConclusionFragment.saveFiche();
                }
                if (Singleton.getInstance().location != null) {
                    Singleton.getInstance().getCurrentNote().setLongitude(Singleton.getInstance().location.getLongitude());
                    Singleton.getInstance().getCurrentNote().setLatitude(Singleton.getInstance().location.getLatitude());
                }
                Singleton.getInstance().getCurrentNote().setStatus(state);
                Singleton.getInstance().getCurrentNote().setUpdated((new Date()).getTime());
                Singleton.getInstance().getCurrentNote().save();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getActivity(), getActivity().getString(R.string.msgErrorEnregistrement), Toast.LENGTH_SHORT).show();
            }
        }
        return false;
    }

    @Override
    public void onValidateAction() {
        if (saveNoteInDb("VALIDE")) {
            Singleton.getInstance().setCurrentNote(null);
            ((MainActivity) getActivity()).displayView(1);
        }
    }

    @Override
    public void saveNoteAction() {
        if (Singleton.getInstance().getCurrentNote().getStatus().equals("IN_PROGRESS")) {
            if (saveNoteInDb("IN_PROGRESS")) {
                Singleton.getInstance().setCurrentNote(null);
                ((MainActivity) getActivity()).displayView(1);
            }
        }
    }

    @Override
    public void abandonNoteAction() {
        if (Singleton.getInstance().getCurrentNote().getStatus().equals("IN_PROGRESS")) {
            FragmentManager fm = getFragmentManager();
            AbandonCauseDialogFragment abandonCauseDialogFragment = new AbandonCauseDialogFragment();
            abandonCauseDialogFragment.onAbandonCauseInteractionListener = CertificationFragment.this;
            abandonCauseDialogFragment.show(fm, "AbandonCauseDialogFragment");
        }
    }

    @Override
    public void onConfirmAction(AbandonReason abandonReason) {
        Singleton.getInstance().getCurrentNote().setMotifOfAbandonment(abandonReason.getName());
        if (saveNoteInDb("ABANDON")) {
            Singleton.getInstance().setCurrentNote(null);
            ((MainActivity) getActivity()).displayView(1);
        }
    }

    @Override
    public void duplicateNoteAction() {
        super.duplicateNoteAction();
        if (Singleton.getInstance().getCurrentNote() != null) {
            if (Singleton.getInstance().getCurrentNote().getStatus().equals("IN_PROGRESS")) {
                if (!saveNoteInDb("IN_PROGRESS")) {
                    return;
                }
            }
            Note note = new Note();
            note.setTechnicianServerId(Singleton.getInstance().getCurrentNote().getTechnicianServerId());
            note.setTechnicianId(Singleton.getInstance().getCurrentNote().getTechnicianId());
            note.setStatus("IN_PROGRESS");
            note.setDuration(Singleton.getInstance().getCurrentNote().getDuration());
            note.setAuditorId(Singleton.getInstance().getCurrentNote().getAuditorId());
            note.setCustomerId(Singleton.getInstance().getCurrentNote().getCustomerId());
            note.setCustomerId2(Singleton.getInstance().getCurrentNote().getCustomerId2());
            note.setOperationsDetails(Singleton.getInstance().getCurrentNote().getOperationsDetails());
            note.setSiteNumber(Singleton.getInstance().getCurrentNote().getSiteNumber());
            note.setOtNumber(Singleton.getInstance().getCurrentNote().getOtNumber());
            note.setLabel(Singleton.getInstance().getCurrentNote().getLabel());
            note.setSectorId(Singleton.getInstance().getCurrentNote().getSectorId());
            note.setProfessionFamilyId(Singleton.getInstance().getCurrentNote().getProfessionFamilyId());
            note.setProfessionId(Singleton.getInstance().getCurrentNote().getProfessionId());
            note.setTechnicianPresent(Singleton.getInstance().getCurrentNote().isTechnicianPresent());
            note.setDate(Singleton.getInstance().getCurrentNote().getDate());
            note.setCreated(Singleton.getInstance().getCurrentNote().getCreated());
            note.setType(Singleton.getInstance().getCurrentNote().getType());
            note.setPicture1(Singleton.getInstance().getCurrentNote().getPicture1());
            note.setPicture2(Singleton.getInstance().getCurrentNote().getPicture2());
            note.setPicture3(Singleton.getInstance().getCurrentNote().getPicture3());
            note.setComment1(Singleton.getInstance().getCurrentNote().getComment2());
            note.setComment2(Singleton.getInstance().getCurrentNote().getComment2());
            note.setComment3(Singleton.getInstance().getCurrentNote().getComment3());
            note.setGlobalScore(Singleton.getInstance().getCurrentNote().getGlobalScore());
            note.setPriceOfNonconformity(Singleton.getInstance().getCurrentNote().getPriceOfNonconformity());
            note.setAuditorGlobalScore(Singleton.getInstance().getCurrentNote().getGlobalScore());
            note.setGranted(Singleton.getInstance().getCurrentNote().isGranted());
            note.setMotifGrant(Singleton.getInstance().getCurrentNote().getMotifGrant());
            note.setTechnicianRealFunction(Singleton.getInstance().getCurrentNote().getTechnicianRealFunction());
            note.setTechnicianEvolutionProfession(Singleton.getInstance().getCurrentNote().getTechnicianEvolutionProfession());
            note.setTechnicianEvolutionProfessionComment(Singleton.getInstance().getCurrentNote().getTechnicianEvolutionProfessionComment());
            note.setTechnicianSignature(Singleton.getInstance().getCurrentNote().getTechnicianSignature());
            note.setAuditorSignature(Singleton.getInstance().getCurrentNote().getAuditorSignature());
            note.setSiteAmount(Singleton.getInstance().getCurrentNote().getSiteAmount());
            note.setOperationTypeId(Singleton.getInstance().getCurrentNote().getOperationTypeId());

            note.save();
            List<Answer> answers = Answer.find(Answer.class, "note_id = ?", "" + Singleton.getInstance().getCurrentNote().getId());
            for (Answer answer : answers) {
                Answer newanswer = new Answer();
                newanswer.setComment(answer.getComment());
                newanswer.setNoteId(note.getId());
                newanswer.setPicture(answer.getPicture());
                newanswer.setScore(answer.getScore());
                newanswer.setSkillId(answer.getSkillId());
                newanswer.setTypeSkillId(answer.getTypeSkillId());
                newanswer.setWorkUnitId(answer.getWorkUnitId());
                newanswer.setSkillId(answer.getSkillId());
                newanswer.setHasRecovery(answer.isHasRecovery());
                newanswer.setNowRecovery(answer.isNowRecovery());
                newanswer.setActionPlans(answer.getActionPlansString());
                newanswer.setRecoverypictures(answer.getRecoverypicturesString());
                newanswer.save();
            }
            //Singleton.getInstance().setCurrentNote(null);
            try {
                Utils.loadDatasInObjcets(getActivity());
            } catch (Exception e) {
            }
            Singleton.getInstance().setCurrentNote(note);
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.frame_container, new CertificationFragment()).commit();
        }

    }
}
