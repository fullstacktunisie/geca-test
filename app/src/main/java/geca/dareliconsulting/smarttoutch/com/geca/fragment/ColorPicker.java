package geca.dareliconsulting.smarttoutch.com.geca.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Constants;


public class ColorPicker extends DialogFragment {

    public OnColorPickerListener onColorPickerListener;

    public ColorPicker() {

        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        View view = getActivity().getLayoutInflater().inflate(R.layout.layout_color_picker, null);
        Dialog alertDialog = new Dialog(getActivity(), R.style.TransparentDialog);
        alertDialog.setContentView(view);

        GridView gridView = (GridView) view.findViewById(R.id.grid_view);
        gridView.setAdapter(new ColorAdapter(getActivity(), Constants.BASIC_COLORS));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(onColorPickerListener != null) {
                    onColorPickerListener.onColorPicked(Constants.BASIC_COLORS[position]);
                }
                dismiss();
            }
        });
        return alertDialog;
    }


    public interface OnColorPickerListener {
        // TODO: Update argument type and name
        void onColorPicked(String color);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        Window window = getDialog().getWindow();
        window.setLayout(getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels);
        window.setGravity(Gravity.CENTER);
    }

    public class ColorAdapter extends BaseAdapter {

        private final Activity context;
        protected String[] colors;

        public View view;

        public ColorAdapter(Activity context, String[] colors) {
            this.context = context;
            this.colors = colors;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            LayoutInflater inflater = context.getLayoutInflater();
            TextView rowView = (TextView)inflater.inflate(R.layout.color_item, null, true);
            rowView.setText(colors[position]);
            rowView.setBackgroundColor(Color.parseColor(colors[position]));
            return rowView;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public Object getItem(int position) {
            return colors[position];
        }

        @Override
        public int getCount() {
            return colors.length;
        }
    }
}