package geca.dareliconsulting.smarttoutch.com.geca.fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Utils;
import geca.dareliconsulting.smarttoutch.com.geca.view.ScaleSmartImageView;

public class FullScreenPictureDialogFragment extends DialogFragment {

    public OnFragmentInteractionListener onFragmentInteractionListener;
     String url;


    public FullScreenPictureDialogFragment() {

        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static FullScreenPictureDialogFragment newInstance(String url) {
        FullScreenPictureDialogFragment fragment = new FullScreenPictureDialogFragment();
        Bundle args = new Bundle();
        args.putString("url", url);
        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        url = getArguments().getString("url");

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
         super.onCreateDialog(savedInstanceState);

        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_full_screen_picture_dialog, null);
        Dialog alertDialog = new Dialog(getActivity(), R.style.TransparentDialog);
        alertDialog.setContentView(view);

        if (url != null) {
            ScaleSmartImageView imageView = (ScaleSmartImageView)view.findViewById(R.id.imageView);
            imageView.setImageBitmap(Utils.loadSmallBitmapFromFile(url));

        }

        Button deleteButton = (Button) view.findViewById(R.id.deleteButton);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("clickDelete", "clickDelete");

                if (onFragmentInteractionListener != null) {
                    onFragmentInteractionListener.onFragmentInteraction();
                    dismiss();
                }

            }
        });

        return alertDialog;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction( );
    }

    @Override
    public void onResume()
    {
        super.onResume();
        Window window = getDialog().getWindow();
        window.setLayout(getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels);
        window.setGravity(Gravity.CENTER);
    }
}
