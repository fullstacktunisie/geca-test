package geca.dareliconsulting.smarttoutch.com.geca.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.adapter.AccompagnementAdapter;
import geca.dareliconsulting.smarttoutch.com.geca.model.RequestTraining;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Utils;


public class ListAccompagnementFragment extends Fragment {


    AccompagnementAdapter accompagnementAdapter;
    private View view;
    private TextView textviewtitleaccompagnement;
    private Button iButtonAddAccompgnament;
    private EditText edittextsearchAccompgnament;
    private ListView listviewaccompagnement;


    public ListAccompagnementFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragmentlistaccompagnement, container, false);
        loadViews(view);
        final List<RequestTraining> requestTrainings = RequestTraining.listAll(RequestTraining.class);
        accompagnementAdapter = new AccompagnementAdapter(getActivity(), (ArrayList<RequestTraining>) requestTrainings);
        listviewaccompagnement.setAdapter(accompagnementAdapter);
        listviewaccompagnement.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    AddAccompagnementFragment addAccompagnementFragment = AddAccompagnementFragment.newInstance(requestTrainings.get(position).getId());
                    fragmentTransaction.replace(R.id.frame_container, addAccompagnementFragment);
                    fragmentTransaction.commit();

            }
        });

        iButtonAddAccompgnament.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                AddAccompagnementFragment addAccompagnementFragment = new AddAccompagnementFragment();
                fragmentTransaction.replace(R.id.frame_container, addAccompagnementFragment);
                fragmentTransaction.commit();
            }
        });


        edittextsearchAccompgnament.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = edittextsearchAccompgnament.getText().toString().toLowerCase(Locale.getDefault());
                accompagnementAdapter.filter(text);
            }
        });


        return view;
    }


    void loadViews(View view) {
        iButtonAddAccompgnament = ((Button) view.findViewById(R.id.addaccompagnement));
        edittextsearchAccompgnament = ((EditText) view.findViewById(R.id.edittextsearchaccompagnement));
        textviewtitleaccompagnement = ((TextView) view.findViewById(R.id.accompagnementstitle));
        Typeface tf = Utils.getHelveticaFont(getActivity());
        textviewtitleaccompagnement.setTypeface(tf);
        listviewaccompagnement = ((ListView) view.findViewById(R.id.listviewaccompagbnement));
    }



}
