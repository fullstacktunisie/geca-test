package geca.dareliconsulting.smarttoutch.com.geca.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import geca.dareliconsulting.smarttoutch.com.geca.R;

public class MainFragment extends Fragment {

    public ImageView imageviewnews;
    public ImageView imageviewmynote;
    public ImageView imageviewaccompagnement;
    public ImageView imageviewressources;
    public ImageView imageviewsynchronisation;
    public ImageView imageviewparametre;
    public View rootView;

    public MainFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_main, container, false);
        imageviewnews = ((ImageView) rootView.findViewById(R.id.imageviewnews));
        imageviewmynote = ((ImageView) rootView.findViewById(R.id.imageviewmynote));
        imageviewaccompagnement = ((ImageView) rootView.findViewById(R.id.imageviewaccompagnement));
        imageviewressources = ((ImageView) rootView.findViewById(R.id.imageviewressources));
        imageviewsynchronisation = ((ImageView) rootView.findViewById(R.id.imageviewsynchronisation));
        imageviewparametre = ((ImageView) rootView.findViewById(R.id.imageviewparametre));
        imageviewressources.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                RessourcesFragment oFRessource = new RessourcesFragment();
                fragmentTransaction.replace(R.id.frame_container, oFRessource);
                fragmentTransaction.commit();
            }
        });
        imageviewnews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                AddNoteFragment oFnote = new AddNoteFragment();
                fragmentTransaction.replace(R.id.frame_container, oFnote);
                fragmentTransaction.commit();
            }
        });

        imageviewmynote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                AllNotesFragment oAnotes = new AllNotesFragment();
                fragmentTransaction.replace(R.id.frame_container, oAnotes);
                fragmentTransaction.commit();
            }
        });

        imageviewaccompagnement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                ListAccompagnementFragment OfAcc = new ListAccompagnementFragment();
                fragmentTransaction.replace(R.id.frame_container, OfAcc);
                fragmentTransaction.commit();
            }
        });

        imageviewsynchronisation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                SynchronisationFragment OfSynch = new SynchronisationFragment();
                fragmentTransaction.replace(R.id.frame_container, OfSynch);
                fragmentTransaction.commit();
            }
        });
        imageviewparametre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                ParametresFragment OfParmetre = new ParametresFragment();
                fragmentTransaction.replace(R.id.frame_container, OfParmetre);
                fragmentTransaction.commit();
            }
        });
        return rootView;
    }
}
