package geca.dareliconsulting.smarttoutch.com.geca.fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import geca.dareliconsulting.smarttoutch.com.geca.BuildConfig;
import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.activity.LoginActivity;
import geca.dareliconsulting.smarttoutch.com.geca.model.Answer;
import geca.dareliconsulting.smarttoutch.com.geca.model.Note;
import geca.dareliconsulting.smarttoutch.com.geca.model.RequestTraining;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Constants;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Utils;

public class ParametresFragment extends Fragment {

    TextView ititleSynch, deconnect, usernameeditor, textViewAppVersion;

    ProgressDialog progressDialog;

    public ParametresFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_parametres, container, false);

        textViewAppVersion = (TextView)rootView.findViewById(R.id.textViewAppVersion);
        ititleSynch = ((TextView) rootView.findViewById(R.id.ressourcestitle));
        deconnect = ((TextView) rootView.findViewById(R.id.deconnect));
        usernameeditor = ((TextView) rootView.findViewById(R.id.usernameeditor));

        String versionName = BuildConfig.VERSION_NAME;
        textViewAppVersion.setText(versionName);
        usernameeditor.setText(getActivity().getSharedPreferences(Constants.SHARED_PREFS_NAME, getActivity().MODE_PRIVATE).getString("firstname", "") + " " + getActivity().getSharedPreferences(Constants.SHARED_PREFS_NAME, getActivity().MODE_PRIVATE).getString("lastname", ""));
        Typeface tf = Utils.getHelveticaFont(getActivity());
        ititleSynch.setTypeface(tf);

        rootView.findViewById(R.id.deleteAllnoteBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog = ProgressDialog.show(getActivity(), getActivity().getString(R.string.msgAttente), getActivity().getString(R.string.msgSupressionFiches), true);
                progressDialog.setCancelable(false);
                Note.deleteAll(Note.class, "synchronised = ?", "1");
                RequestTraining.deleteAll(RequestTraining.class, "is_synchron = ?", "1");
                progressDialog.dismiss();
                Toast.makeText(getActivity(), getActivity().getString(R.string.msgSupressionFichesSucces), Toast.LENGTH_SHORT).show();
            }
        });



        deconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Note> notes = Note.find(Note.class, "synchronised = ?", "0");
                if ((notes != null) && (notes.size() > 0)) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                    alertDialogBuilder.setTitle(R.string.msgNoSynchronisedNotes);
                    alertDialogBuilder
                            .setMessage(R.string.msgConfirmDisconnect)
                            .setCancelable(false)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    disconnect();
                                }
                            })
                            .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                } else {
                    disconnect();
                }
            }
        });

        Switch vpnSwitch = (Switch)rootView.findViewById(R.id.switchVPN);
        vpnSwitch.setChecked(Constants.isSVNEnabled(getActivity()));
        vpnSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Constants.enableDisableSVN(getActivity(), isChecked);
            }
        });
        return rootView;
    }

    void disconnect() {
        progressDialog = ProgressDialog.show(getActivity(), getActivity().getString(R.string.msgAttente), getActivity().getString(R.string.msgDeconnexionEnCours), true);
        progressDialog.setCancelable(false);
        progressDialog.dismiss();
        Answer.deleteAll(Answer.class);
        Note.deleteAll(Note.class);
        SharedPreferences prefs = getActivity().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
        prefs.edit().putBoolean("isConnected",false).commit();
        Toast.makeText(getActivity(), getActivity().getString(R.string.msgDeconnexionAvecSucces), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);
        getActivity().finish();
    }
}
