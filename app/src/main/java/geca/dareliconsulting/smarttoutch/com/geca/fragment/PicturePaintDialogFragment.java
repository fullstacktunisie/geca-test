package geca.dareliconsulting.smarttoutch.com.geca.fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Utils;
import geca.dareliconsulting.smarttoutch.com.geca.view.ScaleSmartImageView;
import me.panavtec.drawableview.DrawableView;
import me.panavtec.drawableview.DrawableViewConfig;


public class PicturePaintDialogFragment extends DialogFragment {

    public OnPictureChangedListener onPictureChangedListener;
    boolean deleteButtonVisible=true;
    String url;
    ScaleSmartImageView imageView;
    DrawableView drawableView;
    View imageLayout;
    DrawableViewConfig config;

    public PicturePaintDialogFragment() {

        // Required empty public constructor
        deleteButtonVisible=true;
    }


    // TODO: Rename and change types and number of parameters
    public static PicturePaintDialogFragment newInstance(String url) {
        PicturePaintDialogFragment fragment = new PicturePaintDialogFragment();
        Bundle args = new Bundle();
        args.putString("url", url);
        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        url = getArguments().getString("url");

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
         super.onCreateDialog(savedInstanceState);

        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_picture_paint_dialog, null);
        Dialog alertDialog = new Dialog(getActivity(), R.style.TransparentDialog);
        alertDialog.setContentView(view);
        if (url == null) {
            dismiss();
            return alertDialog;
        }
        imageView = (ScaleSmartImageView)view.findViewById(R.id.imageView);
        drawableView = (DrawableView)view.findViewById(R.id.paintView);
        imageLayout = view.findViewById(R.id.imageLayout);
        Bitmap bitmap = Utils.loadSmallBitmapFromFile(url);
        imageView.setImageBitmap(bitmap);
        config = new DrawableViewConfig();
        config.setStrokeColor(getResources().getColor(android.R.color.black));
        config.setShowCanvasBounds(true); // If the view is bigger than canvas, with this the user will see the bounds (Recommended)
        config.setStrokeWidth(20.0f);
        config.setMinZoom(1.0f);
        config.setMaxZoom(3.0f);
        config.setCanvasHeight((int)(float)(getResources().getDisplayMetrics().heightPixels*9/10));
        config.setCanvasWidth(getResources().getDisplayMetrics().widthPixels);
        //config.setCanvasHeight(bitmap.getHeight());
        //config.setCanvasWidth(bitmap.getWidth());
        drawableView.setConfig(config);

        Button deleteButton = (Button) view.findViewById(R.id.deleteButton);
        if(deleteButtonVisible) {
            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onPictureChangedListener != null) {
                        onPictureChangedListener.onPictureDeleted();
                    }
                    dismiss();
                }
            });
        }else
            deleteButton.setVisibility(View.INVISIBLE);
        Button minusButton = (Button) view.findViewById(R.id.minusButton);
        minusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                config.setStrokeWidth(config.getStrokeWidth() - 10);
            }
        });
        Button addButton = (Button) view.findViewById(R.id.addButton);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                config.setStrokeWidth(config.getStrokeWidth() + 10);
            }
        });
        Button undoButton = (Button) view.findViewById(R.id.undoButton);
        undoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawableView.undo();
            }
        });
        final Button colorButton = (Button) view.findViewById(R.id.colorButton);
        colorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getFragmentManager();
                ColorPicker colorPicker = new ColorPicker();
                colorPicker.onColorPickerListener = new ColorPicker.OnColorPickerListener() {
                    @Override
                    public void onColorPicked(String color) {
                        config.setStrokeColor(Color.parseColor(color));
                    }
                };
                colorPicker.show(fm, "ColorPicker");
            }
        });
        Button saveButton = (Button) view.findViewById(R.id.saveButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    imageLayout.setDrawingCacheEnabled(true);
                    Bitmap b = imageLayout.getDrawingCache();
                    b.compress(Bitmap.CompressFormat.JPEG, 50, new FileOutputStream(url));
                    Toast.makeText(getActivity(), "Succés d'enregistrement", Toast.LENGTH_SHORT).show();
                    if (onPictureChangedListener != null) {
                        onPictureChangedListener.onPictureChanged();
                    }
                    dismiss();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Erreur d'enregistrement", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return alertDialog;
    }
    public void setDeletevisibility(){
            deleteButtonVisible=false;
    }
    public interface OnPictureChangedListener {
        // TODO: Update argument type and name
        void onPictureDeleted();
        void onPictureChanged();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        Window window = getDialog().getWindow();
        window.setLayout(getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels);
        window.setGravity(Gravity.CENTER);
    }
}
