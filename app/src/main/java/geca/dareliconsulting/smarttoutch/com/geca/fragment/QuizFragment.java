package geca.dareliconsulting.smarttoutch.com.geca.fragment;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.Date;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.activity.MainActivity;
import geca.dareliconsulting.smarttoutch.com.geca.model.AbandonReason;
import geca.dareliconsulting.smarttoutch.com.geca.singleton.Singleton;

/**
 * A simple {@link Fragment} subclass.
 */
public class QuizFragment extends BaseFragment implements QuizResultFragment.OnQuizResultFragmentListener, AbandonCauseDialogFragment.OnAbandonCauseInteractionListener {

    FragmentManager fm;
    FragmentTransaction fragmentTransaction;
    public QuizIdentificationFragment quizIdentificationFragment;
    public QuizResultFragment quizResultFragment;
    public QuizQuestionsFragment quizQuestionsFragment;
    public int count = 0;

    public QuizFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = super.onCreateView(inflater, container, savedInstanceState);

        textViewTitle.setText("Quiz");
        fm = getFragmentManager();
        fragmentTransaction = fm.beginTransaction();
        quizIdentificationFragment = new QuizIdentificationFragment();
        fragmentTransaction.replace(R.id.content, quizIdentificationFragment, "quizzidentification");
        fragmentTransaction.commit();
        addLayoutListener();
        if (count == 0) {
            imageViewLeft.setVisibility(View.INVISIBLE);
            imageViewRight.setVisibility(View.VISIBLE);
        }
        textvDuplicate.setVisibility(View.INVISIBLE);
        return view;
    }

    private void addLayoutListener() {
        imageViewLeft.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                switch (count) {
                    case 1: {
                        count = count - 1;
                        fragmentTransaction = fm.beginTransaction();
                        if (quizIdentificationFragment == null) {
                            quizIdentificationFragment = new QuizIdentificationFragment();
                        }
                        fragmentTransaction.replace(R.id.content, quizIdentificationFragment, "quizzidentification");
                        fragmentTransaction.commit();
                        imageViewLeft.setVisibility(View.INVISIBLE);
                        imageViewRight.setVisibility(View.VISIBLE);
                        break;
                    }
                    case 2: {
                        count = count - 1;
                        fragmentTransaction = fm.beginTransaction();
                        if (quizQuestionsFragment == null) {
                            quizQuestionsFragment = new QuizQuestionsFragment();
                        }
                        fragmentTransaction.replace(R.id.content, quizQuestionsFragment, "quizzquestion");
                        fragmentTransaction.commit();
                        imageViewLeft.setVisibility(View.VISIBLE);
                        imageViewRight.setVisibility(View.VISIBLE);


                        break;
                    }
                    default:
                        break;
                }
            }
        });
        imageViewRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (count) {
                    case 0: {
                        if (quizIdentificationFragment.saveNote()) {
                            count++;
                            fragmentTransaction = fm.beginTransaction();
                            if (quizQuestionsFragment == null) {
                                quizQuestionsFragment = new QuizQuestionsFragment();
                            }
                            fragmentTransaction.replace(R.id.content, quizQuestionsFragment, "quizzquestion");
                            fragmentTransaction.commit();
                            imageViewLeft.setVisibility(View.VISIBLE);
                            imageViewRight.setVisibility(View.VISIBLE);
                        }
                        break;
                    }
                    case 1: {

                        try {
                            quizQuestionsFragment.saveNoteScore();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        count = count + 1;
                        fragmentTransaction = fm.beginTransaction();
                        if (quizResultFragment == null) {
                            quizResultFragment = new QuizResultFragment();
                            quizResultFragment.onQuizResultFragmentListener = QuizFragment.this;
                        }
                        fragmentTransaction.replace(R.id.content, quizResultFragment, "quizzresult");
                        fragmentTransaction.commit();
                        imageViewRight.setVisibility(View.INVISIBLE);
                        imageViewLeft.setVisibility(View.VISIBLE);

                    }
                    default:
                        break;
                }
            }

        });
    }


    public boolean saveNoteInDb(String state) {
        if (quizIdentificationFragment.saveNote()) {
            try {
                if (quizQuestionsFragment != null) {
                    quizQuestionsFragment.saveNoteScore();
                    quizQuestionsFragment.saveAnswersInDatabase();
                }
                if (quizResultFragment != null) {
                    quizResultFragment.saveFiche();
                }
                if (Singleton.getInstance().location != null) {
                    Singleton.getInstance().getCurrentNote().setLongitude(Singleton.getInstance().location.getLongitude());
                    Singleton.getInstance().getCurrentNote().setLatitude(Singleton.getInstance().location.getLatitude());
                }
                Singleton.getInstance().getCurrentNote().setStatus(state);
                Singleton.getInstance().getCurrentNote().setUpdated((new Date()).getTime());
                Singleton.getInstance().getCurrentNote().save();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getActivity(), getActivity().getString(R.string.msgErrorEnregistrement), Toast.LENGTH_SHORT).show();
            }
        }
        return false;
    }

    @Override
    public void onValidateAction() {
        if (saveNoteInDb("VALIDE")) {
            Singleton.getInstance().setCurrentNote(null);
            ((MainActivity) getActivity()).displayView(1);
        }
    }

    @Override
    public void saveNoteAction() {
        if (Singleton.getInstance().getCurrentNote().getStatus().equals("IN_PROGRESS")) {
            if (saveNoteInDb("IN_PROGRESS")) {
                Singleton.getInstance().setCurrentNote(null);
                ((MainActivity) getActivity()).displayView(1);
            }
        }
    }

    @Override
    public void abandonNoteAction() {
        if (Singleton.getInstance().getCurrentNote().getStatus().equals("IN_PROGRESS")) {
            FragmentManager fm = getFragmentManager();
            AbandonCauseDialogFragment abandonCauseDialogFragment = new AbandonCauseDialogFragment();
            abandonCauseDialogFragment.onAbandonCauseInteractionListener = QuizFragment.this;
            abandonCauseDialogFragment.show(fm, "AbandonCauseDialogFragment");
        }
    }

    @Override
    public void onConfirmAction(AbandonReason abandonReason) {
        Singleton.getInstance().getCurrentNote().setMotifOfAbandonment(abandonReason.getName());
        if (saveNoteInDb("ABANDON")) {
            Singleton.getInstance().setCurrentNote(null);
            ((MainActivity) getActivity()).displayView(1);
        }
    }
}
