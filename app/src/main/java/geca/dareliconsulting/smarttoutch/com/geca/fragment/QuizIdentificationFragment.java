package geca.dareliconsulting.smarttoutch.com.geca.fragment;


import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.model.Note;
import geca.dareliconsulting.smarttoutch.com.geca.model.Technician;
import geca.dareliconsulting.smarttoutch.com.geca.singleton.Singleton;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Constants;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Utils;

public class QuizIdentificationFragment extends Fragment implements SearchDialogFragment.OnSearchInteractionListener {

    TextView textViewNoteId, textViewNoteState, textViewNoteSynched, textViewProfession, textViewDate, textViewTechFirstname, textViewTechLastname, textViewTechMatricule, textViewTechFonction;
    EditText editTextNoteName;
    ImageView imageViewTechPhoto;
    Button searchTechnician;
    private SimpleDateFormat dateFormatter;
    private DatePickerDialog fromDatePickerDialog;
    Date noteDate = null;
    Technician technician;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_quizz_identification, container, false);
        dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

        loadViews(view);
        addLayoutListener();
        Utils.enableDisableView(view, Singleton.getInstance().getCurrentNote().getStatus().equals("IN_PROGRESS"));
        return view;
    }

    void loadViews(View view) {
        textViewNoteId = (TextView) view.findViewById(R.id.textViewNoteId);
        editTextNoteName = (EditText) view.findViewById(R.id.editTextNoteName);
        textViewNoteState = (TextView) view.findViewById(R.id.textViewNoteState);
        textViewNoteSynched = (TextView) view.findViewById(R.id.textViewNoteSynched);
        textViewProfession = (TextView) view.findViewById(R.id.textViewProfession);
        textViewDate = (TextView) view.findViewById(R.id.textViewDate);
        searchTechnician = (Button) view.findViewById(R.id.searchTechnician);
        textViewTechFirstname = (TextView) view.findViewById(R.id.textViewTechFirstname);
        textViewTechLastname = (TextView) view.findViewById(R.id.textViewTechLastname);
        textViewTechMatricule = (TextView) view.findViewById(R.id.textViewTechMatricule);
        textViewTechFonction = (TextView) view.findViewById(R.id.textViewTechFonction);
        imageViewTechPhoto = (ImageView) view.findViewById(R.id.imageViewTechPhoto);
    }

    @Override
    public void onResume() {
        super.onResume();
        loadViewsContent();
    }

    void loadViewsContent() {
        Note note = Singleton.getInstance().getCurrentNote();
        Log.d(Constants.TAG, "tech id" + note.getTechnicianId());
        if (Singleton.getInstance().getCurrentNote() == null) {
            Toast.makeText(getActivity(), "Note is null", Toast.LENGTH_LONG).show();
            return;
        }
        if (Singleton.getInstance().getCurrentNote().getId() != null) {
            textViewNoteId.setText("ID " + Singleton.getInstance().getCurrentNote().getId());
        }
        if (Singleton.getInstance().getCurrentNote().getStatus().equals("IN_PROGRESS")) {
            textViewNoteState.setText(getActivity().getString(R.string.enCours));
        } else if (Singleton.getInstance().getCurrentNote().getStatus().equals("VALIDE")) {
            textViewNoteState.setText(getActivity().getString(R.string.validee));
        } else {
            textViewNoteState.setText(getActivity().getString(R.string.abandonnee));
        }
        textViewNoteSynched.setText(Singleton.getInstance().getCurrentNote().isSynchronised() ? "Oui" : "Non");
        textViewProfession.setText(Singleton.getInstance().getCurrentNoteProfession().getLabel());
        if (Singleton.getInstance().getCurrentNote().getDate() != null) {
            textViewDate.setText(dateFormatter.format(Singleton.getInstance().getCurrentNote().getDate()));
        } else {
            noteDate = new Date();
            textViewDate.setText(dateFormatter.format(noteDate));
        }
        if (Singleton.getInstance().getCurrentNote().getTechnicianId() != null) {
            technician = Technician.findById(Technician.class, Singleton.getInstance().getCurrentNote().getTechnicianId());
            if (technician != null) {
                textViewTechFirstname.setText(technician.getName());
                textViewTechLastname.setText(technician.getSurName());
                textViewTechMatricule.setText(technician.getRegistrationNumber());
                textViewTechFonction.setText(technician.getFunction());
                try {
                    imageViewTechPhoto.setImageBitmap(Utils.loadSmallBitmapFromFile(technician.getLocalImage()));
                } catch (NullPointerException e) {
                } catch (IllegalArgumentException e) {
                }
            }
        }
        updateNoteName();
    }

    private void updateNoteName() { if (!Singleton.getInstance().getCurrentNote().getStatus().equals("IN_PROGRESS")) {
        editTextNoteName.setText(Singleton.getInstance().getCurrentNote().getLabel());
        return;
    }
        String noteName = "Quizz" + "_" + Singleton.getInstance().getNoteBaseName();
        if (technician != null) {
            noteName += "_" + technician.getName() + "-" + technician.getSurName();
        }

        noteName+="_"+Singleton.getInstance().getCurrentNoteProfessionFamily().getLabel();
        if (textViewDate.getText() != null) {
            noteName += "_" + textViewDate.getText().toString().replace("/", "-");
        }

        editTextNoteName.setText(noteName);
    }

    private void addLayoutListener() {
        Calendar newCalendar = Calendar.getInstance();
        textViewDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                fromDatePickerDialog.show();
            }
        });
        fromDatePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        noteDate = newDate.getTime();
                        textViewDate.setText(dateFormatter.format(noteDate));
                        updateNoteName();
                    }

                },
                newCalendar.get(Calendar.YEAR),
                newCalendar.get(Calendar.MONTH),
                newCalendar.get(Calendar.DAY_OF_MONTH)
        );
        searchTechnician.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                SearchDialogFragment searchDialogFragment = new SearchDialogFragment();
                searchDialogFragment.onSearchInteractionListener = QuizIdentificationFragment.this;
                searchDialogFragment.show(fm, "SignatureDialogFragment");
            }
        });
    }

    public boolean saveNote() {
        if (editTextNoteName.getText().toString().equals("") || textViewDate.getText().toString().equals("") || technician == null) {
            Toast.makeText(getActivity().getApplicationContext(), getActivity().getString(R.string.ChampsObligatoire), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            String noteLabel = editTextNoteName.getText().toString();

            Singleton.getInstance().getCurrentNote().setLabel(noteLabel);
            try {
                Singleton.getInstance().getCurrentNote().setDate(dateFormatter.parse(textViewDate.getText().toString().trim()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Singleton.getInstance().getCurrentNote().setTechnicianId(technician.getId());
            Singleton.getInstance().getCurrentNote().setTechnicianServerId(technician.getServerId());
            Singleton.getInstance().getCurrentNote().setSynchronised(false);
            return true;
        }
    }

    @Override
    public void onSearchListFinished(ArrayList<Technician> technicians) {
        if (technicians != null) {
            for (int i = 0; i < technicians.size(); i++) {
                this.technician = technicians.get(i);
                textViewTechFirstname.setText(technician.getName());
                textViewTechLastname.setText(technician.getSurName());
                textViewTechMatricule.setText(technician.getRegistrationNumber());
                textViewTechFonction.setText(technician.getFunction());
                try {
                    imageViewTechPhoto.setImageBitmap(Utils.loadSmallBitmapFromFile(technician.getLocalImage()));
                } catch (NullPointerException e) {
                } catch (IllegalArgumentException e) {
                }
            }
        }
        updateNoteName();
    }
}
