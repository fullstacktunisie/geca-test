package geca.dareliconsulting.smarttoutch.com.geca.fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.adapter.QuizDataSetAdapter;
import geca.dareliconsulting.smarttoutch.com.geca.model.Answer;
import geca.dareliconsulting.smarttoutch.com.geca.model.Question;
import geca.dareliconsulting.smarttoutch.com.geca.model.Skill;
import geca.dareliconsulting.smarttoutch.com.geca.model.TypeSkill;
import geca.dareliconsulting.smarttoutch.com.geca.singleton.Singleton;

public class QuizQuestionsFragment extends Fragment {


    ArrayList<Object> dataset;
    ListView listView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_quizz_questions, container, false);

        listView = (ListView) view.findViewById(R.id.listview_quizztypequestion);
        ArrayList<TypeSkill> typeSkillsList = Singleton.getInstance().getCurrentNoteProfession().getTypeSkills();
        dataset = new ArrayList<>();
        //-----------------------------------------------------------
        for (TypeSkill typeSkill : typeSkillsList) {
            if (typeSkill.getSkills().size() > 0) {
                ArrayList<Object> skillsItems = new ArrayList<>();
                for (Skill skill : typeSkill.getSkills()) {
                    if (skill.getQuestions().size() > 0) {
                        skillsItems.add(skill);
                        int i = 0;
                        for (Question question : skill.getQuestions()) {
                            if (i < 3) {
                                //if (question.getAnswer() == null) {
                                    if (Singleton.getInstance().getCurrentNote().getId() != null) {
                                        List<Answer> answers = Answer.find(Answer.class, "note_id = ? and skill_id = ? and type_skill_id = ? and question_id = ?", "" + Singleton.getInstance().getCurrentNote().getId(), "" + skill.getId(), "" + typeSkill.getIdTypeSkill(), "" + question.getId());
                                        if (answers != null && answers.size() > 0) {
                                            question.setAnswer(answers.get(0));
                                        }
                                    }
                                    if (question.getAnswer() == null) {
                                        Answer answer = new Answer();
                                        answer.setComment("");
                                        answer.setScore(0);
                                        answer.setSkillId(skill.getId());
                                        answer.setTypeSkillId(typeSkill.getIdTypeSkill());
                                        answer.setQuestionId(question.getId());
                                        answer.setCorrect(false);
                                        answer.setIsAnswered(false);
                                        question.setAnswer(answer);
                                    }
                                //}
                                skillsItems.add(question);
                                i++;
                            } else
                                break;
                        }
                    }
                }
                if (skillsItems.size() > 0) {
                    dataset.add(typeSkill);
                    dataset.addAll(skillsItems);
                }
            }
        }
        QuizDataSetAdapter quizDataSetAdapter = new QuizDataSetAdapter(getActivity(), dataset);
        listView.setAdapter(quizDataSetAdapter);
        int[] colors = {0, 0, 0};
        listView.setDivider(new GradientDrawable(
                GradientDrawable.Orientation.RIGHT_LEFT, colors));
        listView.setDividerHeight(10);
        return view;
    }

    public boolean saveAnswersInDatabase() {
        Singleton.getInstance().getCurrentNote().save();
        if (!allAnswersAreValid()) {
            PopUpAnswerAllQuestions();
            return false;
        } else {
            ArrayList<Question> questions = getQuestionsFromDataSet();
            for (Question q : questions) {
                q.getAnswer().setNoteId(Singleton.getInstance().getCurrentNote().getId());
                q.getAnswer().save();
            }
            return true;
        }
    }


    public void saveNoteScore() {
        int correctAnswersNumber = 0;
        ArrayList<Question> questions = getQuestionsFromDataSet();
        for (Question q : questions)
            if (q.getAnswer().isCorrect())
                correctAnswersNumber++;
        Singleton.getInstance().getCurrentNote().setGlobalScore(correctAnswersNumber * (20 / questions.size()));
    }


    ArrayList<Question> getQuestionsFromDataSet() {
        ArrayList<Question> questions = new ArrayList<>();
        for (Object o : dataset)
            if (o instanceof Question) {
                questions.add((Question) o);
            }
        return questions;
    }


    boolean allAnswersAreValid() {
        ArrayList<Question> questions = getQuestionsFromDataSet();
        for (Question q : questions)
            if (!q.getAnswer().isAnswered())
                return false;
        return true;
    }

    // SHOW POP UP MSG
    void PopUpAnswerAllQuestions() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getActivity());
        alertDialogBuilder.setTitle("Message");
        alertDialogBuilder
                .setMessage(getActivity().getString(R.string.msgRepondreTTquestion))
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
