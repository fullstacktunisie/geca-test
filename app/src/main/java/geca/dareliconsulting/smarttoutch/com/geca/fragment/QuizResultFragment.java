package geca.dareliconsulting.smarttoutch.com.geca.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Date;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.model.Technician;
import geca.dareliconsulting.smarttoutch.com.geca.singleton.Singleton;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Utils;

public class QuizResultFragment extends Fragment implements SignatureDialogFragment.OnSignatureInteractionListener {
    TextView technicienname, scoreText;
    ImageButton imageButtonEditFormerSign, imageButtonDeleteFormerSign;
    ImageView formerSignView;
    Button buttonValidate;
    String picSignAuditor;

    public OnQuizResultFragmentListener onQuizResultFragmentListener;

    public interface OnQuizResultFragmentListener {
        void onValidateAction();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_quizz_result, container, false);
        loadViews(view);
        loadViewContent();
        layoutListners();
        return view;
    }
    public void saveFiche() {
        Singleton.getInstance().getCurrentNote().setAuditorSignature(picSignAuditor);
        Singleton.getInstance().getCurrentNote().setUpdated((new Date()).getTime());
    }

    @Override
    public void onSignatureFinished(Bitmap bitmap) {
        formerSignView.setImageBitmap(bitmap);
        picSignAuditor = Utils.saveBitmapToFile(bitmap);
    }


    void loadViews(View view) {
        scoreText = (TextView) view.findViewById(R.id.textViewScore);
        scoreText.setText(Singleton.getInstance().getCurrentNote().getGlobalScore() + " / 20");
        technicienname = (TextView) view.findViewById(R.id.technicienname);
        technicienname.setText(Technician.findById(Technician.class, Singleton.getInstance().getCurrentNote().getTechnicianId()).getName());
        imageButtonEditFormerSign = (ImageButton) view.findViewById(R.id.updateSignature);
        imageButtonDeleteFormerSign = (ImageButton) view.findViewById(R.id.deleteSignature);
        formerSignView = (ImageView) view.findViewById(R.id.signatureImage);
        buttonValidate = (Button) view.findViewById(R.id.buttonValidate);
    }


    void layoutListners() {
        imageButtonEditFormerSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                SignatureDialogFragment signatureDialogFragment = new SignatureDialogFragment();
                signatureDialogFragment.onSignatureInteractionListener = QuizResultFragment.this;
                signatureDialogFragment.show(fm, "SignatureDialogFragment");
            }
        });
        imageButtonDeleteFormerSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                formerSignView.setImageBitmap(null);
                picSignAuditor = null;
            }
        });

        buttonValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onQuizResultFragmentListener != null) {
                    onQuizResultFragmentListener.onValidateAction();
                }
            }
        });
    }

    void loadViewContent() {
        if (Singleton.getInstance().getCurrentNote().getAuditorSignature() != null && !Singleton.getInstance().getCurrentNote().getAuditorSignature().isEmpty()) {
            picSignAuditor = Singleton.getInstance().getCurrentNote().getAuditorSignature();
            byte[] decodedString = Base64.decode(Singleton.getInstance().getCurrentNote().getAuditorSignature(), Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            formerSignView.setImageBitmap(bitmap);
        } else {
            formerSignView.setImageBitmap(null);
        }
    }


}
