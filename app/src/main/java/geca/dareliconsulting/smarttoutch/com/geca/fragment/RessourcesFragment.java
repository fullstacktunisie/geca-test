package geca.dareliconsulting.smarttoutch.com.geca.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.adapter.ListTechnicianAdapter;
import geca.dareliconsulting.smarttoutch.com.geca.model.Technician;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Utils;

public class RessourcesFragment extends Fragment {

    private View rootView;
    private TextView ititleressource;
    private Button iButtonAddTechnical;
    private EditText edittextsearchtechnician;
    private ListView listTechnicans;
    ListTechnicianAdapter listTechnicianAdapter;
    public RessourcesFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_list_ressources, container, false);
        loadViews(rootView);
        final List<Technician> technicians = Technician.listAll(Technician.class);
        listTechnicianAdapter = new ListTechnicianAdapter(getActivity(), (ArrayList<Technician>) technicians);
        listTechnicans.setAdapter(listTechnicianAdapter);


        listTechnicans.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                AddRessourceFragment addRessourceFragment = AddRessourceFragment.newInstance(technicians.get(position).getId());
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.frame_container, addRessourceFragment).commit();

            }
        });


        iButtonAddTechnical.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                AddRessourceFragment addRessourceFragment = new AddRessourceFragment();
                fragmentTransaction.replace(R.id.frame_container, addRessourceFragment);
                fragmentTransaction.commit();
            }
        });
        edittextsearchtechnician.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = edittextsearchtechnician.getText().toString().toLowerCase(Locale.getDefault());
                listTechnicianAdapter.filter(text);
            }
        });

        return rootView;
    }


    void loadViews(View view) {
        iButtonAddTechnical = ((Button) rootView.findViewById(R.id.addresource));
        edittextsearchtechnician = ((EditText) rootView.findViewById(R.id.edittextsearchtechnician));
        ititleressource = ((TextView) rootView.findViewById(R.id.ressourcestitle));
        Typeface tf = Utils.getHelveticaFont(getActivity());
        ititleressource.setTypeface(tf);
        listTechnicans = ((ListView) rootView.findViewById(R.id.listviewressources));
    }
}
