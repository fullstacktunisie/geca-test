package geca.dareliconsulting.smarttoutch.com.geca.fragment;

import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.adapter.SearchTechnicanAdapter;
import geca.dareliconsulting.smarttoutch.com.geca.model.Technician;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Utils;

public class SearchDialogFragment extends DialogFragment {


    public OnSearchInteractionListener onSearchInteractionListener;
    private TextView ititleressource;
    private Button chosetechnicien;
    private EditText edittextsearchtechnician;
    private ListView listTechnicans;
    SearchTechnicanAdapter listTechnicianAdapter;
    Technician technician;
    ArrayList<Technician> technicians;
    String type_search = "SINGLE";
    private static final String ARG_TYPE_SEARCH = "TYPE_SEARCH";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type_search = getArguments().getString(ARG_TYPE_SEARCH);
        }
    }

    public SearchDialogFragment() {
        // Required empty public constructor
    }

    public static SearchDialogFragment newInstance(String TYPE_SEARCH) {
        SearchDialogFragment fragment = new SearchDialogFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TYPE_SEARCH, TYPE_SEARCH);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        // safety check
        if (getDialog() == null)
            return;

        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        float dialogWidth = 0.95f * width;
        float dialogHeight = 0.8f * height;
        getDialog().getWindow().setLayout((int) dialogWidth, (int) dialogHeight);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search_ressource_dialog, container, false);
        loadViews(view);
        technicians = (ArrayList<Technician>) Technician.listAll(Technician.class);
        listTechnicianAdapter = new SearchTechnicanAdapter(getActivity(), technicians);
        listTechnicans.setAdapter(listTechnicianAdapter);
        listTechnicans.setItemsCanFocus(false);
        if (type_search.equals("MULTIPLE")) {
            listTechnicans.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        } else {
            listTechnicans.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        listTechnicans.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                technician = technicians.get(position);

            }
        });

        edittextsearchtechnician.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = edittextsearchtechnician.getText().toString().toLowerCase(Locale.getDefault());
                listTechnicianAdapter.filter(text);
            }
        });

        chosetechnicien.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });
        return view;
    }

    public void save() {

        ArrayList<Technician> listResults = new ArrayList<>();
        if (onSearchInteractionListener != null) {
            for (int i = 0; i < technicians.size(); i++) {
                if (technicians.get(i).isIsselected()) {
                    listResults.add(technicians.get(i));
                }
            }
            onSearchInteractionListener.onSearchListFinished(listResults);
        }
        dismiss();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onSearchInteractionListener = null;
    }

    public interface OnSearchInteractionListener {
        void onSearchListFinished(ArrayList<Technician> technician);
    }

    void loadViews(View view) {
        chosetechnicien = ((Button) view.findViewById(R.id.chosetechnicien));
        edittextsearchtechnician = ((EditText) view.findViewById(R.id.edittextsearchtechnician));
        ititleressource = ((TextView) view.findViewById(R.id.ressourcestitle));
        Typeface tf = Utils.getHelveticaFont(getActivity());
        ititleressource.setTypeface(tf);
        listTechnicans = ((ListView) view.findViewById(R.id.listviewressources));
    }

}
