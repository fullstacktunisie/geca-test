package geca.dareliconsulting.smarttoutch.com.geca.fragment;

import android.app.DialogFragment;
import android.gesture.GestureOverlayView;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import geca.dareliconsulting.smarttoutch.com.geca.R;

public class SignatureDialogFragment extends DialogFragment {

    GestureOverlayView signatureView;
    public OnSignatureInteractionListener onSignatureInteractionListener;

    public SignatureDialogFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_signature_dialog, container, false);
        signatureView = (GestureOverlayView) view.findViewById(R.id.signatureView);
        Button buttonSave = (Button) view.findViewById(R.id.buttonSave);
        Button buttonCancel = (Button) view.findViewById(R.id.buttonCancel);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return view;
    }

    public void save() {
        try {
            signatureView.setDrawingCacheEnabled(true);
            Bitmap bm = Bitmap.createBitmap(signatureView.getDrawingCache());
            if (onSignatureInteractionListener != null) {
                onSignatureInteractionListener.onSignatureFinished(bm);
            }

        } catch (Exception e) {
            Log.v("Gestures", e.getMessage());
            e.printStackTrace();
        }
        dismiss();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onSignatureInteractionListener = null;
    }

    public interface OnSignatureInteractionListener {
        void onSignatureFinished(Bitmap bitmap);
    }

}
