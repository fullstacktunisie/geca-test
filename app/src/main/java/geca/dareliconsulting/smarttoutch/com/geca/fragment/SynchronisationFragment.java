package geca.dareliconsulting.smarttoutch.com.geca.fragment;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.model.ActionPlan;
import geca.dareliconsulting.smarttoutch.com.geca.model.Answer;
import geca.dareliconsulting.smarttoutch.com.geca.model.Note;
import geca.dareliconsulting.smarttoutch.com.geca.model.RequestTraining;
import geca.dareliconsulting.smarttoutch.com.geca.model.Skill;
import geca.dareliconsulting.smarttoutch.com.geca.model.Technician;
import geca.dareliconsulting.smarttoutch.com.geca.model.TypeSkill;
import geca.dareliconsulting.smarttoutch.com.geca.model.WorkUnit;
import geca.dareliconsulting.smarttoutch.com.geca.singleton.Singleton;
import geca.dareliconsulting.smarttoutch.com.geca.utils.ClickGuard;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Constants;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Utils;


public class SynchronisationFragment extends Fragment implements Utils.OnDataSystemLoadedListener, Utils.OnTechnicianLoadedListener {
    public Button synchronstuct;
    public TextView ititleSynch;
    public Button synchronnotes, synchronNotesRequestTrainings;
    public Button synchrondatdbaseressources;
    ProgressDialog progressDialog;
    ArrayList<Note> notes;
    ArrayList<Technician> technicians;
    ArrayList<RequestTraining> requestTrainings;
    int currentNote = 0;
    int successfullNotesCount = 0;
    int successfullRequestTraining = 0;
    int currentTechnian = 0;
    int currentRequestTraining = 0;

    public SynchronisationFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_synchronisation, container, false);
        loadview(view);
        addLayoutListener();

        return view;
    }

    void synchroniseNextNote() {
        if (currentNote < notes.size()) {
            final Note note = notes.get(currentNote);
            //Singleton init for note
            Singleton.getInstance().setCurrentNote(note);
            for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills())
                for (Skill skill : typeSkill.getSkills())
                    for (WorkUnit workUnit : skill.getWorkUnits()) {
                        if (Singleton.getInstance().getCurrentNote().getId() != null) {
                            List<Answer> answers = Answer.find(Answer.class, "note_id = ? and skill_id = ? and type_skill_id = ? and work_unit_id = ?", "" + Singleton.getInstance().getCurrentNote().getId(), "" + skill.getId(), "" + typeSkill.getIdTypeSkill(), "" + workUnit.getId());
                            if (answers != null && answers.size() > 0) {
                                workUnit.setAnswer(answers.get(0));
                            }
                        }
                    }
            progressDialog.setMessage("Fiche en cours : " + note.getLabel() + "\nAvancement : " + (currentNote + 1) + " / " + notes.size());
            RequestParams params = new RequestParams();
            JSONObject noteJO = new JSONObject();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date today = Calendar.getInstance().getTime();
            String dateStr = df.format(today);
            try {
                dateStr = df.format(note.getDate());
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (note.getType().equals("CERTIFICATION")) {
                    noteJO.put("operationsDetails", note.getOperationsDetails());
                } else {
                    noteJO.put("operationsDetails", "");
                }

                noteJO.put("ficheTitle", note.getLabel());
                noteJO.put("ficheType", note.getType());
                noteJO.put("ficheStatus", note.getStatus());
                noteJO.put("profession", note.getProfessionId());
                noteJO.put("professionFamily", note.getProfessionFamilyId());
                noteJO.put("sector", note.getSectorId());
                noteJO.put("customer", note.getCustomerId());
                noteJO.put("customer2", note.getCustomerId2());
                noteJO.put("auditor", note.getAuditorId());
                noteJO.put("technician", note.getTechnicianServerId());
                noteJO.put("ficheSiteNumber", note.getSiteNumber());
                noteJO.put("ficheNumOt", note.getOtNumber());
                noteJO.put("ficheCreated", dateStr);
                noteJO.put("ficheTechnicianIsPresent", note.isTechnicianPresent() ? "1" : "0");
                noteJO.put("technicianRealFunction", note.getTechnicianRealFunction());
                noteJO.put("ficheCpic1", note.getComment1());
                noteJO.put("ficheCpic2", note.getComment2());
                noteJO.put("ficheCpic3", note.getComment3());
                noteJO.put("ficheNoteGlobal", note.getGlobalScore());
                noteJO.put("ficheNoteGlobalAuditor", note.getAuditorGlobalScore());
                noteJO.put("fichePriceNoCompliance", note.getPriceOfNonconformity());
                noteJO.put("fiche_isgranted", note.isGranted() ? 1 : 0);
                noteJO.put("ficheCommentGranted", note.getMotifGrant());
                noteJO.put("ficheProEvolutive", note.getTechnicianEvolutionProfession());
                noteJO.put("ficheCommentProEvolutive", note.getTechnicianEvolutionProfessionComment());
                noteJO.put("ficheDuration", (int) ((note.getUpdated() - note.getCreated()) / 6000));
                noteJO.put("longitude", note.getLongitude());
                noteJO.put("latitude", note.getLatitude());
                noteJO.put("securityAlert", note.getSecurityAlert());
                noteJO.put("listTypeSkillWithSecurityAlert", note.getListTypeSkillWithSecurityAlert());
                noteJO.put("nc_cost", note.getCostClientWU());
                noteJO.put("nc_cost_min", note.getCostClientWUMin());
                noteJO.put("nc_cost_maj", note.getCostClientWUMaj());
                noteJO.put("recovery", note.hasRecovery());
                noteJO.put("nb_recovery", note.getRecoveryWU());
                noteJO.put("nb_recovery_now", note.getRecoveryNowWU());
                noteJO.put("cost_recovery_now", note.getCostRecoveryNowWU());
                noteJO.put("cumulative_penality", note.getPriceOfPenality());
                //noteJO.put("operation_cost", note.getSiteAmount());
                noteJO.put("nextAudit", note.getNextAudit());
                noteJO.put("total_cost", note.getTotalCost());
                float completude = (Singleton.getInstance().getCurrentNote().getControledWU() * 100) / Singleton.getInstance().getCurrentNote().getAllWU();
                noteJO.put("completude", String.format("%.2f", completude));
                float txPertinance = 0;
                if ((Singleton.getInstance().getCurrentNote().getControledWU() + Singleton.getInstance().getCurrentNote().getFITWU()) != 0)
                    txPertinance = Singleton.getInstance().getCurrentNote().getControledWU() * 100 / (Singleton.getInstance().getCurrentNote().getControledWU() + Singleton.getInstance().getCurrentNote().getFITWU());
                noteJO.put("pertinence", txPertinance);
                float bilan = 0;
                if (Singleton.getInstance().getCurrentNote().getControledWU() != 0)
                    bilan = Singleton.getInstance().getCurrentNote().getNCWU() * 100 / Singleton.getInstance().getCurrentNote().getControledWU();
                noteJO.put("bilanNc", String.format("%.2f", bilan) + "%");
                noteJO.put("UONC", note.getNCWU());
                noteJO.put("UOC", note.getCWU());
                noteJO.put("UOEx", note.getExWU());
                float gravity = 0;
                if (Singleton.getInstance().getCurrentNote().getControledWU() != 0)
                    gravity = Singleton.getInstance().getCurrentNote().getNCWUMaj() * 100 / Singleton.getInstance().getCurrentNote().getControledWU();
                noteJO.put("TxGravity", String.format("%.2f", gravity) + "%");
                float txRecovery = 0;
                if (Singleton.getInstance().getCurrentNote().getNCWU() != 0)
                    txRecovery = Singleton.getInstance().getCurrentNote().getRecoveryNowWU() * 100 / Singleton.getInstance().getCurrentNote().getNCWU();
                noteJO.put("TxRecoveryNow", String.format("%.2f", txRecovery) + "%");
                noteJO.put("NC_min", note.getNCWUMin());
                noteJO.put("NC_maj", note.getNCWUMaj());
                noteJO.put("NC_all", note.getNCWU());
                noteJO.put("recovery_min", note.getRecoveryWUMin());
                noteJO.put("recovery_maj", note.getRecoveryWUMaj());
                noteJO.put("recovery_all", note.getRecoveryWU());
                noteJO.put("recovery_now_min", note.getRecoveryNowWUMin());
                noteJO.put("recovery_now_maj", note.getRecoveryNowWUMaj());
                noteJO.put("intern_cost_min", note.getCostInternWUMin());
                noteJO.put("intern_cost_maj", note.getCostInternWUMaj());
                noteJO.put("intern_cost_all", note.getCostInternWU());
                noteJO.put("recovery_now_cost_min", note.getCostRecoveryNowWUMin());
                noteJO.put("recovery_now_cost_maj", note.getCostRecoveryNowWUMaj());
                float TIENC = 0;
                noteJO.put("tienc", String.format("%.2f", TIENC) + "%");
                JSONArray delaysArray = new JSONArray();
                for (int r = 0; r < note.getRecoveryNumbers().size(); r++) {
                    JSONObject dObj = new JSONObject();
                    dObj.put("nbRecovery", note.getRecoveryNumbers().get(r).getNumber());
                    dObj.put("delayRecovery", note.getRecoveryNumbers().get(r).getDelay());
                    delaysArray.put(dObj);
                }
                noteJO.put("delayArray", delaysArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (note.getPicture1() != null) {
                try {
                    params.put("fichePic1", new File(note.getPicture1()));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
            if (note.getPicture2() != null) {
                try {
                    params.put("fichePic2", new File(note.getPicture2()));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
            if (note.getPicture3() != null) {
                try {
                    params.put("fichePic3", new File(note.getPicture3()));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
            if (note.getAuditorSignature() != null) {
                Log.e("sign", "auditor");
                try {
                    params.put("auditorSigned", new File(note.getAuditorSignature()));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
            if (note.getTechnicianSignature() != null) {
                Log.e("sign", "technician");
                try {
                    params.put("technicianSigned", new File(note.getTechnicianSignature()));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
            params.put("data", noteJO.toString());
            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(60000);
            client.post(getActivity(), Constants.API_BASE_URL(getActivity()) + "adds/news/fiches", params, new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            String response = new String(responseBody);
                            Log.i("onResponse", "sync note " + response);
                            currentNote++;
                            try {
                                Log.e("TAG", "onSuccess: " + new String(response));
                                JSONObject root = new JSONObject(response);
                                Log.e("resp", root.toString());
                                if (root.optBoolean("success")) {
                                    note.setSynchronised(true);
                                    note.save();
                                    int noteServerId = root.optJSONObject("data").optInt("id");
                                    synchAnswersForNote(note, noteServerId);
                                    successfullNotesCount++;
                                } else {
                                    synchroniseNextNote();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                synchroniseNextNote();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            currentNote++;
                            synchroniseNextNote();
                            if (responseBody.length > 0)
                                Log.e("error", statusCode + " " + error.getMessage() + " " + new String(responseBody));
                            else
                                Log.e("error", statusCode + " " + error.getMessage());

                        }
                    }
            );
        } else {
            progressDialog.dismiss();
            Toast.makeText(getActivity(), getActivity().getString(R.string.msgEtatSynchronisation) + " : " + successfullNotesCount + " / " + notes.size(), Toast.LENGTH_SHORT).show();
            currentNote = 0;
            successfullNotesCount = 0;
        }

    }

    void synchAnswersForNote(final Note note, final int noteIdServer) {
        final List<Answer> answers = Answer.find(Answer.class, "note_id = ?", "" + note.getId());
        JSONObject noteJO = new JSONObject();
        try {
            noteJO.put("Id", noteIdServer);
            noteJO.put("nc_cost", note.getCostClientWU());
            noteJO.put("nc_cost_min", note.getCostClientWUMin());
            noteJO.put("nc_cost_maj", note.getCostClientWUMaj());
            noteJO.put("recovery", note.hasRecovery());
            noteJO.put("nb_recovery", note.getRecoveryWU());
            noteJO.put("nb_recovery_now", note.getRecoveryNowWU());
            noteJO.put("cost_recovery_now", note.getCostRecoveryNowWU());
            noteJO.put("cumulative_penality", note.getPriceOfPenality());
            //noteJO.put("operation_cost", note.getSiteAmount());
            noteJO.put("nextAudit", note.getNextAudit());
            noteJO.put("total_cost", note.getTotalCost());
            float completude = (Singleton.getInstance().getCurrentNote().getControledWU() * 100) / Singleton.getInstance().getCurrentNote().getAllWU();
            noteJO.put("completude", String.format("%.2f", completude));
            float txPertinance = 0;
            if ((Singleton.getInstance().getCurrentNote().getControledWU() + Singleton.getInstance().getCurrentNote().getFITWU()) != 0)
                txPertinance = Singleton.getInstance().getCurrentNote().getControledWU() * 100 / (Singleton.getInstance().getCurrentNote().getControledWU() + Singleton.getInstance().getCurrentNote().getFITWU());
            noteJO.put("pertinence", txPertinance);
            float bilan = 0;
            if (Singleton.getInstance().getCurrentNote().getControledWU() != 0)
                bilan = Singleton.getInstance().getCurrentNote().getNCWU() * 100 / Singleton.getInstance().getCurrentNote().getControledWU();
            noteJO.put("bilanNc", String.format("%.2f", bilan) + "%");
            noteJO.put("UONC", note.getNCWU());
            noteJO.put("UOC", note.getCWU());
            noteJO.put("UOEx", note.getExWU());
            float gravity = 0;
            if (Singleton.getInstance().getCurrentNote().getControledWU() != 0)
                gravity = Singleton.getInstance().getCurrentNote().getNCWUMaj() * 100 / Singleton.getInstance().getCurrentNote().getControledWU();
            noteJO.put("TxGravity", String.format("%.2f", gravity) + "%");
            float txRecovery = 0;
            if (Singleton.getInstance().getCurrentNote().getNCWU() != 0)
                txRecovery = Singleton.getInstance().getCurrentNote().getRecoveryNowWU() * 100 / Singleton.getInstance().getCurrentNote().getNCWU();
            noteJO.put("TxRecoveryNow", String.format("%.2f", txRecovery) + "%");
            noteJO.put("NC_min", note.getNCWUMin());
            noteJO.put("NC_maj", note.getNCWUMaj());
            noteJO.put("NC_all", note.getNCWU());
            noteJO.put("recovery_min", note.getRecoveryWUMin());
            noteJO.put("recovery_maj", note.getRecoveryWUMaj());
            noteJO.put("recovery_all", note.getRecoveryWU());
            noteJO.put("recovery_now_min", note.getRecoveryNowWUMin());
            noteJO.put("recovery_now_maj", note.getRecoveryNowWUMaj());
            noteJO.put("intern_cost_min", note.getCostInternWUMin());
            noteJO.put("intern_cost_maj", note.getCostInternWUMaj());
            noteJO.put("intern_cost_all", note.getCostInternWU());
            noteJO.put("recovery_now_cost_min", note.getCostRecoveryNowWUMin());
            noteJO.put("recovery_now_cost_maj", note.getCostRecoveryNowWUMaj());
            float TIENC = 0;
           /* if (Float.parseFloat(Singleton.getInstance().getCurrentNote().getSiteAmount()) != 0)
                TIENC = Singleton.getInstance().getCurrentNote().getTotalCost() * 100 / Float.parseFloat(Singleton.getInstance().getCurrentNote().getSiteAmount());
           */
            noteJO.put("tienc", String.format("%.2f", TIENC) + "%");
            JSONArray delaysArray = new JSONArray();
            for (int r = 0; r < note.getRecoveryNumbers().size(); r++) {
                JSONObject dObj = new JSONObject();
                dObj.put("nbRecovery", note.getRecoveryNumbers().get(r).getNumber());
                dObj.put("delayRecovery", note.getRecoveryNumbers().get(r).getDelay());
                delaysArray.put(dObj);
            }
            noteJO.put("delayArray", delaysArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("note", noteJO.toString());
        if (answers != null && answers.size() > 0) {

            RequestParams params = new RequestParams();
            JSONArray answersJA = new JSONArray();
            try {
                for (Answer answer : answers) {
                    if (answer != null && answer.getScore() != 0) {
                        answer.setNoteIdServer(noteIdServer);
                        answer.save();
                        JSONObject answerJO = new JSONObject();
                        answerJO.put("fiche", noteIdServer);
                        answerJO.put("skill", answer.getSkillId());
                        answerJO.put("typeSkill", answer.getTypeSkillId());
                        answerJO.put("workUnit", answer.getWorkUnitId());
                        answerJO.put("question", answer.getQuestionId());
                        answerJO.put("score", answer.getScore());
                        answerJO.put("questionIsTrue", answer.isCorrect());
                        answerJO.put("isRecovery", answer.isHasRecovery());
                        answerJO.put("is_recovery_now", answer.isNowRecovery());
                        if (answer.isNowRecovery()) {
                            if (answer.getRecoverypictures().size() != 0) {
                                answerJO.put("recoveryPicture1", answer.getRecoverypictures().get(0).replace(Environment.getExternalStorageDirectory().getAbsolutePath() + "/", "").replace("GECA/", ""));
                                answerJO.put("recoveryPicture2", answer.getRecoverypictures().get(1).replace(Environment.getExternalStorageDirectory().getAbsolutePath() + "/", "").replace("GECA/", ""));
                                try {
                                    params.put(answer.getRecoverypictures().get(0).replace
                                                    (Environment.getExternalStorageDirectory().getAbsolutePath() + "/", "").replace("GECA/", ""),
                                            new File(answer.getRecoverypictures().get(0)));
                                    params.put(answer.getRecoverypictures().get(1).replace
                                                    (Environment.getExternalStorageDirectory().getAbsolutePath() + "/", "").replace("GECA/", ""),
                                            new File(answer.getRecoverypictures().get(1)));
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        answerJO.put("total_cost", answer.getTotalCost());
                        answerJO.put("comment", answer.getComment());
                        JSONArray actionsArray = new JSONArray();
                        List<ActionPlan> actionPlans = ActionPlan.listAll(ActionPlan.class);
                        for (int r = 0; r < answer.getActionPlans().size(); r++) {
                            actionsArray.put(actionPlans.get(answer.getActionPlans().get(r)).getServerId());
                        }
                        answerJO.put("actionsArray", actionsArray);
                        if (answer.getPicture() != null) {
                            answerJO.put("picture", answer.getPicture().replace(Environment.getExternalStorageDirectory().getAbsolutePath() + "/", "").replace("GECA/", ""));
                            try {
                                params.put(answer.getPicture().replace(Environment.getExternalStorageDirectory().getAbsolutePath() + "/", "").replace("GECA/", ""), new File(answer.getPicture()));
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            }
                        }


                        answersJA.put(answerJO);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            params.put("noteData", noteJO.toString());
            params.put("data", answersJA.toString());
            boolean english = Locale.getDefault().getLanguage().equals("en");
            String language = "fr";
            if (english)
                language = "en";
            params.put("language", language);
            Log.e("data", answersJA.toString());
            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(300000);
            client.post(getActivity(), Constants.API_BASE_URL(getActivity()) + "sends/questions", params, new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            String response = new String(responseBody);
                            Log.d("response", response);
                            synchroniseNextNote();
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            synchroniseNextNote();
                        }
                    }
            );

        } else {
            synchroniseNextNote();
        }
    }

    void synchroniseNextTechnicien() {
        if (currentTechnian < technicians.size()) {
            Log.e("technicien", "technicien " + currentTechnian);
            if ((progressDialog == null) || !progressDialog.isShowing()) {
                progressDialog = ProgressDialog.show(getActivity(), getActivity().getString(R.string.msgSynchronisationTech), "", true);
                progressDialog.setCancelable(false);
            }
            RequestParams params = new RequestParams();
            final Technician technician = technicians.get(currentTechnian);
            progressDialog.setMessage("Technicien en cours : " + technician.getName() + "\nAvancement : " + (currentTechnian + 1) + " / " + technicians.size());
            JSONObject technicanJO = new JSONObject();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
            Date today = Calendar.getInstance().getTime();
            String pdateend = null;
            String pdatestart = df.format(today);
            try {
                if (technician.getEndDate() != null) {
                    Date ddateendf = dateFormatter.parse(technician.getEndDate());
                    pdateend = df.format(ddateendf);
                }

                Date ddatestartd = dateFormatter.parse(technician.getStartDate());
                pdatestart = df.format(ddatestartd);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                if (technician.getServerId() != 0) {
                    technicanJO.put("id", technician.getServerId());
                }
                technicanJO.put("name", technician.getName());
                technicanJO.put("surname", technician.getSurName());
                technicanJO.put("email", technician.getEmail());
                technicanJO.put("matricule", technician.getRegistrationNumber());
                technicanJO.put("socialSecurityNumber", technician.getSocialSecurityNumber());
                technicanJO.put("fonction", technician.getFunctionId());
                technicanJO.put("phone", technician.getPhone());
                technicanJO.put("busnissResponsable", technician.getBusinessResponsibleId());
                technicanJO.put("dateStart", pdatestart);
                technicanJO.put("dateEnd", pdateend);
                technicanJO.put("agence", technician.getAgencyId());
                technicanJO.put("company", technician.getCompanyId());
                technicanJO.put("formation",technician.getFormation());
                technicanJO.put("experience", technician.getExperience());
                if (technician.getLocalImage() != null) {
                    try {
                        params.put("picture", new File(technician.getLocalImage()));
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            params.put("data", technicanJO.toString());
            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(60000);
            client.post(getActivity(), Constants.API_BASE_URL(getActivity()) + "adds/news/technicians", params, new AsyncHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                    // called when response HTTP status is "200 OK"
                    currentTechnian++;
                    JSONObject root = null;
                    try {
                        root = new JSONObject(new String(response));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.e("update technicien", "===>" + response.toString());
                    if (root != null && root.optBoolean("success")) {
                        int technicianServerId = root.optJSONObject("data").optInt("id");
                        technician.setServerId(technicianServerId);
                        technician.setEdit(false);
                        technician.save();
                        synchroniseNextTechnicien();
                    } else {
                        synchroniseNextTechnicien();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    Log.e("technicien error", "---" + error);
                    currentTechnian++;
                    synchroniseNextTechnicien();
                }
            });
        } else {
            Log.e("technicien", "technicien All synch");
            currentTechnian = 0;
            progressDialog.dismiss();
            Toast.makeText(getActivity(), getActivity().getString(R.string.msgEtatSynchronisation), Toast.LENGTH_SHORT).show();
            if ((progressDialog == null) || !progressDialog.isShowing()) {
                progressDialog = ProgressDialog.show(getActivity(), getActivity().getString(R.string.msgSynchronisationTech), getActivity().getString(R.string.msgUpdateEnCours), true);
                progressDialog.setCancelable(false);
            }
            Utils.loadTechnicians(getActivity(), new Utils.OnTechnicianLoadedListener() {
                @Override
                public void technicianLoadingCompleted(boolean success) {
                    progressDialog.dismiss();
                }
            });
        }
    }

    @Override
    public void dataSystemLoadingCompleted(boolean success) {
        progressDialog.dismiss();
        if (success) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.msgUpdateDataSucces), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getActivity(), getActivity().getString(R.string.msgErrorUpdateData), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void technicianLoadingCompleted(boolean success) {
        progressDialog.dismiss();
        if (success) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.msgUpdateRessourcesSucces), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getActivity(), getActivity().getString(R.string.msgErrorUpdateRessourcesSucces), Toast.LENGTH_LONG).show();
        }

    }

    void SynFicheNextAccompagnement() {

        if (currentRequestTraining < requestTrainings.size()) {
            if ((progressDialog == null) || !progressDialog.isShowing()) {
                progressDialog = ProgressDialog.show(getActivity(), getActivity().getString(R.string.msgSynchronisation), "", true);
                progressDialog.setCancelable(false);
            }
            final RequestTraining requestTraining = requestTrainings.get(currentRequestTraining);
            progressDialog.setMessage("Fiche en cours : " + requestTraining.getId() + "\nAvancement : " + (currentRequestTraining + 1) + " / " + requestTrainings.size());
            RequestParams params = new RequestParams();
            JSONObject requestTrainingJO = new JSONObject();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date today = Calendar.getInstance().getTime();
            String datestart = df.format(today);

            try {
                datestart = df.format(requestTraining.getDatestart());
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                requestTrainingJO.put("training", requestTraining.getTrainingId());
                requestTrainingJO.put("skill", requestTraining.getSkillId());
                requestTrainingJO.put("trainer", getActivity().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE).getString("user_id", ""));
                requestTrainingJO.put("dateStart", datestart);
                requestTrainingJO.put("duration", requestTraining.getDuration());
                requestTrainingJO.put("trainingComment", requestTraining.getComment());
                requestTrainingJO.put("trainingTypeId", requestTraining.getType());
                try {
                    requestTrainingJO.put("trainingObjectId", Integer.parseInt(requestTraining.getTrainingObject()));

                } catch (Exception o) {
                    requestTrainingJO.put("trainingObjectId", requestTraining.getTrainingObject());
                }

                requestTrainingJO.put("trainingNumber", requestTraining.getNumTraining());
                JSONArray listTechniciansJA = new JSONArray();
                if (requestTraining.getListTechniciens() != null) {
                    try {
                        JSONArray jsonArray = new JSONArray(requestTraining.getListTechniciens());
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Technician technician = Technician.findById(Technician.class, jsonArray.getLong(i));
                            if (technician != null) {
                                listTechniciansJA.put(new JSONObject().put("id", technician.getServerId()));
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                requestTrainingJO.put("listTechnician", listTechniciansJA);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            params.put("data", requestTrainingJO.toString());
            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(60000);
            client.post(getActivity(), Constants.API_BASE_URL(getActivity()) + "adds/news/accompanyings/notes", params, new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            String response = new String(responseBody);
                            currentRequestTraining++;
                            Log.i("onResponse", "" + response);
                            try {
                                JSONObject root = new JSONObject(response);
                                if (root.optBoolean("success")) {
                                    requestTraining.setSynchron(true);
                                    requestTraining.save();
                                    successfullRequestTraining++;
                                    SynFicheNextAccompagnement();
                                } else {
                                    SynFicheNextAccompagnement();
                                    Log.i("currentRequestTrain", "" + currentRequestTraining);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                SynFicheNextAccompagnement();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            Log.i("onErrorResponse", "" + error);
                            currentRequestTraining++;
                            Log.i("currentRequestTrain", "" + currentRequestTraining);
                            SynFicheNextAccompagnement();
                        }
                    }
            );
        } else {
            Log.i("currentRequestTrain", "" + currentRequestTraining);
            progressDialog.dismiss();
            Toast.makeText(getActivity(), getActivity().getString(R.string.msgSynchronisationFicheTerminee) + " : " + successfullRequestTraining + " / " + requestTrainings.size(), Toast.LENGTH_SHORT).show();
            currentRequestTraining = 0;
            successfullRequestTraining = 0;
        }
    }

    public static class MyLog {

        public static void d(String TAG, String message) {
            int maxLogSize = 2000;
            for (int i = 0; i <= message.length() / maxLogSize; i++) {
                int start = i * maxLogSize;
                int end = (i + 1) * maxLogSize;
                end = end > message.length() ? message.length() : end;
                android.util.Log.d(TAG, message.substring(start, end));
            }
        }

    }

    void loadview(View view) {
        ititleSynch = ((TextView) view.findViewById(R.id.ressourcestitle));
        synchronstuct = ((Button) view.findViewById(R.id.synchronstuct));
        synchronnotes = ((Button) view.findViewById(R.id.synchronnotes));
        synchronNotesRequestTrainings = ((Button) view.findViewById(R.id.synchronNotesRequestTrainings));
        synchrondatdbaseressources = ((Button) view.findViewById(R.id.synchrondatdbaseressources));
        Typeface tf = Utils.getHelveticaFont(getActivity());
        ititleSynch.setTypeface(tf);
    }

    private void addLayoutListener() {
        synchronstuct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isOnline(getActivity())) {
                    progressDialog = ProgressDialog.show(getActivity(), getActivity().getString(R.string.msgUpdateData), "", true);
                    progressDialog.setCancelable(false);
                    Utils.loadDataSytsem(getActivity(), SynchronisationFragment.this);
                } else {
                    Toast.makeText(getActivity(), getActivity().getString(R.string.msgConnexionInternet), Toast.LENGTH_SHORT).show();
                }
            }
        });
        synchrondatdbaseressources.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Utils.isOnline(getActivity())) {
                    technicians = new ArrayList<Technician>();
                    for (Technician technician : Technician.listAll(Technician.class)) {
                        if (technician.getServerId() == 0) {
                            technicians.add(technician);
                        } else if (technician.isEdit()) {
                            technicians.add(technician);
                        }
                    }
                    if (technicians.size() > 0) {
                        synchroniseNextTechnicien();
                    } else {
                        if ((progressDialog == null) || !progressDialog.isShowing()) {
                            progressDialog = ProgressDialog.show(getActivity(), getActivity().getString(R.string.msgSynchronisationTech), getActivity().getString(R.string.msgUpdateEnCours), true);
                            progressDialog.setCancelable(false);
                        }
                        Utils.loadTechnicians(getActivity(), new Utils.OnTechnicianLoadedListener() {
                            @Override
                            public void technicianLoadingCompleted(boolean success) {
                                progressDialog.dismiss();
                            }
                        });
                    }
                } else {
                    Toast.makeText(getActivity(), getActivity().getString(R.string.msgConnexionInternet), Toast.LENGTH_SHORT).show();
                }
            }
        });

        synchronnotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog = ProgressDialog.show(getActivity(), getActivity().getString(R.string.msgSynchronisation), "", true);
                progressDialog.setCancelable(false);
                Handler handler = new Handler();
                Runnable r = new Runnable() {
                    public void run() {
                        if (Utils.isOnline(getActivity())) {
                            if (Utils.allTechnicianIsSyn()) {
                                notes = new ArrayList<Note>();
                                for (Note note : Note.listAll(Note.class)) {
                                    if (!note.isSynchronised() && !note.getStatus().equals("IN_PROGRESS")) {
                                        Technician technician = Technician.findById(Technician.class, note.getTechnicianId());
                                        if (technician.getServerId() != 0) {
                                            if (note.getTechnicianServerId() == technician.getServerId()) {
                                                notes.add(note);
                                            } else if (note.getTechnicianServerId() == 0) {
                                                note.setTechnicianServerId(technician.getServerId());
                                                notes.add(note);
                                            }
                                        }

                                    }
                                }
                                if (notes.size() > 0) {
                                    synchroniseNextNote();
                                } else {
                                    progressDialog.dismiss();
                                    Toast.makeText(getActivity(), getActivity().getString(R.string.msgAucinFichierAsynchro), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(getActivity(), getActivity().getString(R.string.msgTechnitienNonSynchro), Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), getActivity().getString(R.string.msgConnexionInternet), Toast.LENGTH_SHORT).show();
                        }
                    }
                };
                handler.postDelayed(r, 100);
            }
        });
        synchronNotesRequestTrainings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isOnline(getActivity())) {
                    if (Utils.allTechnicianIsSyn()) {
                        requestTrainings = new ArrayList<RequestTraining>();
                        for (RequestTraining requestTraining : RequestTraining.listAll(RequestTraining.class)) {

                            if (!requestTraining.isSynchron()) {
                                JSONArray jsonArray = null;
                                JSONArray jListTechnicans = new JSONArray();
                                try {
                                    jsonArray = new JSONArray(requestTraining.getListTechniciens());
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        Technician technician = Technician.findById(Technician.class, jsonArray.getLong(i));
                                        jListTechnicans.put(technician.getServerId());
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                requestTraining.setListTechnicienssend(jListTechnicans.toString());
                                requestTrainings.add(requestTraining);
                            }
                        }
                        Log.e("train", requestTrainings.size() + "");
                        if (requestTrainings.size() > 0) {
                            SynFicheNextAccompagnement();
                        } else {
                            Toast.makeText(getActivity(), getActivity().getString(R.string.msgAucinFichierAsynchro), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), getActivity().getString(R.string.msgTechnitienNonSynchro), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(getActivity(), getActivity().getString(R.string.msgConnexionInternet), Toast.LENGTH_SHORT).show();
                }

            }
        });
        ClickGuard.guard(synchronstuct, synchrondatdbaseressources, synchronnotes, synchronNotesRequestTrainings);
    }
}
