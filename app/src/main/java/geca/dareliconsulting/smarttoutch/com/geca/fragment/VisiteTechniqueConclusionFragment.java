
package geca.dareliconsulting.smarttoutch.com.geca.fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.adapter.RecoveryNumberAdapter;
import geca.dareliconsulting.smarttoutch.com.geca.model.Fonction;
import geca.dareliconsulting.smarttoutch.com.geca.model.MyProfession;
import geca.dareliconsulting.smarttoutch.com.geca.model.Note;
import geca.dareliconsulting.smarttoutch.com.geca.model.RecoveryNumber;
import geca.dareliconsulting.smarttoutch.com.geca.model.Skill;
import geca.dareliconsulting.smarttoutch.com.geca.model.Technician;
import geca.dareliconsulting.smarttoutch.com.geca.model.TypeSkill;
import geca.dareliconsulting.smarttoutch.com.geca.model.WorkUnit;
import geca.dareliconsulting.smarttoutch.com.geca.singleton.Singleton;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Constants;
import geca.dareliconsulting.smarttoutch.com.geca.utils.ListViewUtils;
import geca.dareliconsulting.smarttoutch.com.geca.utils.SerialClone;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Utils;


/**
 * A simple {@link Fragment} subclass.
 */
public class VisiteTechniqueConclusionFragment extends Fragment implements SignatureDialogFragment.OnSignatureInteractionListener {

    TextView textViewGlobalScore, textViewNonConformityPrice, textViewFormerName, textViewTechName, textViewPenalityPrice;
    TextView textViewVTCompletude, textViewVTPertinence,textViewBilanNC,textViewUONC,textViewUOC, textViewUOEx, textViewGravity, textViewTxRecoveryNow, textViewNcMineur, textViewNcMajeur, textViewNcSomme, textViewRecoveryMineur, textViewRecoveryMajeur, textViewRecoverySomme,
            textViewRecoveryNowMineur,textViewRecoveryNowMajeur,textViewRecoveryNowSomme, textViewCostClientMineur,textViewCostClientMajeur,
            textViewCostClientSomme,textViewCostInternMineur,textViewCostInternMajeur,textViewCostInternSomme,textViewCostRecoveryNowMineur,
            textViewCostRecoveryNowMajeur,textViewCostRecoveryNowSomme,textViewTotalCost,textViewTIENC, textViewRecoveryTitle,textViewFitTitle, textViewFITNb;
    ListView recoveryList,fitList;

    Spinner spinnerFomerScore, spinnerRealFunction, spinnerCibleFunction;
    EditText editTextComment1, editTextComment2, editTextNextAudit;
    Switch switchSecurityAlert,switchGenerateCertif;
    ImageButton imageButtonEditFormerSign, imageButtonDeleteFormerSign, imageButtonEditTechSign, imageButtonDeleteTechSign;
    ImageView formerSignView, techSignView;
    Button buttonValidate;
    String[] listNotes;
    static final int SIGNATURE_FORMER = 111;
    static final int SIGNATURE_TECH = 222;
    int currentSignature;
    String picSignAuditor, picSignTechnician;

    public OnVisiteTechniqueConclusionFragmentListener onVisiteTechniqueConclusionFragmentListener;

    public interface OnVisiteTechniqueConclusionFragmentListener {
        void onValidateAction();
    }

    public VisiteTechniqueConclusionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_visite_technique_conclusion, container, false);

        loadViews(view);
        loadViewsContent();
        addLayoutListener();

        Utils.enableDisableView(view, Singleton.getInstance().getCurrentNote().getStatus().equals("IN_PROGRESS"));
        return view;
    }

    @Override
    public void onSignatureFinished(Bitmap bitmap) {
        switch (currentSignature) {
            case SIGNATURE_FORMER: {
                formerSignView.setImageBitmap(bitmap);
                picSignAuditor = Utils.saveBitmapToFile(bitmap);

                break;
            }
            case SIGNATURE_TECH: {
                techSignView.setImageBitmap(bitmap);
                picSignTechnician = Utils.saveBitmapToFile(bitmap);
                break;
            }
        }
    }

    void loadViews(View view) {
        textViewGlobalScore = (TextView) view.findViewById(R.id.textviewGlobalScore);
        textViewNonConformityPrice = (TextView) view.findViewById(R.id.textViewNonConformityPrice);
        textViewFormerName = (TextView) view.findViewById(R.id.textviewNameFormer);
        textViewTechName = (TextView) view.findViewById(R.id.textviewNameTechnicien);

        textViewVTCompletude = (TextView) view.findViewById(R.id.textViewVTCompletude);
        textViewVTPertinence = (TextView) view.findViewById(R.id.textViewVTPertinence);
        textViewBilanNC = (TextView) view.findViewById(R.id.textViewBilanNC);
        textViewUONC=(TextView) view.findViewById(R.id.textViewNbUONC);
        textViewUOC=(TextView) view.findViewById(R.id.textViewNbUOC);
        textViewUOEx=(TextView) view.findViewById(R.id.textViewNbUOEx);
        textViewGravity=(TextView) view.findViewById(R.id.textViewGravity);
        textViewTxRecoveryNow=(TextView) view.findViewById(R.id.textViewTxRecoveryNow);
        textViewNcMineur=(TextView) view.findViewById(R.id.textViewNCMineur);
        textViewNcMajeur=(TextView) view.findViewById(R.id.textViewNbNCMajeur);
        textViewNcSomme=(TextView) view.findViewById(R.id.textViewNCSomme);
        textViewRecoveryMineur=(TextView) view.findViewById(R.id.textViewRecoveryMineur);
        textViewRecoveryMajeur=(TextView) view.findViewById(R.id.textViewRecoveryMajeur);
        textViewRecoverySomme=(TextView) view.findViewById(R.id.textViewRecoverySomme);
        textViewRecoveryNowMineur=(TextView) view.findViewById(R.id.textViewRecoveryNowMineur);
        textViewRecoveryNowMajeur=(TextView) view.findViewById(R.id.textViewRecoveryNowMajeur);
        textViewRecoveryNowSomme=(TextView) view.findViewById(R.id.textViewrecoveryNowSomme);
        textViewCostClientMineur=(TextView) view.findViewById(R.id.textViewCostClientMineur);
        textViewCostClientMajeur=(TextView) view.findViewById(R.id.textViewNbCostClientMajeur);
        textViewCostClientSomme=(TextView) view.findViewById(R.id.textViewCostClientSomme);
        textViewCostInternMineur=(TextView) view.findViewById(R.id.textViewCostInternMineur);
        textViewCostInternMajeur=(TextView) view.findViewById(R.id.textViewCostInternMajeur);
        textViewCostInternSomme=(TextView) view.findViewById(R.id.textViewCostInternSomme);
        textViewCostRecoveryNowMineur=(TextView) view.findViewById(R.id.textViewCostRecoveryNowMineur);
        textViewCostRecoveryNowMajeur=(TextView) view.findViewById(R.id.textViewCostRecoveryNowMajeur);
        textViewCostRecoveryNowSomme=(TextView) view.findViewById(R.id.textViewcostRecoveryNowSomme);
        textViewTotalCost=(TextView) view.findViewById(R.id.textViewTotalPrice);
        textViewPenalityPrice=(TextView) view.findViewById(R.id.textViewPenalityPrice);
        textViewRecoveryTitle=(TextView) view.findViewById(R.id.recoveryListTitle);
        textViewFitTitle=(TextView) view.findViewById(R.id.FITTitle);
        textViewFITNb=(TextView) view.findViewById(R.id.textViewFitNb);
        textViewTIENC=(TextView) view.findViewById(R.id.textViewTIENC);
        recoveryList=(ListView)view.findViewById(R.id.recoveryList);
        fitList=(ListView)view.findViewById(R.id.fitList);

        spinnerFomerScore = (Spinner) view.findViewById(R.id.spinnerFormateurScore);
        spinnerRealFunction = (Spinner) view.findViewById(R.id.spinnerRealFunction);
        spinnerCibleFunction = (Spinner) view.findViewById(R.id.spinnerJobEvolution);
        editTextComment1 = (EditText) view.findViewById(R.id.editTextComment1);
        editTextComment2 = (EditText) view.findViewById(R.id.editTextComment2);
        editTextNextAudit = (EditText) view.findViewById(R.id.editTextNextAudit);
        switchSecurityAlert = (Switch) view.findViewById(R.id.alertsecurity);
        switchGenerateCertif = (Switch) view.findViewById(R.id.generateCertif);
        imageButtonEditFormerSign = (ImageButton) view.findViewById(R.id.imageButtonEditSignFormer);
        imageButtonDeleteFormerSign = (ImageButton) view.findViewById(R.id.imageButtonRemoveSignFormer);
        imageButtonEditTechSign = (ImageButton) view.findViewById(R.id.imageButtonEditSignTechnicien);
        imageButtonDeleteTechSign = (ImageButton) view.findViewById(R.id.imageButtonRemoveSignTechnicien);
        formerSignView = (ImageView) view.findViewById(R.id.viewSignFormer);
        techSignView = (ImageView) view.findViewById(R.id.viewSignTechnicien);
        buttonValidate = (Button) view.findViewById(R.id.buttonValidateCertif);

        listNotes = getActivity().getResources().getStringArray(R.array.list_work_unit_vt);
    }


    public void saveFiche() {
        Singleton.getInstance().getCurrentNote().setAuditorGlobalScore(spinnerFomerScore.getSelectedItemPosition());
        Singleton.getInstance().getCurrentNote().setGranted(false);
        Singleton.getInstance().getCurrentNote().setMotifGrant(editTextComment1.getText().toString().trim());
        Singleton.getInstance().getCurrentNote().setTechnicianRealFunction(((Fonction) spinnerRealFunction.getSelectedItem()).getIdFunction());
        Singleton.getInstance().getCurrentNote().setTechnicianEvolutionProfession(((MyProfession) spinnerCibleFunction.getSelectedItem()).getIdServer());
        Singleton.getInstance().getCurrentNote().setTechnicianEvolutionProfessionComment(editTextComment2.getText().toString().trim());
        if(!editTextNextAudit.getText().toString().equals(""))
            Singleton.getInstance().getCurrentNote().setNextAudit(Integer.parseInt(editTextNextAudit.getText().toString()));
        Singleton.getInstance().getCurrentNote().setAuditorSignature(picSignAuditor);
        Singleton.getInstance().getCurrentNote().setTechnicianSignature(picSignTechnician);
        Singleton.getInstance().getCurrentNote().setSecurityAlert(switchSecurityAlert.isChecked() ? "true" : "false");
         if (switchGenerateCertif.isChecked()) {
            if(Singleton.getInstance().getCurrentNote().isCertification()) {
                int score = 3;
                for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
                    for (Skill skill : typeSkill.getSkills()) {
                        int scoreSkill = 0;
                        for (WorkUnit workUnit : skill.getWorkUnits()) {
                            if (workUnit.isCertification()) {
                                if (workUnit.getAnswer() != null && workUnit.getAnswer().getScore() != 0)
                                    score = Math.min(score, workUnit.getAnswer().getScore());
                            }
                        }
                    }
                }

                Note certification = SerialClone.clone(Singleton.getInstance().getCurrentNote());
                certification.setType("CERTIFICATION");
                certification.setGlobalScore(score);
                String name = certification.getLabel();
                certification.setLabel(name.replace("VT", "Cert"));
                certification.save();
            }else{
                PopUpNoCertificationsWU();
            }
        }
    }
    void PopUpNoCertificationsWU() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getActivity());
        alertDialogBuilder.setTitle("Info");
        alertDialogBuilder
                .setMessage(getActivity().getString(R.string.msgNoWUCertification2))
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    private void addLayoutListener() {
        imageButtonEditFormerSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                SignatureDialogFragment signatureDialogFragment = new SignatureDialogFragment();
                signatureDialogFragment.onSignatureInteractionListener = VisiteTechniqueConclusionFragment.this;
                signatureDialogFragment.show(fm, "SignatureDialogFragment");
                currentSignature = SIGNATURE_FORMER;
            }
        });
        imageButtonEditTechSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                SignatureDialogFragment signatureDialogFragment = new SignatureDialogFragment();
                signatureDialogFragment.onSignatureInteractionListener = VisiteTechniqueConclusionFragment.this;
                signatureDialogFragment.show(fm, "SignatureDialogFragment");
                currentSignature = SIGNATURE_TECH;
            }
        });
        imageButtonDeleteFormerSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                formerSignView.setImageBitmap(null);
                picSignAuditor = null;
            }
        });
        imageButtonDeleteTechSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                techSignView.setImageBitmap(null);
                picSignTechnician = null;
            }
        });
        buttonValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onVisiteTechniqueConclusionFragmentListener != null) {
                    onVisiteTechniqueConclusionFragmentListener.onValidateAction();
                }
            }
        });
    }

    void loadViewsContent() {
        if (Singleton.getInstance().getCurrentNote().getGlobalScore() == 0) {
            textViewGlobalScore.setText(listNotes[Singleton.getInstance().getCurrentNote().getGlobalScore()]);
            textViewGlobalScore.setBackground(getActivity().getResources().getDrawable(R.color.gray));
        } else if (Singleton.getInstance().getCurrentNote().getGlobalScore() == 1) {
            textViewGlobalScore.setText(listNotes[Singleton.getInstance().getCurrentNote().getGlobalScore()]);
            textViewGlobalScore.setBackground(getActivity().getResources().getDrawable(R.color.wrong));
        } else if (Singleton.getInstance().getCurrentNote().getGlobalScore() == 2) {
            textViewGlobalScore.setText(listNotes[Singleton.getInstance().getCurrentNote().getGlobalScore()]);
            textViewGlobalScore.setBackground(getActivity().getResources().getDrawable(R.color.green));
        } else {
            textViewGlobalScore.setText(listNotes[Singleton.getInstance().getCurrentNote().getGlobalScore()]);
            textViewGlobalScore.setBackground(getActivity().getResources().getDrawable(R.color.blue));
        }
        float completude=(Singleton.getInstance().getCurrentNote().getControledWU() * 100 )/Singleton.getInstance().getCurrentNote().getAllWU();
        textViewVTCompletude.setText(String.format("%.1f",completude)+"%");
        float txPertinance=0;
        if((Singleton.getInstance().getCurrentNote().getControledWU()+Singleton.getInstance().getCurrentNote().getFITWU())!=0)
            txPertinance=Singleton.getInstance().getCurrentNote().getControledWU() * 100/(Singleton.getInstance().getCurrentNote().getControledWU()+Singleton.getInstance().getCurrentNote().getFITWU());
        textViewVTPertinence.setText(String.format("%.1f", txPertinance)+"%");
        float bilan=0;
        if(Singleton.getInstance().getCurrentNote().getControledWU()!=0)
            bilan=Singleton.getInstance().getCurrentNote().getNCWU()*100 / Singleton.getInstance().getCurrentNote().getControledWU();
        textViewBilanNC.setText(String.format("%.1f",bilan)+"%");
        textViewUONC.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getNCWU()));
        textViewUOC.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getCWU()));
        textViewUOEx.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getExWU()));
        float gravity=0;
        if(Singleton.getInstance().getCurrentNote().getControledWU()!=0)
            gravity=Singleton.getInstance().getCurrentNote().getNCWUMaj()*100/Singleton.getInstance().getCurrentNote().getControledWU();
        textViewGravity.setText(String.format("%.1f", gravity) + "%");
        float txRecovery=0;
        if(Singleton.getInstance().getCurrentNote().getNCWU()!=0)
            txRecovery=Singleton.getInstance().getCurrentNote().getRecoveryNowWU() * 100/Singleton.getInstance().getCurrentNote().getNCWU();
        textViewTxRecoveryNow.setText(String.format("%.1f",txRecovery)+"%");
        textViewNcMineur.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getNCWUMin()));
        textViewNcMajeur.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getNCWUMaj()));
        textViewNcSomme.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getNCWU()));
        textViewRecoveryMineur.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getRecoveryWUMin()));
        textViewRecoveryMajeur.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getRecoveryWUMaj()));
        textViewRecoverySomme.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getRecoveryWU()));
        textViewRecoveryNowMineur.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getRecoveryNowWUMin()));
        textViewRecoveryNowMajeur.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getRecoveryNowWUMaj()));
        textViewRecoveryNowSomme.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getRecoveryNowWU()));
        textViewCostClientMineur.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getCostClientWUMin())+" €");
        textViewCostClientMajeur.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getCostClientWUMaj())+" €");
        textViewCostClientSomme.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getCostClientWU())+" €");
        textViewCostInternMineur.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getCostInternWUMin())+" €");
        textViewCostInternMajeur.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getCostInternWUMaj())+" €");
        textViewCostInternSomme.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getCostInternWU())+" €");
        textViewCostRecoveryNowMineur.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getCostRecoveryNowWUMin())+" €");
        textViewCostRecoveryNowMajeur.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getCostRecoveryNowWUMaj())+" €");
        textViewCostRecoveryNowSomme.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getCostRecoveryNowWU())+" €");
        textViewTotalCost.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getTotalCost())+" €");
        textViewPenalityPrice.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getPriceOfPenality())+" €");
        float TIENC=0;
        if(Float.parseFloat(Singleton.getInstance().getCurrentNote().getSiteAmount())!=0)
            TIENC=Singleton.getInstance().getCurrentNote().getTotalCost()*100 / Float.parseFloat(Singleton.getInstance().getCurrentNote().getSiteAmount());
        textViewTIENC.setText(String.format("%.1f", TIENC) + "%");

        if(Singleton.getInstance().getCurrentNote().getRecoveryNumbers().size()>0) {
            textViewRecoveryTitle.setVisibility(View.VISIBLE);
            recoveryList.setVisibility(View.VISIBLE);
            ArrayList<RecoveryNumber> recoveries = new ArrayList<>();
            recoveries.add(null);
            recoveries.addAll(Singleton.getInstance().getCurrentNote().getRecoveryNumbers());
            RecoveryNumberAdapter adapter = new RecoveryNumberAdapter(getActivity(), recoveries);
            recoveryList.setAdapter(adapter);
            ListViewUtils.setListViewHeightBasedOnChildren(recoveryList);
        }else{
            textViewRecoveryTitle.setVisibility(View.GONE);
            recoveryList.setVisibility(View.GONE);
        }
        //FIT List
        if(Singleton.getInstance().getCurrentNote().getFITWU()>0) {
            textViewFitTitle.setVisibility(View.VISIBLE);
            fitList.setVisibility(View.VISIBLE);
            ArrayList<WorkUnit> fits = Singleton.getInstance().getCurrentNote().getAllFIT();
            ArrayList<String> names=new ArrayList<>();
            for(WorkUnit wu:fits)
                names.add(wu.getLabel());
            ArrayAdapter<String> adapter=new ArrayAdapter<> ( getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1, names );
            //FitWUAdapter adapter = new FitWUAdapter(getActivity(), fits);
            fitList.setAdapter(adapter);
            ListViewUtils.setListViewHeightBasedOnChildren(fitList);
        }else{
            textViewFitTitle.setVisibility(View.GONE);
            fitList.setVisibility(View.GONE);
        }
        textViewFITNb.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getFITWU()));

        Note n = Singleton.getInstance().getCurrentNote();
        switchSecurityAlert.setChecked(n.getSecurityAlert() != null && n.getSecurityAlert().equalsIgnoreCase("true"));
        editTextComment1.setText(Singleton.getInstance().getCurrentNote().getMotifGrant());
        editTextComment2.setText(Singleton.getInstance().getCurrentNote().getTechnicianEvolutionProfessionComment());
        editTextNextAudit.setText(String.valueOf(Singleton.getInstance().getCurrentNote().getNextAudit()));
        textViewNonConformityPrice.setText(Singleton.getInstance().getCurrentNote().getPriceOfNonconformity() + " €");
        ArrayAdapter<String> adapterNotes = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, listNotes);
        spinnerFomerScore.setAdapter(adapterNotes);
        spinnerFomerScore.setSelection(Singleton.getInstance().getCurrentNote().getGlobalScore());
        ArrayAdapter<Fonction> adapterRealFunction = new ArrayAdapter<Fonction>(getActivity(), R.layout.spinner_item, Fonction.listAll(Fonction.class));
        spinnerRealFunction.setAdapter(adapterRealFunction);

        ArrayAdapter<MyProfession> adapterEvolutionProfession = new ArrayAdapter<MyProfession>(getActivity(), R.layout.spinner_item, MyProfession.listAll(MyProfession.class));
        spinnerCibleFunction.setAdapter(adapterEvolutionProfession);

        int realFunction = 0;
        int evolutionProfession = 0;
        int indexFunction = -1;
        int indexProfession = -1;
        if (Singleton.getInstance().getCurrentNote().getTechnicianRealFunction() != 0) {
            for (Fonction fonction : Fonction.listAll(Fonction.class)) {
                if (fonction.getIdFunction() == Singleton.getInstance().getCurrentNote().getTechnicianRealFunction()) {
                    realFunction = indexFunction;
                }
            }
            spinnerRealFunction.setSelection(realFunction);
        }
        if (Singleton.getInstance().getCurrentNote().getTechnicianEvolutionProfession() != 0) {
            for (MyProfession myProfession : MyProfession.listAll(MyProfession.class)) {
                if (myProfession.getIdServer() == Singleton.getInstance().getCurrentNote().getTechnicianEvolutionProfession()) {
                    evolutionProfession = indexProfession;
                }
                indexProfession++;
            }
            spinnerCibleFunction.setSelection(evolutionProfession);
        }


        String name = getActivity().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE).getString("firstname", "") + " " + getActivity().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE).getString("lastname", "");
        textViewFormerName.setText(name);
        Technician technician = Technician.findById(Technician.class, Singleton.getInstance().getCurrentNote().getTechnicianId());
        textViewTechName.setText(technician.getName() + " " + technician.getSurName());

        if (Singleton.getInstance().getCurrentNote().getAuditorSignature() != null && !Singleton.getInstance().getCurrentNote().getAuditorSignature().isEmpty()) {
            formerSignView.setImageBitmap(Utils.loadFullBitmapFromFile(Singleton.getInstance().getCurrentNote().getAuditorSignature()));
        } else {
            formerSignView.setImageBitmap(null);
        }
        if (Singleton.getInstance().getCurrentNote().getTechnicianSignature() != null && !Singleton.getInstance().getCurrentNote().getTechnicianSignature().isEmpty()) {
            techSignView.setImageBitmap(Utils.loadFullBitmapFromFile(Singleton.getInstance().getCurrentNote().getTechnicianSignature()));
        } else {
            techSignView.setImageBitmap(null);
        }
    }

}
