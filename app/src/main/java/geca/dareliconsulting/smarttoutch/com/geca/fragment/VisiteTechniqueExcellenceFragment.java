package geca.dareliconsulting.smarttoutch.com.geca.fragment;


import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import java.util.ArrayList;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.singleton.Singleton;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Utils;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;

/**
 * A simple {@link android.app.Fragment} subclass.
 */
public class VisiteTechniqueExcellenceFragment extends Fragment implements PicturePaintDialogFragment.OnPictureChangedListener{

    EditText editTextComment1, editTextComment2, editTextComment3;
    ImageButton imageButtonPhoto1, imageButtonPhoto2, imageButtonPhoto3;
    String image1 = null;
    String image2 = null;
    String image3 = null;
    int currentIndex;
    static final int CAMERA_INTENT = 4444;
    private static final int REQUEST_IMAGE = 2;
    private ArrayList<String> mSelectPath;

    public VisiteTechniqueExcellenceFragment() {
        // Required empty public constructor
    }

    public void configIntentGetImage(){

        int selectedMode = MultiImageSelectorActivity.MODE_SINGLE;
        selectedMode = MultiImageSelectorActivity.MODE_SINGLE;

        Intent intent = new Intent(getActivity(), MultiImageSelectorActivity.class);
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SHOW_CAMERA, true);

        intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_MODE, selectedMode);
        if (mSelectPath != null && mSelectPath.size() > 0) {
            intent.putExtra(MultiImageSelectorActivity.EXTRA_DEFAULT_SELECTED_LIST, mSelectPath);
        }
        startActivityForResult(intent, REQUEST_IMAGE);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_certif_excellence, container, false);
        editTextComment1 = (EditText) view.findViewById(R.id.editTextComment1);
        editTextComment2 = (EditText) view.findViewById(R.id.editTextComment2);
        editTextComment3 = (EditText) view.findViewById(R.id.editTextComment3);
        imageButtonPhoto1 = (ImageButton) view.findViewById(R.id.imageButtonPhoto1);
        imageButtonPhoto2 = (ImageButton) view.findViewById(R.id.imageButtonPhoto2);
        imageButtonPhoto3 = (ImageButton) view.findViewById(R.id.imageButtonPhoto3);

        imageButtonPhoto1.setOnClickListener(cameraListener);
        imageButtonPhoto2.setOnClickListener(cameraListener);
        imageButtonPhoto3.setOnClickListener(cameraListener);
        image1 = Singleton.getInstance().getCurrentNote().getPicture1();
        image2 = Singleton.getInstance().getCurrentNote().getPicture2();
        image3 = Singleton.getInstance().getCurrentNote().getPicture3();
        editTextComment1.setText(Singleton.getInstance().getCurrentNote().getComment1());
        editTextComment2.setText(Singleton.getInstance().getCurrentNote().getComment2());
        editTextComment3.setText(Singleton.getInstance().getCurrentNote().getComment3());
        if ((Singleton.getInstance().getCurrentNote().getPicture1() != null) && !Singleton.getInstance().getCurrentNote().getPicture1().isEmpty()) {
            imageButtonPhoto1.setImageBitmap(Utils.loadSmallBitmapFromFile(Singleton.getInstance().getCurrentNote().getPicture1()));
        }
        if ((Singleton.getInstance().getCurrentNote().getPicture2() != null) && !Singleton.getInstance().getCurrentNote().getPicture2().isEmpty()) {
            imageButtonPhoto2.setImageBitmap(Utils.loadSmallBitmapFromFile(Singleton.getInstance().getCurrentNote().getPicture2()));
        }
        if ((Singleton.getInstance().getCurrentNote().getPicture3() != null) && !Singleton.getInstance().getCurrentNote().getPicture3().isEmpty()) {
            imageButtonPhoto3.setImageBitmap(Utils.loadSmallBitmapFromFile(Singleton.getInstance().getCurrentNote().getPicture3()));
        }
        Utils.enableDisableView(view, Singleton.getInstance().getCurrentNote().getStatus().equals("IN_PROGRESS"));
        return view;
    }
    View.OnClickListener cameraListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.imageButtonPhoto1: {

                    if ((image1 != null) && ! image1.isEmpty()) {
                        FragmentManager fm = getFragmentManager();
                        PicturePaintDialogFragment picturePaintDialogFragment = PicturePaintDialogFragment.newInstance(image1 );
                        picturePaintDialogFragment.onPictureChangedListener = VisiteTechniqueExcellenceFragment.this;
                        picturePaintDialogFragment.show(fm, "PicturePaintDialogFragment");
                    }
                    else {
                        configIntentGetImage();
                    }
                    currentIndex = 0;
                    break;
                }
                case R.id.imageButtonPhoto2: {

                    if ((image2 != null) && ! image2.isEmpty()) {
                        FragmentManager fm = getFragmentManager();
                        PicturePaintDialogFragment picturePaintDialogFragment = PicturePaintDialogFragment.newInstance(image2);
                        picturePaintDialogFragment.onPictureChangedListener = VisiteTechniqueExcellenceFragment.this;
                        picturePaintDialogFragment.show(fm, "PicturePaintDialogFragment");
                    }
                    else {
                        configIntentGetImage();
                    }

                    currentIndex = 1;
                    break;
                }
                case R.id.imageButtonPhoto3: {
                    if ((image3 != null) && ! image3.isEmpty()) {
                        FragmentManager fm = getFragmentManager();
                        PicturePaintDialogFragment picturePaintDialogFragment = PicturePaintDialogFragment.newInstance(image3);
                        picturePaintDialogFragment.onPictureChangedListener = VisiteTechniqueExcellenceFragment.this;
                        picturePaintDialogFragment.show(fm, "PicturePaintDialogFragment");
                    }
                    else {
                        configIntentGetImage();
                    }
                    currentIndex = 2;
                    break;
                }
                default:
                    break;
            }
        }
    };
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                mSelectPath = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
                for (String p : mSelectPath) {
                    Log.i("TEST", p);
                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    Bitmap bitmap = BitmapFactory.decodeFile(p, bmOptions);

                    if (bitmap != null) {

                        switch (currentIndex) {
                            case 0: {
                                image1 = Utils.saveBitmapToFile(bitmap);
                                imageButtonPhoto1.setImageBitmap(Utils.loadSmallBitmapFromFile(image1));
                                break;
                            }
                            case 1: {

                                image2 = Utils.saveBitmapToFile(bitmap);
                                imageButtonPhoto2.setImageBitmap(Utils.loadSmallBitmapFromFile(image2));
                                break;
                            }
                            case 2: {

                                image3 = Utils.saveBitmapToFile(bitmap);
                                imageButtonPhoto3.setImageBitmap(Utils.loadSmallBitmapFromFile(image3));
                                break;
                            }
                            default:
                                break;
                        }
                    }

                }
            }


        }
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    public void saveExcellencePhotos() {
        Singleton.getInstance().getCurrentNote().setComment1(editTextComment1.getText().toString().trim());
        Singleton.getInstance().getCurrentNote().setComment2(editTextComment2.getText().toString().trim());
        Singleton.getInstance().getCurrentNote().setComment3(editTextComment3.getText().toString().trim());
        Singleton.getInstance().getCurrentNote().setPicture1(image1);
        Singleton.getInstance().getCurrentNote().setPicture2(image2);
        Singleton.getInstance().getCurrentNote().setPicture3(image3);
    }

    @Override
    public void onPictureChanged() {
        switch (currentIndex) {
            case 0: {
                if ((Singleton.getInstance().getCurrentNote().getPicture1() != null) && !Singleton.getInstance().getCurrentNote().getPicture1().isEmpty()) {
                    imageButtonPhoto1.setImageBitmap(Utils.loadSmallBitmapFromFile(Singleton.getInstance().getCurrentNote().getPicture1()));
                }
                break;
            }
            case 1: {
                if ((Singleton.getInstance().getCurrentNote().getPicture2() != null) && !Singleton.getInstance().getCurrentNote().getPicture2().isEmpty()) {
                    imageButtonPhoto2.setImageBitmap(Utils.loadSmallBitmapFromFile(Singleton.getInstance().getCurrentNote().getPicture2()));
                }
                break;
            }
            case 2: {
                if ((Singleton.getInstance().getCurrentNote().getPicture3() != null) && !Singleton.getInstance().getCurrentNote().getPicture3().isEmpty()) {
                    imageButtonPhoto3.setImageBitmap(Utils.loadSmallBitmapFromFile(Singleton.getInstance().getCurrentNote().getPicture3()));
                }
                break;
            }
            default:
                break;
        }
    }

    @Override
    public void onPictureDeleted() {
        switch (currentIndex) {
            case 0: {
                image1 = null;
                imageButtonPhoto1.setImageResource(R.drawable.camera);
                break;
            }
            case 1: {
                image2 = null;
                imageButtonPhoto2.setImageResource(R.drawable.camera);
                break;
            }
            case 2: {
                image3 = null;
                imageButtonPhoto3.setImageResource(R.drawable.camera);
                break;
            }
            default:
                break;
        }
    }

}
