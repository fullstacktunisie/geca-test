package geca.dareliconsulting.smarttoutch.com.geca.fragment;


import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.model.Customer;
import geca.dareliconsulting.smarttoutch.com.geca.model.Note;
import geca.dareliconsulting.smarttoutch.com.geca.model.OperationType;
import geca.dareliconsulting.smarttoutch.com.geca.model.Technician;
import geca.dareliconsulting.smarttoutch.com.geca.singleton.Singleton;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Constants;
import geca.dareliconsulting.smarttoutch.com.geca.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class VisiteTechniqueIdentificationFragment extends Fragment implements SearchDialogFragment.OnSearchInteractionListener {

    TextView textViewNoteId, textViewNoteState, textViewNoteSynched, textViewProfession, textViewDate, textViewAuditor, textViewTechFirstname, textViewTechLastname, textViewTechMatricule, textViewTechFonction;
    EditText editTextNoteName, editTextNumChantier, editTextNumOT, editTextAmountChantier;
    Spinner spinnerClient, noteOperationTypeSpinner;
    Switch swithPresence;
    Button searchTechnician;
    ImageView imageViewTechPhoto;
    private SimpleDateFormat dateFormatter;
    private DatePickerDialog fromDatePickerDialog;
    Date noteDate = null;
    Technician technician;

    public VisiteTechniqueIdentificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_visite_technique_identification, container, false);
        dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

        loadViews(view);
        addLayoutListener();
        Utils.enableDisableView(view, Singleton.getInstance().getCurrentNote().getStatus().equals("IN_PROGRESS"));
        return view;
    }

    void loadViews(View view) {
        textViewNoteId = (TextView) view.findViewById(R.id.textViewNoteId);
        editTextNoteName = (EditText) view.findViewById(R.id.editTextNoteName);
        textViewNoteState = (TextView) view.findViewById(R.id.textViewNoteState);
        textViewNoteSynched = (TextView) view.findViewById(R.id.textViewNoteSynched);
        textViewProfession = (TextView) view.findViewById(R.id.textViewProfession);
        textViewDate = (TextView) view.findViewById(R.id.textViewDate);
        textViewAuditor = (TextView) view.findViewById(R.id.textViewAuditor);
        editTextNumChantier = (EditText) view.findViewById(R.id.editTextNumChantier);
        editTextAmountChantier = (EditText) view.findViewById(R.id.editTextAmountChantier);
        editTextNumOT = (EditText) view.findViewById(R.id.editTextNumOT);
        spinnerClient = (Spinner) view.findViewById(R.id.spinnerClient);
        searchTechnician = (Button) view.findViewById(R.id.searchTechnician);
        swithPresence = (Switch) view.findViewById(R.id.swithPresence);
        textViewTechFirstname = (TextView) view.findViewById(R.id.textViewTechFirstname);
        textViewTechLastname = (TextView) view.findViewById(R.id.textViewTechLastname);
        textViewTechMatricule = (TextView) view.findViewById(R.id.textViewTechMatricule);
        textViewTechFonction = (TextView) view.findViewById(R.id.textViewTechFonction);
        imageViewTechPhoto = (ImageView) view.findViewById(R.id.imageViewTechPhoto);
        ArrayAdapter<Customer> adapterCustomers = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, Customer.listAll(Customer.class));
        spinnerClient.setAdapter(adapterCustomers);


        noteOperationTypeSpinner = ((Spinner) view.findViewById(R.id.spinner_operation_typenote));

        // spinner operation type
        List<OperationType> opTypes = OperationType.listAll(OperationType.class);
        ArrayAdapter<OperationType> operationTypeArrayAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, opTypes);
        noteOperationTypeSpinner.setAdapter(operationTypeArrayAdapter);

        if (Singleton.getInstance().getCurrentNote().getType().equals("VISITE_SECURITE_ENVIRONNEMENT")) {
            view.findViewById(R.id.oot).setVisibility(View.GONE);
            view.findViewById(R.id.mot).setVisibility(View.GONE);
            view.findViewById(R.id.lot).setVisibility(View.GONE);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        loadViewsContent();
    }

    void loadViewsContent() {
        Note note = Singleton.getInstance().getCurrentNote();
        if (Singleton.getInstance().getCurrentNote() == null) {
            Toast.makeText(getActivity(), "Note is null", Toast.LENGTH_LONG).show();
            return;
        }
        if (Singleton.getInstance().getCurrentNote().getId() != null) {
            textViewNoteId.setText("ID " + Singleton.getInstance().getCurrentNote().getId());
        }
        if (Singleton.getInstance().getCurrentNote().getStatus().equals("IN_PROGRESS")) {
            textViewNoteState.setText(getActivity().getString(R.string.enCours));
        } else if (Singleton.getInstance().getCurrentNote().getStatus().equals("VALIDE")) {
            textViewNoteState.setText(getActivity().getString(R.string.validee));
        } else {
            textViewNoteState.setText(getActivity().getString(R.string.abandonnee));
        }
        textViewNoteSynched.setText(Singleton.getInstance().getCurrentNote().isSynchronised() ? "Oui" : "Non");
        textViewProfession.setText(Singleton.getInstance().getCurrentNoteProfession().getLabel());
        editTextNumOT.setText(Singleton.getInstance().getCurrentNote().getOtNumber());
        editTextNumChantier.setText(Singleton.getInstance().getCurrentNote().getSiteNumber());
        String name = getActivity().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE).getString("firstname", "") + " " + getActivity().getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE).getString("lastname", "");
        textViewAuditor.setText(name);
        if (Singleton.getInstance().getCurrentNote().getDate() != null) {
            textViewDate.setText(dateFormatter.format(Singleton.getInstance().getCurrentNote().getDate()));
        } else {
            noteDate = new Date();
            textViewDate.setText(dateFormatter.format(noteDate));
        }
        swithPresence.setChecked(Singleton.getInstance().getCurrentNote().isTechnicianPresent());
        if (Singleton.getInstance().getCurrentNote().getTechnicianId() != null) {
            technician = Technician.findById(Technician.class, Singleton.getInstance().getCurrentNote().getTechnicianId());
            if (technician != null) {
                textViewTechFirstname.setText(technician.getName());
                textViewTechLastname.setText(technician.getSurName());
                textViewTechMatricule.setText(technician.getRegistrationNumber());
                textViewTechFonction.setText(technician.getFunction());
                try {
                    imageViewTechPhoto.setImageBitmap(Utils.loadSmallBitmapFromFile(technician.getLocalImage()));
                } catch (NullPointerException e) {
                } catch (IllegalArgumentException e) {
                }
            }
        }
        if (Singleton.getInstance().getCurrentNote().getCustomerId() != 0) {
            int customerIndex = -1;
            for (int i = 0; i < Customer.listAll(Customer.class).size(); i++) {
                Customer customer = Customer.listAll(Customer.class).get(i);
                if (customer.getId() == Singleton.getInstance().getCurrentNote().getCustomerId()) {
                    customerIndex = i;
                    break;
                }
            }
            spinnerClient.setSelection(customerIndex, true);
        }

        spinnerClient.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                updateNoteName();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        noteOperationTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                editTextAmountChantier.setText(((OperationType) (noteOperationTypeSpinner.getSelectedItem())).getAmount());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        editTextNumOT.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                updateNoteName();
            }
        });
        updateNoteName();
    }

    private void updateNoteName() {
        if (!Singleton.getInstance().getCurrentNote().getStatus().equals("IN_PROGRESS")) {
            editTextNoteName.setText(Singleton.getInstance().getCurrentNote().getLabel());
            return;
        }
        String noteName;
        if (Singleton.getInstance().getCurrentNote().getType().equals("VISITE_SECURITE_ENVIRONNEMENT")) {
            noteName = "VSE" + "_" + Singleton.getInstance().getNoteBaseName();

        } else {
            noteName = "VT" + "_" + Singleton.getInstance().getNoteBaseName();

        }
        if (!Singleton.getInstance().getCurrentNote().getType().equals("VISITE_SECURITE_ENVIRONNEMENT")) {
            if (editTextNumOT.getText() != null) {
                noteName += "_" + editTextNumOT.getText().toString();
            }
        }
        if (spinnerClient.getSelectedItemPosition() != Spinner.INVALID_POSITION) {
            noteName += "_" + ((Customer) spinnerClient.getSelectedItem()).getCustomerName();
        }
        if (technician != null) {
            noteName += "_" + technician.getName() + "-" + technician.getSurName();
        }

        noteName += "_" + Singleton.getInstance().getCurrentNoteProfessionFamily().getLabel();
        if (textViewDate.getText() != null) {
            noteName += "_" + textViewDate.getText().toString().replace("/", "-");
        }

        editTextNoteName.setText(noteName);
    }

    private void addLayoutListener() {
        Calendar newCalendar = Calendar.getInstance();
        textViewDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                fromDatePickerDialog.show();
            }
        });
        fromDatePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        noteDate = newDate.getTime();
                        textViewDate.setText(dateFormatter.format(noteDate));
                        updateNoteName();
                    }

                },
                newCalendar.get(Calendar.YEAR),
                newCalendar.get(Calendar.MONTH),
                newCalendar.get(Calendar.DAY_OF_MONTH)
        );
        searchTechnician.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                SearchDialogFragment searchDialogFragment = new SearchDialogFragment();
                searchDialogFragment.onSearchInteractionListener = VisiteTechniqueIdentificationFragment.this;
                searchDialogFragment.show(fm, "SignatureDialogFragment");
            }
        });
    }

    public boolean saveNote() {

        if (editTextNoteName.getText().toString().equals("") || textViewDate.getText().toString().equals("") || spinnerClient.getSelectedItemPosition() == AdapterView.INVALID_POSITION || technician == null) {
            Toast.makeText(getActivity().getApplicationContext(), getActivity().getString(R.string.msgChampsObl), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            if (!Singleton.getInstance().getCurrentNote().getType().equals("VISITE_SECURITE_ENVIRONNEMENT")) {
                if (editTextNumOT.getText().toString().equals("") || noteOperationTypeSpinner.getSelectedItemPosition() == AdapterView.INVALID_POSITION) {
                    Toast.makeText(getActivity().getApplicationContext(), getActivity().getString(R.string.msgChampsObl), Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
            String noteLabel = editTextNoteName.getText().toString();
            Customer customer = (Customer) spinnerClient.getSelectedItem();
            Singleton.getInstance().getCurrentNote().setLabel(noteLabel);
            Singleton.getInstance().getCurrentNote().setCustomerId(customer.getIdserveur());
            if (!Singleton.getInstance().getCurrentNote().getType().equals("VISITE_SECURITE_ENVIRONNEMENT")) {
                Singleton.getInstance().getCurrentNote().setSiteAmount(editTextAmountChantier.getText().toString().trim());
                Singleton.getInstance().getCurrentNote().setOtNumber(editTextNumOT.getText().toString().trim());
                Singleton.getInstance().getCurrentNote().setOperationTypeId(((OperationType) (noteOperationTypeSpinner.getSelectedItem())).getServerId());

            } else {
                Singleton.getInstance().getCurrentNote().setSiteAmount("0");
                Singleton.getInstance().getCurrentNote().setOtNumber("0");
                Singleton.getInstance().getCurrentNote().setOperationTypeId(0);
            }
            Singleton.getInstance().getCurrentNote().setSiteNumber(editTextNumChantier.getText().toString().trim());
            Singleton.getInstance().getCurrentNote().setTechnicianPresent(swithPresence.isChecked());

            try {
                Singleton.getInstance().getCurrentNote().setDate(dateFormatter.parse(textViewDate.getText().toString().trim()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Singleton.getInstance().getCurrentNote().setTechnicianId(technician.getId());
            Singleton.getInstance().getCurrentNote().setTechnicianServerId(technician.getServerId());
            Singleton.getInstance().getCurrentNote().setSynchronised(false);
            return true;
        }
    }


    @Override
    public void onSearchListFinished(ArrayList<Technician> technicians) {

        if (technicians != null) {
            for (int i = 0; i < technicians.size(); i++) {
                this.technician = technicians.get(i);
                textViewTechFirstname.setText(technician.getName());
                textViewTechLastname.setText(technician.getSurName());
                textViewTechMatricule.setText(technician.getRegistrationNumber());
                textViewTechFonction.setText(technician.getFunction());
                try {
                    imageViewTechPhoto.setImageBitmap(Utils.loadSmallBitmapFromFile(technician.getLocalImage()));
                } catch (NullPointerException e) {
                } catch (IllegalArgumentException e) {
                }
            }
        }
        updateNoteName();
    }
}
