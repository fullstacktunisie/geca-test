package geca.dareliconsulting.smarttoutch.com.geca.fragment;


import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import geca.dareliconsulting.smarttoutch.com.geca.R;
import geca.dareliconsulting.smarttoutch.com.geca.adapter.TechnicalVisitDataSetAdapter;
import geca.dareliconsulting.smarttoutch.com.geca.model.Answer;
import geca.dareliconsulting.smarttoutch.com.geca.model.Skill;
import geca.dareliconsulting.smarttoutch.com.geca.model.TypeSkill;
import geca.dareliconsulting.smarttoutch.com.geca.model.WorkUnit;
import geca.dareliconsulting.smarttoutch.com.geca.singleton.Singleton;

/**
 * A simple {@link Fragment} subclass.
 */
public class VisiteTechniqueQuestionsFragment extends Fragment {

    ArrayList<Object> dataset;
    ListView listView;
    String alertsecurity = "false";
    private TechnicalVisitDataSetAdapter adapter;

    public VisiteTechniqueQuestionsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_certif_questions, container, false);
        dataset = new ArrayList<>();
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            if (Singleton.getInstance().getCurrentNote().getListTypeSkillNotApplicable() != null) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(Singleton.getInstance().getCurrentNote().getListTypeSkillNotApplicable());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        if (jsonArray.getLong(i) == typeSkill.getIdTypeSkill()) {
                            typeSkill.setNotApplicable(true);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            if (Singleton.getInstance().getCurrentNote().getListTypeSkillWithSecurityAlert() != null) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(Singleton.getInstance().getCurrentNote().getListTypeSkillWithSecurityAlert());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        if (jsonArray.getLong(i) == typeSkill.getIdTypeSkill()) {
                            typeSkill.setWithAlertSecurity(true);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            dataset.add(typeSkill);
            for (Skill skill : typeSkill.getSkills()) {
                if (Singleton.getInstance().getCurrentNote().getListSkillisNotApplicable() != null) {
                    JSONArray jsonArray = null;
                    try {
                        jsonArray = new JSONArray(Singleton.getInstance().getCurrentNote().getListSkillisNotApplicable());
                        for (int i = 0; i < jsonArray.length(); i++) {
                            if (jsonArray.getLong(i) == skill.getId()) {
                                skill.setNotApplicable(true);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                dataset.add(skill);
                for (WorkUnit workUnit : skill.getWorkUnits()) {
                    //if (workUnit.getAnswer() == null) {
                        if (Singleton.getInstance().getCurrentNote().getId() != null) {
                            List<Answer> answers = Answer.find(Answer.class, "note_id = ? and skill_id = ? and type_skill_id = ? and work_unit_id = ?", "" + Singleton.getInstance().getCurrentNote().getId(), "" + skill.getId(), "" + typeSkill.getIdTypeSkill(), "" + workUnit.getId());
                            if (answers != null && answers.size() > 0) {
                                workUnit.setAnswer(answers.get(0));
                            }
                        }
                        if (workUnit.getAnswer() == null) {
                            Answer answer = new Answer();
                            answer.setHasRecovery(false);
                            answer.setComment("");
                            answer.setScore(0);
                            answer.setSkillId(skill.getId());
                            answer.setTypeSkillId(typeSkill.getIdTypeSkill());
                            answer.setWorkUnitId(workUnit.getId());
                            workUnit.setAnswer(answer);
                        } else {
                            if (workUnit.getAnswer().isTypeSkillNotApplicable()) {
                                typeSkill.setNotApplicable(true);
                            }
                            if (workUnit.getAnswer().isSkillNotApplicable()) {
                                skill.setNotApplicable(true);
                            }
                        }
                    //}
                    dataset.add(workUnit);
                }
            }
        }
        listView = (ListView) view.findViewById(R.id.listView);
        View footer = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dummy_listview_footer, null);
        listView.addFooterView(footer);
        adapter=new TechnicalVisitDataSetAdapter(getActivity(), dataset);
        listView.setAdapter(adapter);
        return view;
    }

    public void saveAnswersInDB() {
        Singleton.getInstance().getCurrentNote().save();
        for (Object o : dataset) {
            if ((o instanceof WorkUnit) && !((WorkUnit) o).isNotApplicable()) {
                ((WorkUnit) o).getAnswer().setNoteId(Singleton.getInstance().getCurrentNote().getId());
                ((WorkUnit) o).getAnswer().save();
            }
        }
    }

    public void saveNoteScore() {
        int nonConromityPrice = 0;
        int recoveryCount = 0;
        int globalScore = 0;
        int skillsCount = 0;
        JSONArray jsonArrayTypeSkillNotApplicable = new JSONArray();
        JSONArray jsonArrayTypeSkillWithSecurityAlert = new JSONArray();
        boolean getCertification=true, wucertifExists=false;
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            if (typeSkill.isWithAlertSecurity()) {
                alertsecurity = "true";
                jsonArrayTypeSkillWithSecurityAlert.put(typeSkill.getIdTypeSkill());
            }
            if (!typeSkill.isNotApplicable()) {
                for (Skill skill : typeSkill.getSkills()) {
                    skillsCount++;
                    if (!skill.isNotApplicable()) {
                        double skillScore = 0;
                        int wuCount = 0;
                        for (WorkUnit workUnit : skill.getWorkUnits()) {
                            if(workUnit.getAnswer().getScore()!=0) {
                                wuCount++;
                                skillScore += workUnit.getAnswer().getScore();
                                if(workUnit.isCertification())
                                    wucertifExists=true;
                                if (workUnit.getAnswer().getScore() < 2) {
                                    nonConromityPrice += workUnit.getNonConformityCount();
                                    if(workUnit.isCertification())
                                        getCertification=false;
                                }
                            }
                        }
                        if (wuCount > 0) {
                            skillScore = skillScore / wuCount;
                            globalScore += skillScore;
                        }else{
                            skillsCount--;
                        }

                    }
                }

            } else {
                jsonArrayTypeSkillNotApplicable.put(typeSkill.getIdTypeSkill());
            }
        }
        Singleton.getInstance().getCurrentNote().setCertification(getCertification && wucertifExists);
        Singleton.getInstance().getCurrentNote().setListTypeSkillNotApplicable(jsonArrayTypeSkillNotApplicable.toString());
        Singleton.getInstance().getCurrentNote().setListTypeSkillWithSecurityAlert(jsonArrayTypeSkillWithSecurityAlert.toString());
        if(skillsCount!=0)
            Singleton.getInstance().getCurrentNote().setGlobalScore(globalScore / skillsCount);
        Singleton.getInstance().getCurrentNote().setPriceOfNonconformity(String.valueOf(nonConromityPrice));
        Singleton.getInstance().getCurrentNote().setPriceOfPenality((recoveryCount>=Singleton.getInstance().maxRecovery) ? Singleton.getInstance().penality : 0);
    }

    public void notifyErrors(){
        adapter.notifyDataSetChanged();
    }
}
