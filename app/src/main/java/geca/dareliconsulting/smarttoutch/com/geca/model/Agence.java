package geca.dareliconsulting.smarttoutch.com.geca.model;

import com.orm.SugarRecord;

/**
 * Created by Smart on 29/04/2015.
 */
public class Agence extends SugarRecord<Agence> {

    private int idserveur;
    private String label;

    public Agence() {
    }

    public Agence(int idserveur, String label) {
        this.idserveur = idserveur;
        this.label = label;
    }

    public int getIdserveur() {
        return idserveur;
    }

    public void setIdserveur(int idserveur) {
        this.idserveur = idserveur;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }


    @Override
    public String toString() {
        return this.getLabel();
    }
}
