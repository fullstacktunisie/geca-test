package geca.dareliconsulting.smarttoutch.com.geca.model;

import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.ArrayList;

import geca.dareliconsulting.smarttoutch.com.geca.singleton.Singleton;

/**
 * Created by abdelkader on 27.04.15.
 */
public class Answer extends SugarRecord<Answer> implements Serializable {
    private long noteId;
    private int noteIdServer;
    private int workUnitId;
    private int questionId;
    private int skillId;
    private int typeSkillId;
    private boolean correct;
    private int score;
    private String comment="";
    private String picture;
    private boolean hasRecovery;
    private boolean isAnswered = false;
    private boolean isTypeSkillNotApplicable = false;
    private boolean isSkillNotApplicable = false;
    private String recoverypictures="";
    private boolean nowRecovery;
    private String actionPlans="";
    private boolean fit=false;
    private boolean error=false;

    public Answer() {
    }

    public Answer(long noteId, int noteIdServer, int workUnitId, int questionId, int skillId, int typeSkillId, boolean correct, int score, String comment, String picture, boolean hasRecovery) {
        this.noteId = noteId;
        this.noteIdServer = noteIdServer;
        this.workUnitId = workUnitId;
        this.questionId = questionId;
        this.skillId = skillId;
        this.typeSkillId = typeSkillId;
        this.correct = correct;
        this.score = score;
        this.comment = comment;
        this.picture = picture;
        this.hasRecovery = hasRecovery;
    }

    public long getNoteId() {
        return noteId;
    }

    public void setNoteId(long noteId) {
        this.noteId = noteId;
    }

    public int getWorkUnitId() {
        return workUnitId;
    }

    public void setWorkUnitId(int workUnitId) {
        this.workUnitId = workUnitId;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public int getSkillId() {
        return skillId;
    }

    public void setSkillId(int skillId) {
        this.skillId = skillId;
    }

    public int getTypeSkillId() {
        return typeSkillId;
    }

    public void setTypeSkillId(int typeSkillId) {
        this.typeSkillId = typeSkillId;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public boolean isHasRecovery() {
        return hasRecovery;
    }

    public void setHasRecovery(boolean hasRecovery) {
        this.hasRecovery = hasRecovery;
    }

    public int getNoteIdServer() {
        return noteIdServer;
    }

    public void setNoteIdServer(int noteIdServer) {
        this.noteIdServer = noteIdServer;
    }

    public boolean isAnswered() {
        return isAnswered;
    }

    public void setIsAnswered(boolean isAnswered) {
        this.isAnswered = isAnswered;
    }

    public boolean isTypeSkillNotApplicable() {
        return isTypeSkillNotApplicable;
    }

    public void setTypeSkillNotApplicable(boolean isTypeSkillNotApplicable) {
        this.isTypeSkillNotApplicable = isTypeSkillNotApplicable;
    }

    public boolean isSkillNotApplicable() {
        return isSkillNotApplicable;
    }

    public void setSkillNotApplicable(boolean isSkillNotApplicable) {
        this.isSkillNotApplicable = isSkillNotApplicable;
    }

    public void setAnswered(boolean isAnswered) {
        this.isAnswered = isAnswered;
    }

    public ArrayList<String> getRecoverypictures() {
        ArrayList<String> recoveries=new ArrayList<>();
        if(!recoverypictures.equals("")) {
            String[] rs = recoverypictures.split("----");
            for (String r : rs)
                recoveries.add(r);
        }
        return recoveries;
    }
    public String getRecoverypicturesString() {
        return this.recoverypictures;
    }
    public void setRecoverypictures(String recoverypictures) {
        this.recoverypictures = recoverypictures;
    }


    public void addRecoverypicture(String recoverypicture) {
        this.recoverypictures += recoverypicture+"----";
    }

    public boolean isNowRecovery() {
        return nowRecovery;
    }

    public void setNowRecovery(boolean nowRecovery) {
        this.nowRecovery = nowRecovery;
    }

    public ArrayList<Integer> getActionPlans() {
        ArrayList<Integer> actions=new ArrayList<>();
        if(!actionPlans.equals("")) {
            String[] ats = actionPlans.split("----");
            for (String a : ats)
                actions.add(Integer.parseInt(a));
        }
        return actions;
    }
    public String getActionPlansString() {
        return actionPlans;
    }

    public void setActionPlans(String actionPlans) {
        this.actionPlans = actionPlans;
    }

    public void addActionPlan(int actionPlan) {
        this.actionPlans+= actionPlan+"----";
    }

    public float getTotalCost(){
        float i = 0;
        WorkUnit wu=null;
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills())
            for (Skill sk : typeSkill.getSkills())
                for (WorkUnit w : sk.getWorkUnits())
                    if(w.getId()==getWorkUnitId())
                        wu=w;
        if(isNowRecovery())
            i+=wu.getImmediatRecoveryPrice();
        if(wu.getAnswer()!= null && wu.getAnswer().getScore()==1)
            i+=wu.getNonConformityCount()+wu.getNonConformityInternCount();
        return i;
    }


    public boolean isFit() {
        return fit;
    }

    public void setFit(boolean fit) {
        this.fit = fit;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}
