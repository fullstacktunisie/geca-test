package geca.dareliconsulting.smarttoutch.com.geca.model;

import com.orm.SugarRecord;

/**
 * Created by iMac1 on 06/05/15.
 */
public class BusinessManager extends SugarRecord<BusinessManager> {

    int serverId;
    String name;

    public String getName() {
        return name;
    }



    public int getServerId() {
        return serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }



    public BusinessManager() {

    }

    public BusinessManager(int serverId, String name) {
        this.serverId = serverId;
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.getName();
    }
}