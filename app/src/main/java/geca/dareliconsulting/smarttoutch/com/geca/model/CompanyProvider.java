package geca.dareliconsulting.smarttoutch.com.geca.model;

import com.orm.SugarRecord;

/**
 * Created by Smart on 29/04/2015.
 */
public class CompanyProvider extends SugarRecord<CompanyProvider> {

    private int idserveur;
    private String raisonsociale;

    public CompanyProvider(int idserveur, String raisonsociale) {
        this.idserveur = idserveur;
        this.raisonsociale = raisonsociale;
    }

    public CompanyProvider() {
    }

    public int getIdserveur() {
        return idserveur;
    }

    public void setIdserveur(int idserveur) {
        this.idserveur = idserveur;
    }

    public String getRaisonsociale() {
        return raisonsociale;
    }

    public void setRaisonsociale(String raisonsociale) {
        this.raisonsociale = raisonsociale;
    }


    @Override
    public String toString() {
        return this.getRaisonsociale();
    }
}




