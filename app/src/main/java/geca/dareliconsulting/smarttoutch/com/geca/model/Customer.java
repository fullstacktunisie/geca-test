package geca.dareliconsulting.smarttoutch.com.geca.model;

import com.orm.SugarRecord;

/**
 * Created by Smart on 27/04/2015.
 */
public class Customer  extends SugarRecord<Customer>{

    private int idserveur;
    private String customerName;
    private String customerSocial;

    public Customer(int idserveur, String customerName, String customerSocial) {
        this.idserveur = idserveur;
        this.customerName = customerName;
        this.customerSocial = customerSocial;
    }

    public Customer() {
    }

    public int getIdserveur() {
        return idserveur;
    }

    public void setIdserveur(int idserveur) {
        this.idserveur = idserveur;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerSocial() {
        return customerSocial;
    }

    public void setCustomerSocial(String customerSocial) {
        this.customerSocial = customerSocial;
    }


    @Override
    public String toString() {
        return this.getCustomerName();
    }
}
