package geca.dareliconsulting.smarttoutch.com.geca.model;

import com.orm.SugarRecord;

/**
 * Created by Smart on 20/04/2015.
 */
public class Fonction extends SugarRecord<Fonction> {
    private int idFunction;
    private String functionLabel;

    public Fonction(int idFunction, String functionLabel) {
        this.idFunction = idFunction;
        this.functionLabel = functionLabel;
    }

    public Fonction() {
    }

    public Fonction(int idFunction) {
        this.idFunction = idFunction;
    }

    public int getIdFunction() {
        return idFunction;
    }

    public void setIdFunction(int idFunction) {
        this.idFunction = idFunction;
    }

    public String getFunctionLabel() {
        return functionLabel;
    }

    public void setFunctionLabel(String functionLabel) {
        this.functionLabel = functionLabel;
    }


    @Override
    public String toString() {
        return this.getFunctionLabel();
    }
}
