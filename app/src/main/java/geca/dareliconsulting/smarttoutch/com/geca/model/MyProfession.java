package geca.dareliconsulting.smarttoutch.com.geca.model;

import com.orm.SugarRecord;

/**
 * Created by Smart on 28/05/2015.
 */
public class MyProfession  extends SugarRecord<Profession> {
    private int idServer;
    private String label;

    public MyProfession() {
    }

    public MyProfession(int idServer, String label) {
        this.idServer = idServer;
        this.label = label;
    }

    public int getIdServer() {
        return idServer;
    }

    public void setIdServer(int idServer) {
        this.idServer = idServer;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }


    @Override
    public String toString() {
        return getLabel();
    }
}
