package geca.dareliconsulting.smarttoutch.com.geca.model;

import com.orm.SugarRecord;

/**
 * Created by Smart on 28/05/2015.
 */
public class MyTypeSkills extends SugarRecord<MyTypeSkills> {

    private int idTypeSkill ;
    private String typeSkillLabel;


    public MyTypeSkills() {
    }

    public MyTypeSkills(int idTypeSkill, String typeSkillLabel) {
        this.idTypeSkill = idTypeSkill;
        this.typeSkillLabel = typeSkillLabel;
    }

    public int getIdTypeSkill() {
        return idTypeSkill;
    }

    public void setIdTypeSkill(int idTypeSkill) {
        this.idTypeSkill = idTypeSkill;
    }

    public String getTypeSkillLabel() {
        return typeSkillLabel;
    }

    public void setTypeSkillLabel(String typeSkillLabel) {
        this.typeSkillLabel = typeSkillLabel;
    }
}
