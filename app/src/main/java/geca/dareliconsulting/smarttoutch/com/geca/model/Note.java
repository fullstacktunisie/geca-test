package geca.dareliconsulting.smarttoutch.com.geca.model;

import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import geca.dareliconsulting.smarttoutch.com.geca.singleton.Singleton;

/**
 * Created by Smart on 14/04/2015.
 */
public class Note extends SugarRecord<Note> implements Serializable {
    //section 1
    private String label;
    private String type;
    private String status = "IN_PROGRESS";
    private boolean synchronised = false;
    private int operationTypeId;
    private int sectorId;
    private int professionFamilyId;
    private int professionId;
    private int customerId;
    private int customerId2;
    private String siteNumber;
    private String otNumber;
    private Date date;
    private String auditorId;
    private String duration;
    private boolean technicianPresent;
    private Long technicianId;
    private int technicianServerId;
    private String technicianRegistrationNumber;
    private String technicianFunction;
    private int technicianRealFunction;
    private int technicianEvolutionProfession;
    private String technicianEvolutionProfessionComment;
    private String technicianPicture;
    private boolean abandoned;
    private String motifOfAbandonment;
    private String auditorSignature;
    private String technicianSignature;
    private int globalScore;
    private int auditorGlobalScore;
    private String priceOfNonconformity;
    private float priceOfPenality=0;
    private boolean granted;
    private String motifGrant;
    private String picture1;
    private String picture2;
    private String picture3;
    private String comment1;
    private String comment2;
    private String comment3;
    private long created = 0;
    private long updated = 0;
    private String securityAlert;
    private String listSkillisNotApplicable;
    private String listTypeSkillNotApplicable;
    private String listTypeSkillWithSecurityAlert;
    private Double longitude;
    private Double latitude;
    private boolean certification;
    private int nextAudit;
    private String siteAmount;
    private ArrayList<TypeSkill> typeSkills;
    private String operationsDetails;
    //private int serverId;

    public Note() {
    }


    public Note(String label, String type, String status, boolean synchronised, int professionId, int customerId, String siteNumber, String otNumber, Date date, String auditorId, boolean technicianPresent, Long technicianId) {
        this.label = label;
        this.type = type;
        this.status = status;
        this.synchronised = synchronised;
        this.professionId = professionId;
        this.customerId = customerId;
        this.siteNumber = siteNumber;
        this.otNumber = otNumber;
        this.date = date;
        this.auditorId = auditorId;
        this.technicianPresent = technicianPresent;
        this.technicianId = technicianId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isSynchronised() {
        return synchronised;
    }

    public void setSynchronised(boolean synchronised) {
        this.synchronised = synchronised;
    }

    public int getProfessionId() {
        return professionId;
    }

    public void setProfessionId(int professionId) {
        this.professionId = professionId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getSiteNumber() {
        return siteNumber;
    }

    public void setSiteNumber(String siteNumber) {
        this.siteNumber = siteNumber;
    }

    public String getOtNumber() {
        return otNumber;
    }

    public void setOtNumber(String otNumber) {
        this.otNumber = otNumber;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getAuditorId() {
        return auditorId;
    }

    public void setAuditorId(String auditorId) {
        this.auditorId = auditorId;
    }

    public boolean isTechnicianPresent() {
        return technicianPresent;
    }

    public void setTechnicianPresent(boolean technicianPresent) {
        this.technicianPresent = technicianPresent;
    }

    public Long getTechnicianId() {
        return technicianId;
    }

    public void setTechnicianId(Long technicianId) {
        this.technicianId = technicianId;
    }

    public String getTechnicianRegistrationNumber() {
        return technicianRegistrationNumber;
    }

    public void setTechnicianRegistrationNumber(String technicianRegistrationNumber) {
        this.technicianRegistrationNumber = technicianRegistrationNumber;
    }

    public String getTechnicianFunction() {
        return technicianFunction;
    }

    public void setTechnicianFunction(String technicianFunction) {
        this.technicianFunction = technicianFunction;
    }

    public int getTechnicianRealFunction() {
        return technicianRealFunction;
    }

    public void setTechnicianRealFunction(int technicianRealFunction) {
        this.technicianRealFunction = technicianRealFunction;
    }

    public String getTechnicianPicture() {
        return technicianPicture;
    }

    public void setTechnicianPicture(String technicianPicture) {
        this.technicianPicture = technicianPicture;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public boolean isAbandoned() {
        return abandoned;
    }

    public void setAbandoned(boolean abandoned) {
        this.abandoned = abandoned;
    }

    public String getMotifOfAbandonment() {
        return motifOfAbandonment;
    }

    public void setMotifOfAbandonment(String motifOfAbandonment) {
        this.motifOfAbandonment = motifOfAbandonment;
    }

    public String getAuditorSignature() {
        return auditorSignature;
    }

    public void setAuditorSignature(String auditorSignature) {
        this.auditorSignature = auditorSignature;
    }

    public String getTechnicianSignature() {
        return technicianSignature;
    }

    public void setTechnicianSignature(String technicianSignature) {
        this.technicianSignature = technicianSignature;
    }

    public int getGlobalScore() {
        return globalScore;
    }

    public void setGlobalScore(int globalScore) {
        this.globalScore = globalScore;
    }

    public int getAuditorGlobalScore() {
        return auditorGlobalScore;
    }

    public void setAuditorGlobalScore(int auditorGlobalScore) {
        this.auditorGlobalScore = auditorGlobalScore;
    }

    public String getPriceOfNonconformity() {
        return priceOfNonconformity;
    }

    public void setPriceOfNonconformity(String priceOfNonconformity) {
        this.priceOfNonconformity = priceOfNonconformity;
    }

    public boolean isGranted() {
        return granted;
    }

    public void setGranted(boolean granted) {
        this.granted = granted;
    }

    public String getMotifGrant() {
        return motifGrant;
    }

    public void setMotifGrant(String motifGrant) {
        this.motifGrant = motifGrant;
    }

    public void setFields(String label, String type, String status, boolean synchronised, int professionId, int customerId, String siteNumber, String otNumber, Date date, String auditorId, boolean technicianPresent, Long technicianId) {
        this.label = label;
        this.type = type;
        this.status = status;
        this.synchronised = synchronised;
        this.professionId = professionId;
        this.customerId = customerId;
        this.siteNumber = siteNumber;
        this.otNumber = otNumber;
        this.date = date;
        this.auditorId = auditorId;
        this.technicianPresent = technicianPresent;
        this.technicianId = technicianId;
    }

    public int getSectorId() {
        return sectorId;
    }

    public void setSectorId(int sectorId) {
        this.sectorId = sectorId;
    }

    public int getProfessionFamilyId() {
        return professionFamilyId;
    }

    public void setProfessionFamilyId(int professionFamilyId) {
        this.professionFamilyId = professionFamilyId;
    }

    public String getPicture1() {
        return picture1;
    }

    public void setPicture1(String picture1) {
        this.picture1 = picture1;
    }

    public String getPicture2() {
        return picture2;
    }

    public void setPicture2(String picture2) {
        this.picture2 = picture2;
    }

    public String getPicture3() {
        return picture3;
    }

    public void setPicture3(String picture3) {
        this.picture3 = picture3;
    }

    public String getComment1() {
        return comment1;
    }

    public void setComment1(String comment1) {
        this.comment1 = comment1;
    }

    public String getComment2() {
        return comment2;
    }

    public void setComment2(String comment2) {
        this.comment2 = comment2;
    }

    public String getComment3() {
        return comment3;
    }

    public void setComment3(String comment3) {
        this.comment3 = comment3;
    }

    public int getTechnicianEvolutionProfession() {
        return technicianEvolutionProfession;
    }

    public void setTechnicianEvolutionProfession(int technicianEvolutionProfession) {
        this.technicianEvolutionProfession = technicianEvolutionProfession;
    }

    public String getTechnicianEvolutionProfessionComment() {
        return technicianEvolutionProfessionComment;
    }

    public void setTechnicianEvolutionProfessionComment(String technicianEvolutionProfessionComment) {
        this.technicianEvolutionProfessionComment = technicianEvolutionProfessionComment;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getUpdated() {
        return updated;
    }

    public void setUpdated(long updated) {
        this.updated = updated;
    }

    public int getTechnicianServerId() {
        return technicianServerId;
    }

    public void setTechnicianServerId(int technicianServerId) {
        this.technicianServerId = technicianServerId;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getSecurityAlert() {
        return securityAlert;
    }

    public void setSecurityAlert(String securityAlert) {
        this.securityAlert = securityAlert;
    }

    public String getListSkillisNotApplicable() {
        return listSkillisNotApplicable;
    }

    public void setListSkillisNotApplicable(String listSkillisNotApplicable) {
        this.listSkillisNotApplicable = listSkillisNotApplicable;
    }

    public String getListTypeSkillNotApplicable() {
        return listTypeSkillNotApplicable;
    }

    public void setListTypeSkillNotApplicable(String listTypeSkillNotApplicable) {
        this.listTypeSkillNotApplicable = listTypeSkillNotApplicable;
    }

    public String getListTypeSkillWithSecurityAlert() {
        return listTypeSkillWithSecurityAlert;
    }

    public void setListTypeSkillWithSecurityAlert(String listTypeSkillWithSecurityAlert) {
        this.listTypeSkillWithSecurityAlert = listTypeSkillWithSecurityAlert;
    }

    public boolean isCertification() {
        return certification;
    }

    public void setCertification(boolean certification) {
        this.certification = certification;
    }

    public float getPriceOfPenality() {
        return priceOfPenality;
    }

    public void setPriceOfPenality(float priceOfPenality) {
        this.priceOfPenality = priceOfPenality;
    }

    public boolean validActionPlans() {
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            for (Skill sk : typeSkill.getSkills())
                for (WorkUnit wu : sk.getWorkUnits()) {
                    if (wu.getAnswer()!=null && wu.getAnswer().getScore() == 1 && wu.getAnswer().getActionPlans().size() < 1) {
                        wu.getAnswer().setError(true);
                        return false;
                    }else if(wu.getAnswer()!=null)
                        wu.getAnswer().setError(false);
                }

        }
        return true;
    }

    public boolean validComments() {
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            for (Skill sk : typeSkill.getSkills())
                for (WorkUnit wu : sk.getWorkUnits()) {
                    if (wu.getAnswer()!=null) {
                        if ((wu.getAnswer().getScore() == 1 || wu.getAnswer().isHasRecovery()) && wu.getAnswer().getComment().equals("")) {
                            wu.getAnswer().setError(true);
                            return false;
                        } else
                            wu.getAnswer().setError(false);
                    }
                }

        }
        return true;
    }

    public int getControledWU() {
        int i = 0;
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            for (Skill sk : typeSkill.getSkills())
                for (WorkUnit wu : sk.getWorkUnits()) {
                    if (wu.getAnswer()!=null && wu.getAnswer().getScore() > 0 )
                        i++;
                }

        }
        return i;
    }

    public int getAllWU() {
        int i = 0;
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            for (Skill sk : typeSkill.getSkills())
                i+=sk.getWorkUnits().size();
        }
        return i;
    }

    public int getNCWU() {
        int i = 0;
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            for (Skill sk : typeSkill.getSkills())
                for (WorkUnit wu : sk.getWorkUnits()) {
                    if (wu.getAnswer()!=null && wu.getAnswer().getScore() ==1 )
                        i++;
                }

        }
        return i;
    }

    public int getCWU() {
        int i = 0;
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            for (Skill sk : typeSkill.getSkills())
                for (WorkUnit wu : sk.getWorkUnits()) {
                    if (wu.getAnswer()!=null && wu.getAnswer().getScore() ==2 )
                        i++;
                }

        }
        return i;
    }

    public int getExWU() {
        int i = 0;
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            for (Skill sk : typeSkill.getSkills())
                for (WorkUnit wu : sk.getWorkUnits()) {
                    if (wu.getAnswer()!=null && wu.getAnswer().getScore() ==3 )
                        i++;
                }

        }
        return i;
    }

    public int getNCWUMaj() {
        int i = 0;
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            for (Skill sk : typeSkill.getSkills())
                for (WorkUnit wu : sk.getWorkUnits()) {
                    if (wu.getAnswer()!=null && wu.getAnswer().getScore() == 1 && wu.getImpactType().equalsIgnoreCase("majeur"))
                        i++;
                }

        }
        return i;
    }
    public int getNCWUMin() {
        int i = 0;
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            for (Skill sk : typeSkill.getSkills())
                for (WorkUnit wu : sk.getWorkUnits()) {
                    if (wu.getAnswer()!=null && wu.getAnswer().getScore() == 1 && wu.getImpactType().equalsIgnoreCase("mineur"))
                        i++;
                }

        }
        return i;
    }
    public int getRecoveryWUMaj() {
        int i = 0;
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            for (Skill sk : typeSkill.getSkills())
                for (WorkUnit wu : sk.getWorkUnits()) {
                    if (wu.getAnswer()!=null && wu.getAnswer().isHasRecovery() && wu.getImpactType().equalsIgnoreCase("majeur"))
                        i++;
                }

        }
        return i;
    }
    public int getRecoveryWUMin() {
        int i = 0;
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            for (Skill sk : typeSkill.getSkills())
                for (WorkUnit wu : sk.getWorkUnits()) {
                    if (wu.getAnswer()!=null &&  wu.getAnswer().isHasRecovery() && wu.getImpactType().equalsIgnoreCase("mineur"))
                        i++;
                }

        }
        return i;
    }
    public int getRecoveryWU() {
        int i = 0;
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            for (Skill sk : typeSkill.getSkills())
                for (WorkUnit wu : sk.getWorkUnits()) {
                    if (wu.getAnswer()!=null &&  wu.getAnswer().isHasRecovery())
                        i++;
                }

        }
        return i;
    }
    public int getRecoveryNowWUMaj() {
        int i = 0;
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            for (Skill sk : typeSkill.getSkills())
                for (WorkUnit wu : sk.getWorkUnits()) {
                    if (wu.getAnswer()!=null &&  wu.getAnswer().isNowRecovery() && wu.getImpactType().equalsIgnoreCase("majeur"))
                        i++;
                }

        }
        return i;
    }
    public int getRecoveryNowWUMin() {
        int i = 0;
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            for (Skill sk : typeSkill.getSkills())
                for (WorkUnit wu : sk.getWorkUnits()) {
                    if (wu.getAnswer()!=null &&  wu.getAnswer().isNowRecovery() && wu.getImpactType().equalsIgnoreCase("mineur"))
                        i++;
                }

        }
        return i;
    }
    public int getRecoveryNowWU() {
        int i = 0;
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            for (Skill sk : typeSkill.getSkills())
                for (WorkUnit wu : sk.getWorkUnits()) {
                    if (wu.getAnswer()!=null &&  wu.getAnswer().isNowRecovery())
                        i++;
                }

        }
        return i;
    }

    public int getFITWU() {
        int i = 0;
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            for (Skill sk : typeSkill.getSkills())
                for (WorkUnit wu : sk.getWorkUnits()) {
                    if (wu.getAnswer()!=null &&  wu.getAnswer().isFit())
                        i++;
                }

        }
        return i;
    }

    public ArrayList<WorkUnit> getAllFIT() {
        ArrayList<WorkUnit> fit=new ArrayList<>();
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            for (Skill sk : typeSkill.getSkills())
                for (WorkUnit wu : sk.getWorkUnits()) {
                    if (wu.getAnswer()!=null &&  wu.getAnswer().isFit())
                        fit.add(wu);
                }

        }
        return fit;
    }

    public int getAllRecoveryWU() {
        int i = 0;
        i+=getRecoveryNowWU();
        i+=getRecoveryWU();
        return i;
    }


    public float getCostClientWUMaj() {
        float i = 0;
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            for (Skill sk : typeSkill.getSkills())
                for (WorkUnit wu : sk.getWorkUnits()) {
                    if (wu.getAnswer()!=null && wu.getAnswer().getScore() == 1 && wu.getImpactType().equalsIgnoreCase("majeur"))
                        i+=wu.getNonConformityCount();
                }

        }
        return i;
    }
    public float getCostClientWUMin() {
        float i = 0;
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            for (Skill sk : typeSkill.getSkills())
                for (WorkUnit wu : sk.getWorkUnits()) {
                    if (wu.getAnswer()!=null && wu.getAnswer().getScore() == 1 && wu.getImpactType().equalsIgnoreCase("mineur"))
                        i+=wu.getNonConformityCount();
                }

        }
        return i;
    }

    public float getCostClientWU() {
        float i = 0;
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            for (Skill sk : typeSkill.getSkills())
                for (WorkUnit wu : sk.getWorkUnits()) {
                    if (wu.getAnswer()!=null && wu.getAnswer().getScore() == 1)
                        i+=wu.getNonConformityCount();
                }

        }
        return i;
    }

    public float getCostInternWUMaj() {
        float i = 0;
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            for (Skill sk : typeSkill.getSkills())
                for (WorkUnit wu : sk.getWorkUnits()) {
                    if (wu.getAnswer()!=null && wu.getAnswer().getScore() == 1 && wu.getImpactType().equalsIgnoreCase("majeur"))
                        i+=wu.getNonConformityInternCount();
                }

        }
        return i;
    }
    public float getCostInternWUMin() {
        float i = 0;
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            for (Skill sk : typeSkill.getSkills())
                for (WorkUnit wu : sk.getWorkUnits()) {
                    if (wu.getAnswer()!=null && wu.getAnswer().getScore() == 1 && wu.getImpactType().equalsIgnoreCase("mineur"))
                        i+=wu.getNonConformityInternCount();
                }

        }
        return i;
    }

    public float getCostInternWU() {
        float i = 0;
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            for (Skill sk : typeSkill.getSkills())
                for (WorkUnit wu : sk.getWorkUnits()) {
                    if (wu.getAnswer()!=null && wu.getAnswer().getScore() == 1)
                        i+=wu.getNonConformityInternCount();
                }

        }
        return i;
    }

    public float getCostRecoveryNowWUMaj() {
        float i = 0;
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            for (Skill sk : typeSkill.getSkills())
                for (WorkUnit wu : sk.getWorkUnits()) {
                    if (wu.getAnswer()!=null && wu.getAnswer().getScore() == 1 && wu.getImpactType().equalsIgnoreCase("majeur") && wu.getAnswer().isNowRecovery())
                        i+=wu.getImmediatRecoveryPrice();
                }

        }
        return i;
    }
    public float getCostRecoveryNowWUMin() {
        float i = 0;
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            for (Skill sk : typeSkill.getSkills())
                for (WorkUnit wu : sk.getWorkUnits()) {
                    if (wu.getAnswer()!=null && wu.getAnswer().getScore() == 1 && wu.getImpactType().equalsIgnoreCase("mineur") && wu.getAnswer().isNowRecovery())
                        i+=wu.getImmediatRecoveryPrice();
                }

        }
        return i;
    }

    public float getCostRecoveryNowWU() {
        float i = 0;
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            for (Skill sk : typeSkill.getSkills())
                for (WorkUnit wu : sk.getWorkUnits()) {
                    if (wu.getAnswer()!=null && wu.getAnswer().getScore() == 1 && wu.getAnswer().isNowRecovery())
                        i+=wu.getImmediatRecoveryPrice();
                }

        }
        if(type.equals("VISITE_TECHNIQUE"))
            i+=getPriceOfPenality();
        return i;
    }

    public float getTotalCost(){
        float i = 0;
        i+=getCostClientWU();
        i+=getCostRecoveryNowWU();
        i+=getCostInternWU();
        return i;
    }

    public boolean hasRecovery() {
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            for (Skill sk : typeSkill.getSkills())
                for (WorkUnit wu : sk.getWorkUnits()) {
                    if (wu.getAnswer()!=null && (wu.getAnswer().isHasRecovery() || wu.getAnswer().isNowRecovery()))
                        return true;
                }

        }
        return false;
    }

    RecoveryNumber containsRecoveryDelay(ArrayList<RecoveryNumber> recoveryNumbers,int delay){
        for(RecoveryNumber r:recoveryNumbers)
            if(r.getDelay()==delay)
                return r;
        return null;
    }
    public ArrayList<RecoveryNumber> getRecoveryNumbers(){
        ArrayList<RecoveryNumber> recoveryNumbers=new ArrayList<>();
        for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNoteProfession().getTypeSkills()) {
            for (Skill sk : typeSkill.getSkills())
                for (WorkUnit wu : sk.getWorkUnits()) {
                    if (wu.getAnswer()!=null && wu.getAnswer().isHasRecovery()){
                        RecoveryNumber r=containsRecoveryDelay(recoveryNumbers,wu.getRecoveryTime());
                        if(r==null)
                            recoveryNumbers.add(new RecoveryNumber(wu.getRecoveryTime()));
                        else
                            r.incNumber();
                    }
                }
        }
        return recoveryNumbers;
    }

    public int getOperationTypeId() {
        return operationTypeId;
    }

    public void setOperationTypeId(int operationTypeId) {
        this.operationTypeId = operationTypeId;
    }

    public int getNextAudit() {
        return nextAudit;
    }

    public void setNextAudit(int nextAudit) {
        this.nextAudit = nextAudit;
    }

    public String getSiteAmount() {
        return siteAmount;
    }

    public void setSiteAmount(String siteAmount) {
        this.siteAmount = siteAmount;
    }

    public ArrayList<TypeSkill> getTypeSkills() {
        return typeSkills;
    }

    public void setTypeSkills(ArrayList<TypeSkill> typeSkills) {
        this.typeSkills = typeSkills;
    }

    public String getOperationsDetails() {
        return operationsDetails;
    }

    public void setOperationsDetails(String operationsDetails) {
        this.operationsDetails = operationsDetails;
    }

    public int getCustomerId2() {
        return customerId2;
    }

    public void setCustomerId2(int customerId2) {
        this.customerId2 = customerId2;
    }
}
