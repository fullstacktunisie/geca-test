package geca.dareliconsulting.smarttoutch.com.geca.model;

import com.orm.SugarRecord;

/**
 * Created by fullstack on 7/31/17.
 * We Code For Fun
 */

public class NoteCompetence extends SugarRecord<NoteCompetence> {

    private int position;
    private int value;

    public NoteCompetence(int position, int value) {
        this.position = position;
        this.value = value;
    }

    public NoteCompetence() {

    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
