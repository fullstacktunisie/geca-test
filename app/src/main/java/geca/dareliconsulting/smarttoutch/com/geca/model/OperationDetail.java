package geca.dareliconsulting.smarttoutch.com.geca.model;

import com.orm.SugarRecord;

/**
 * Created by HP on 11/07/2017.
 */
public class OperationDetail extends SugarRecord<OperationDetail> {

    int operationDetailId;
    String description;

    public OperationDetail() {
    }

    public OperationDetail(int operationDetailId, String description) {
        this.operationDetailId = operationDetailId;
        this.description = description;
    }

    public int getOperationDetailId() {
        return operationDetailId;
    }

    public void setOperationDetailId(int operationDetailId) {
        this.operationDetailId = operationDetailId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return description;
    }
}
