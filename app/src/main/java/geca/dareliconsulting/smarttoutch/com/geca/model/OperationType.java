package geca.dareliconsulting.smarttoutch.com.geca.model;

import com.orm.SugarRecord;

/**
 * Created by iMac1 on 11/05/15.
 */
public class OperationType extends SugarRecord<OperationType> {

    int serverId;
    String name;
    String amount;

    public OperationType(String name, String amount) {
        this.name = name;
        this.amount=amount;
    }

    public OperationType() {
    }

    public int getServerId() {
        return serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return getName();
    }
}
