package geca.dareliconsulting.smarttoutch.com.geca.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Smart on 10/04/2015.
 */
public class Profession    implements Serializable{

    private int idServer;
    private String label;
    private ArrayList<TypeSkill> typeSkills;

    public Profession() {
    }

    public ArrayList<TypeSkill> getTypeSkills() {
        return typeSkills;
    }

    public void setTypeSkills(ArrayList<TypeSkill> typeSkills) {
        this.typeSkills = typeSkills;
    }

    public Profession(int id, String label) {
        this.idServer = id;
        this.label = label;
    }

    public Profession(int id, String label, ArrayList<TypeSkill> typeSkills) {
        this.idServer = id;
        this.label = label;
        this.typeSkills = typeSkills;
    }

    public int getIdServer() {
        return idServer;
    }

    public void setIdServer(int idServer) {
        this.idServer = idServer;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return this.getLabel();
    }


}
