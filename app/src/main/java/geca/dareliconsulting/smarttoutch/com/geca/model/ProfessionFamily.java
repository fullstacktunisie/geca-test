package geca.dareliconsulting.smarttoutch.com.geca.model;

import java.util.ArrayList;

/**
 * Created by Smart on 09/04/2015.
 */
public class ProfessionFamily {
    private int id;
    private String label;
    private ArrayList<Profession> professions;

    public ProfessionFamily(int id, String label, ArrayList<Profession> professions) {
        this.id = id;
        this.label = label;
        this.professions = professions;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public ArrayList<Profession> getProfessions() {
        return professions;
    }

    public void setProfessions(ArrayList<Profession> professions) {
        this.professions = professions;
    }

    @Override
    public String toString() {
        return this.label;
    }
}