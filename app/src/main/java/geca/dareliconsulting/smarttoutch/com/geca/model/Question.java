package geca.dareliconsulting.smarttoutch.com.geca.model;

import java.io.Serializable;

/**
 * Created by Smart on 10/04/2015.
 */
public class Question implements Serializable {
    private int id;
    private int typeSkill;
    private int skill;
    private String questionText;
    private boolean questionIsTrue;
    private String comment;
    private Answer answer;

    public int getTypeSkill() {
        return typeSkill;
    }

    public void setTypeSkill(int typeSkill) {
        this.typeSkill = typeSkill;
    }

    public int getSkill() {
        return skill;
    }

    public void setSkill(int skill) {
        this.skill = skill;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Question(int id, int typeSkill, int skill, String questionText, boolean questionIsTrue, String comment) {
        this.id = id;
        this.typeSkill = typeSkill;
        this.skill = skill;
        this.questionText = questionText;
        this.questionIsTrue = questionIsTrue;
        this.comment = comment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public boolean isQuestionIsTrue() {
        return questionIsTrue;
    }

    public void setQuestionIsTrue(boolean questionIsTrue) {
        this.questionIsTrue = questionIsTrue;
    }

    @Override
    public String toString() {
        return this.questionText;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }
}
