package geca.dareliconsulting.smarttoutch.com.geca.model;

/**
 * Created by HP on 15/09/2016.
 */
public class RecoveryNumber {
    private int delay;
    private int number=1;

    public RecoveryNumber(int delay) {
        this.delay = delay;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
    public void incNumber() {
        this.number++;
    }
}
