package geca.dareliconsulting.smarttoutch.com.geca.model;

import com.orm.SugarRecord;

import java.util.Date;

/**
 * Created by Smart on 20/04/2015.
 */
public class RequestTraining extends SugarRecord<RequestTraining>{

    private int serverId;
    private int trainingId;
    private int skillId;
    private String nametrainer;
    private Date datestart;
    private String duration;
    private String comment;
    private String listTechniciens;
    private String listTechnicienssend;
    private boolean isSynchron;
    private int type;
    private String trainingObject="";
    private int numTraining;
    public RequestTraining() {
    }

    public RequestTraining(int serverId, int trainingId, int skillId, String nametrainer, Date datestart, String duration, String comment, String listTechniciens) {
        this.serverId = serverId;
        this.trainingId = trainingId;
        this.skillId = skillId;
        this.nametrainer = nametrainer;
        this.datestart = datestart;
        this.duration = duration;
        this.comment = comment;
        this.listTechniciens = listTechniciens;
    }

    public int getServerId() {
        return serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public int getTrainingId() {
        return trainingId;
    }

    public void setTrainingId(int trainingId) {
        this.trainingId = trainingId;
    }

    public int getSkillId() {
        return skillId;
    }

    public void setSkillId(int skillId) {
        this.skillId = skillId;
    }

    public String getNametrainer() {
        return nametrainer;
    }

    public void setNametrainer(String nametrainer) {
        this.nametrainer = nametrainer;
    }

    public Date getDatestart() {
        return datestart;
    }

    public void setDatestart(Date datestart) {
        this.datestart = datestart;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getListTechniciens() {
        return listTechniciens;
    }

    public void setListTechniciens(String listTechniciens) {
        this.listTechniciens = listTechniciens;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


    public boolean isSynchron() {
        return isSynchron;
    }

    public String getListTechnicienssend() {
        return listTechnicienssend;
    }

    public void setListTechnicienssend(String listTechnicienssend) {
        this.listTechnicienssend = listTechnicienssend;
    }

    public void setSynchron(boolean isSynchron) {
        this.isSynchron = isSynchron;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getNumTraining() {
        return numTraining;
    }

    public void setNumTraining(int numTraining) {
        this.numTraining = numTraining;
    }

    public String getTrainingObject() {
        return trainingObject;
    }

    public void setTrainingObject(String trainingObject) {
        this.trainingObject = trainingObject;
    }
}
