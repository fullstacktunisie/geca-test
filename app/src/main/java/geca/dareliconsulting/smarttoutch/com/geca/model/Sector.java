package geca.dareliconsulting.smarttoutch.com.geca.model;

import java.util.ArrayList;

/**
 * Created by Smart on 09/04/2015.
 */
public class Sector {
    private int id;
    private  String sectorbabel;
    private ArrayList<ProfessionFamily> listactivityFamilies;

    public Sector(int id, String sectorbabel, ArrayList<ProfessionFamily> listactivityFamilies) {
        this.id = id;
        this.sectorbabel = sectorbabel;
        this.listactivityFamilies = listactivityFamilies;
    }

    public Sector(int id, String sectorbabel) {
        this.id = id;
        this.sectorbabel = sectorbabel;
    }

    public Sector() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<ProfessionFamily> getListactivityFamilies() {
        return listactivityFamilies;
    }

    public void setListactivityFamilies(ArrayList<ProfessionFamily> listactivityFamilies) {
        this.listactivityFamilies = listactivityFamilies;
    }

    public String getSectorbabel() {
        return sectorbabel;
    }

    public void setSectorbabel(String sectorbabel) {
        this.sectorbabel = sectorbabel;
    }

    @Override
    public String toString() {
        return this.getSectorbabel();
    }
}
