package geca.dareliconsulting.smarttoutch.com.geca.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Smart on 10/04/2015.
 */
public class Skill implements Serializable {
    private int id;
    private String label;
    private ArrayList<Question> questions;
    private ArrayList<WorkUnit> workUnits;
    private boolean notApplicable;
    private boolean typeSkillNotApplicable;
    private boolean toCollapse = false;
    private boolean toTypeCollapse = false;
    private int score = -1;

    public ArrayList<WorkUnit> getWorkUnits() {
        return workUnits;
    }

    public void setWorkUnits(ArrayList<WorkUnit> workUnits) {
        this.workUnits = workUnits;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public ArrayList<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<Question> questions) {
        this.questions = questions;
    }

    public Skill(int id, String competencelabel, ArrayList<Question> questions, ArrayList<WorkUnit> workUnits) {
        this.id = id;
        this.label = competencelabel;
        this.questions = questions;
        this.workUnits = workUnits;
    }

    @Override
    public String toString() {
        return this.label;
    }

    public boolean isNotApplicable() {
        return notApplicable;
    }

    public void setNotApplicable(boolean notApplicable) {
        this.notApplicable = notApplicable;
    }

    public boolean isTypeSkillNotApplicable() {
        return typeSkillNotApplicable;
    }

    public void setTypeSkillNotApplicable(boolean typeSkillNotApplicable) {
        this.typeSkillNotApplicable = typeSkillNotApplicable;
    }

    public boolean isToCollapse() {
        return toCollapse;
    }

    public void setToCollapse(boolean toCollapse) {
        this.toCollapse = toCollapse;
    }

    public boolean isTypeToCollapse() {
        return toTypeCollapse;
    }

    public void setTypeToCollapse(boolean toCollapse) {
        this.toTypeCollapse = toCollapse;
    }

    public boolean isToTypeCollapse() {
        return toTypeCollapse;
    }

    public void setToTypeCollapse(boolean toTypeCollapse) {
        this.toTypeCollapse = toTypeCollapse;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
