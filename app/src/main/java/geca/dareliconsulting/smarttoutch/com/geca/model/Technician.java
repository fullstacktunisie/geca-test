package geca.dareliconsulting.smarttoutch.com.geca.model;

import com.orm.SugarRecord;

import java.util.List;

/**
 * Created by Smart on 23/04/2015.
 */
public class Technician extends SugarRecord<Technician> {

    private int serverId = 0;
    private int companyId;
    private String functionLabel;
    private int functionId;
    private int businessResponsibleId;
    private int agencyId;

    private String name;
    private String surName;
    private String socialSecurityNumber;
    private String  startDate =null;
    private String endDate=null;
    private String email;
    private String phone;
    private String registrationNumber;
    private String localImage;
    private boolean isEdit;
    private boolean isselected;

    private String formation;
    private String experience;

    public Technician() {
    }

    public Technician(String localImage,String name, String surName, String socialSecurityNumber,String startDate, String endDate, String email, String phone) {
        this.localImage=localImage;
        this.name = name;
        this.surName = surName;
        this.socialSecurityNumber = socialSecurityNumber;
        this.startDate = startDate;
        this.endDate = endDate;
        this.email = email;
        this.phone = phone;
    }


    public Technician(String name, String surName, String socialSecurityNumber,String startDate, String endDate, String email, String phone) {

        this.name = name;
        this.surName = surName;
        this.socialSecurityNumber = socialSecurityNumber;
        this.startDate = startDate;
        this.endDate = endDate;
        this.email = email;
        this.phone = phone;
    }



    public Technician(int serverId, int companyId, String name, String surName, String socialSecurityNumber, int agencyId, String function, String startDate, String endDate, String email, String phone, String registrationNumber, int businessResponsibleId) {
        this.serverId = serverId;
        this.companyId = companyId;
        this.name = name;
        this.surName = surName;
        this.socialSecurityNumber = socialSecurityNumber;
        this.agencyId = agencyId;
        this.functionLabel = function;
        this.startDate = startDate;
        this.endDate = endDate;
        this.email = email;
        this.phone = phone;
        this.registrationNumber = registrationNumber;
        this.businessResponsibleId = businessResponsibleId;
    }

    public Technician(String name, String surName, String email, String phone) {
        this.name = name;
        this.surName = surName;
        this.email = email;
        this.phone = phone;
    }

    public int getServerId() {
        return serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public int getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(int agencyId) {
        this.agencyId = agencyId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getFunction() {
        return functionLabel;
    }

    public void setFunction(String function) {
        this.functionLabel = function;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public int getBusinessResponsibleId() {
        return businessResponsibleId;
    }

    public void setBusinessResponsibleId(int businessResponsibleId) {
        this.businessResponsibleId = businessResponsibleId;
    }


    public String getLocalImage() {
        return localImage;
    }

    public void setLocalImage(String localImage) {
        this.localImage = localImage;
    }

    @Override
    public String toString() {
        return this.getName();
    }

    public boolean isEdit() {
        return isEdit;
    }

    public void setEdit(boolean isEdit) {
        this.isEdit = isEdit;
    }

    public int getFunctionId() {
        return functionId;
    }

    public void setFunctionId(int functionId) {
        this.functionId = functionId;
    }

    public String getFunctionLabel() {
        return functionLabel;
    }

    public void setFunctionLabel(String functionLabel) {
        this.functionLabel = functionLabel;
    }

    public boolean isIsselected() {
        return isselected;
    }

    public void setIsselected(boolean isselected) {
        this.isselected = isselected;
    }

    public  int  checkTechnicienExist(Technician technician)
    {
        int  exist = 0;
        List<Technician> techniciancheckregisterNumbers =  Technician.find(Technician.class," registration_number  =  ? ", technician.getRegistrationNumber());
        List<Technician> technicianchecksecurityNumber =  Technician.find(Technician.class," social_security_number  =  ? ", technician.getSocialSecurityNumber());
        List<Technician> techniciancheckemail =  Technician.find(Technician.class," email  =  ? ", technician.getEmail());

        if(techniciancheckregisterNumbers.size() > 0 )
        {
          exist = 1;
        }
        else if( technicianchecksecurityNumber.size() > 0)
    {
        exist = 2;
    }
        else if(techniciancheckemail.size()>0)
    {
        exist = 3;
    }
        return exist;

    }


    public String getFormation() {
        return formation;
    }

    public void setFormation(String formation) {
        this.formation = formation;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }
}
