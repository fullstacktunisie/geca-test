package geca.dareliconsulting.smarttoutch.com.geca.model;

import com.orm.SugarRecord;

/**
 * Created by iMac1 on 11/05/15.
 */
public class Training extends SugarRecord<Training> {

    int serverId;


    String name;

    public Training() {
    }

    public Training(int serverId, String name) {
        this.serverId = serverId;
        this.name = name;
    }

    public int getServerId() {
        return serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return getName();
    }
}
