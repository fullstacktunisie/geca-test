package geca.dareliconsulting.smarttoutch.com.geca.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Smart on 29/04/2015.
 */
public class TypeSkill implements Serializable{

    private int idTypeSkill ;
    private String typeSkillLabel;
    private ArrayList<Skill> skills;
    private boolean notApplicable;
    private boolean withAlertSecurity;
    private boolean toCollapse;

    public TypeSkill(int idTypeSkill, String typeSkillLabel, ArrayList<Skill> skills) {
        this.idTypeSkill = idTypeSkill;
        this.typeSkillLabel = typeSkillLabel;
        this.skills = skills;
    }

    public TypeSkill() {
    }

    public int getIdTypeSkill() {
        return idTypeSkill;
    }

    public void setIdTypeSkill(int idTypeSkill) {
        this.idTypeSkill = idTypeSkill;
    }

    public String getTypeSkillLabel() {
        return typeSkillLabel;
    }

    public void setTypeSkillLabel(String typeSkillLabel) {
        this.typeSkillLabel = typeSkillLabel;
    }

    public ArrayList<Skill> getSkills() {
        return skills;
    }

    public void setSkills(ArrayList<Skill> skills) {
        this.skills = skills;
    }

    @Override
    public String toString() {
        return this.getTypeSkillLabel();
    }

    public boolean isNotApplicable() {
        return notApplicable;
    }

    public void setNotApplicable(boolean notApplicable) {
        this.notApplicable = notApplicable;
    }

    public boolean isWithAlertSecurity() {
        return withAlertSecurity;
    }

    public void setWithAlertSecurity(boolean isWithAlertSecurity) {
        withAlertSecurity = isWithAlertSecurity;
    }


    public boolean isToCollapse() {
        return toCollapse;
    }

    public void setToCollapse(boolean toCollapse) {
        this.toCollapse = toCollapse;
    }
}
