package geca.dareliconsulting.smarttoutch.com.geca.model;

import java.io.Serializable;

/**
 * Created by Smart on 10/04/2015.
 */
public class WorkUnit implements Serializable {
    private int id;
    private int typeSkill;
    private int skill;
    private String label;
    private  float nonConformityCount;
    private  float nonConformityInternCount;
    private int score;
    private String comment;
    private String picture ;
    private boolean recovery;
    private boolean hasPicture;
    private Answer answer;
    private boolean notApplicable;
    private boolean certification;
    private String impactType="Mineur";
    private  float immediatRecoveryPrice;
    private boolean possibleRecovery=true;
    private  int recoveryTime;
    private boolean toCollapse;


    public int getTypeSkill() {
        return typeSkill;
    }

    public void setTypeSkill(int typeSkill) {
        this.typeSkill = typeSkill;
    }

    public int getSkill() {
        return skill;
    }

    public void setSkill(int skill) {
        this.skill = skill;
    }

    public WorkUnit() {
    }

    public WorkUnit(int id, int typeSkill, int skill, String label, float nonConformityCount) {
        this.score = 0;
        this.id = id;
        this.typeSkill = typeSkill;
        this.skill = skill;
        this.label = label;
        this.nonConformityCount = nonConformityCount;
    }

    public WorkUnit(int id, int typeSkill, int skill, String label, float nonConformityCount, int score, String comment, boolean recovery) {
        this.id = id;
        this.typeSkill = typeSkill;
        this.skill = skill;
        this.label = label;
        this.nonConformityCount = nonConformityCount;
        this.score = score;
        this.comment = comment;
        this.recovery = recovery;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public float getNonConformityCount() {
        return nonConformityCount;
    }

    public void setNonConformityCount(float nonConformityCount) {
        this.nonConformityCount = nonConformityCount;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return this.getLabel();
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isRecovery() {
        return recovery;
    }

    public void setRecovery(boolean recovery) {
        this.recovery = recovery;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public boolean isHasPicture() {
        return hasPicture;
    }

    public void setHasPicture(boolean hasPicture) {
        this.hasPicture = hasPicture;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    public boolean isNotApplicable() {
        return notApplicable;
    }

    public void setNotApplicable(boolean notApplicable) {
        this.notApplicable = notApplicable;
    }

    public boolean isCertification() {
        return certification;
    }

    public void setCertification(boolean certification) {
        this.certification = certification;
    }

    public String getImpactType() {
        return impactType;
    }

    public void setImpactType(String impactType) {
        this.impactType = impactType;
    }

    public float getImmediatRecoveryPrice() {
        return immediatRecoveryPrice;
    }

    public void setImmediatRecoveryPrice(float immediatRecoveryPrice) {
        this.immediatRecoveryPrice = immediatRecoveryPrice;
    }

    public boolean isPossibleRecovery() {
        return possibleRecovery;
    }

    public void setPossibleRecovery(boolean possibleRecovery) {
        this.possibleRecovery = possibleRecovery;
    }

    public int getRecoveryTime() {
        return recoveryTime;
    }

    public void setRecoveryTime(int recoveryTime) {
        this.recoveryTime = recoveryTime;
    }

    public float getNonConformityInternCount() {
        return nonConformityInternCount;
    }

    public void setNonConformityInternCount(float nonConformityInternCount) {
        this.nonConformityInternCount = nonConformityInternCount;
    }

    public boolean isToCollapse() {
        return toCollapse;
    }

    public void setToCollapse(boolean toCollapse) {
        this.toCollapse = toCollapse;
    }
}
