package geca.dareliconsulting.smarttoutch.com.geca.singleton;

import android.content.BroadcastReceiver;
import android.location.Location;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import geca.dareliconsulting.smarttoutch.com.geca.model.ActionPlan;
import geca.dareliconsulting.smarttoutch.com.geca.model.Note;
import geca.dareliconsulting.smarttoutch.com.geca.model.Profession;
import geca.dareliconsulting.smarttoutch.com.geca.model.ProfessionFamily;
import geca.dareliconsulting.smarttoutch.com.geca.model.Sector;

/**
 * Created by Smart on 28/04/2015.
 */
public class Singleton {
    private static Singleton ourInstance = new Singleton();
    public Location location;
    private ArrayList<Sector> sectors;
    private Note currentNote;
    private String noteBaseName = "";
    //public ActionPlan[] actionPlans=new ActionPlan[]{new ActionPlan(1,"Formation"), new ActionPlan(2,"Compagnonnage"),new ActionPlan(3,"Briefing"), new ActionPlan(4,"Convocation"),new ActionPlan(5,"A suivre"), new ActionPlan(6,"Autre")};
    public int maxRecovery = 3;
    public float penality = 100;
    public BroadcastReceiver mPhotoReceiverRecovery, mPhotoReceiver;
    public HashMap<Integer, Integer> checkList = new HashMap<>();

    public static Singleton getInstance() {
        return ourInstance;
    }

    private Singleton() {
        sectors = new ArrayList<Sector>();
    }

    public ArrayList<Sector> getSectors() {
        return sectors;
    }

    public void setSectors(ArrayList<Sector> sectors) {
        this.sectors = sectors;
    }

    public Note getCurrentNote() {
        return currentNote;
    }

    public void setCurrentNote(Note currentNote) {
        this.currentNote = currentNote;
    }

    public String getNoteBaseName() {
        return noteBaseName;
    }

    public void setNoteBaseName(String noteBaseName) {
        this.noteBaseName = noteBaseName;
    }

    public Profession getCurrentNoteProfession() {
        noteBaseName = "";
        if (currentNote == null) return null;
        Log.e("sectors", "getCurrentNoteProfession: " + getSectors());
        for (Sector s : Singleton.getInstance().getSectors()) {
            if (s.getId() == currentNote.getSectorId()) {
                noteBaseName += s.getSectorbabel();
                for (ProfessionFamily pf : s.getListactivityFamilies()) {
                    if (pf.getId() == currentNote.getProfessionFamilyId()) {
                        for (Profession p : pf.getProfessions()) {
                            if (p.getIdServer() == currentNote.getProfessionId()) {
                                return p;
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    public ProfessionFamily getCurrentNoteProfessionFamily() {
        if (currentNote == null) return null;
        for (Sector s : Singleton.getInstance().getSectors()) {
            if (s.getId() == currentNote.getSectorId()) {
                for (ProfessionFamily pf : s.getListactivityFamilies()) {
                    if (pf.getId() == currentNote.getProfessionFamilyId()) {
                        return pf;
                    }
                }
            }
        }
        return null;
    }


    public void test() {
        
    }

    public String[] getActionPlans() {
        List<ActionPlan> actionPlans = ActionPlan.listAll(ActionPlan.class);
        String[] actions = new String[actionPlans.size()];
        int i = 0;
        for (ActionPlan a : actionPlans) {
            actions[i] = a.getName();
            i++;
        }
        return actions;
    }

}
