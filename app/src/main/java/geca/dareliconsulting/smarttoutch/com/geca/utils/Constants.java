package geca.dareliconsulting.smarttoutch.com.geca.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

/**
 * Created by iMac1 on 06/05/15.
 */
public class Constants {

    //public static String SERVER_HOST = "http://149.202.32.228/geca_v3/web";
    //public static String SERVER_HOST = "http://gecatest.coppelis.com";
    //public static final String SERVER_HOST_VPN = "http://gecatest.coppelis.com";
    //public static String SERVER_HOST = "http://geca.circet.fr";
    //public static String SERVER_HOST_VPN = "http://srv-geca.circet.net";
    /*public static final String SERVER_HOST = "http://geca.dareli-consulting.com";
    public static final String SERVER_HOST_VPN = "http://geca.dareli-consulting.com";*/
    public static String SERVER_HOST = "http://geca-test.coppelis.com";
    public static String SERVER_HOST_VPN = "http://geca-test.coppelis.com";

    public static final String SHARED_PREFS_NAME = "GECA_SHARED_PREFS";
    public static final String DATA_FILE_NAME = "infosystem.txt";
    public static final String TAG = "GECA";
    public static final String FONT_PATH_HELVETICA = "fonts/HelveticaLTStd-ExtraComp.otf";
    public static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_RECOVERY = 2;
    public static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_CERTIF = 3;
    public static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_VISITE_TECHNIQUE = 4;
    public static final String CERTIF_PHOTO_RECEIVER = "certif_photo_receiver";
    public static final String RECOVERY_PHOTO_RECEIVER = "recovery_photo_receiver";
    public static final String VISITE_TECHNIQUE_PHOTO_RECEIVER = "visite_technique_photo_receiver";
    public static final String[] BASIC_COLORS = {"#000000", "#808080", "#C0C0C0", "#FFFFFF", "#800000", "#FF0000", "#808000", "#FFFF00", "#008000", "#00FF00", "#008080", "#00FFFF", "#000080", "#0000FF", "#800080", "#FF00FF"};


    public static String API_BASE_URL(Context context) {
        String apiBaseUrl = (isSVNEnabled(context) ? SERVER_HOST_VPN : SERVER_HOST) + "/webservices/api/";
        return apiBaseUrl;
    }

    public static String API_LOGIN_URL(Context context) {
        String apiLoginUrl = (isSVNEnabled(context) ? SERVER_HOST_VPN : SERVER_HOST) + "/webservices/login";
        return apiLoginUrl;
    }

    public static boolean isSVNEnabled(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE);
        boolean svnEnabled = preferences.getBoolean("svnEnabled", false);
        return svnEnabled;
    }

    public static void enableDisableSVN(Context context, boolean svnEnabled) {
        SharedPreferences.Editor preferencesEditor = context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE).edit();
        preferencesEditor.putBoolean("svnEnabled", svnEnabled);
        preferencesEditor.commit();
    }

}
