package geca.dareliconsulting.smarttoutch.com.geca.utils;

import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

public class ListViewUtils {

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter(); 
       
        
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
        	 View childView = listAdapter.getView(i, null, listView);
             childView.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED), MeasureSpec.makeMeasureSpec(150, MeasureSpec.UNSPECIFIED));
             totalHeight+= childView.getMeasuredHeight();
            //Log.i(" item height", childView.getMeasuredHeight()+" "+childView.getHeight());
            
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        //Log.i("list height", listAdapter.getCount()+" "+params.height/3+" ");
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
  


}
