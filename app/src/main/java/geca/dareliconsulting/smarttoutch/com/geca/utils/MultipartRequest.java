package geca.dareliconsulting.smarttoutch.com.geca.utils;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

/**
 * Created by Smart on 26/05/2015.
 */
public class MultipartRequest extends Request<String> {

    private MultipartEntity entity = new MultipartEntity();

    private final Response.Listener<String> mListener;
    private final HashMap<String, File> paramsfiles;
    private final HashMap<String, String> params;

    public MultipartRequest(String url, Response.Listener<String> listener, Response.ErrorListener errorListener, HashMap<String, File> paramsfile, HashMap<String, String> params, boolean isPicArray) {
        super(Method.POST, url, errorListener);
        mListener = listener;
        this.paramsfiles = paramsfile;
        this.params = params;
        buildMultipartEntity(isPicArray);
    }

    private void buildMultipartEntity(boolean isPicArray) {

        try {
            if (isPicArray) {
                if (paramsfiles.size() > 0) {
                    int i = 0;
                    for (String key : paramsfiles.keySet()) {
                        entity.addPart("answerPictures[" + i + "]", new FileBody(paramsfiles.get(key)));
                        i++;
                    }

                }
            } else {

                if (paramsfiles.size() > 0) {
                    for (String key : paramsfiles.keySet()) {
                        entity.addPart(key, new FileBody(paramsfiles.get(key)));
                    }
                }

            }

            if (params.size() > 0) {
                for (String key : params.keySet()) {

                    entity.addPart(key, new StringBody(params.get(key)));
                }
            }

        } catch (UnsupportedEncodingException e) {
            VolleyLog.e("UnsupportedEncodingException");
        }


    }

    @Override
    public String getBodyContentType() {
        return entity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            entity.writeTo(bos);
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }

    /**
     * copied from Android StringRequest class
     */
    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        String parsed;
        try {
            parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
        } catch (UnsupportedEncodingException e) {
            parsed = new String(response.data);
        }
        return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
    }

    @Override
    protected void deliverResponse(String response) {
        mListener.onResponse(response);
    }
}
