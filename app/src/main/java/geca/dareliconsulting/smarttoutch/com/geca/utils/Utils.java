package geca.dareliconsulting.smarttoutch.com.geca.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import geca.dareliconsulting.smarttoutch.com.geca.model.AbandonReason;
import geca.dareliconsulting.smarttoutch.com.geca.model.ActionPlan;
import geca.dareliconsulting.smarttoutch.com.geca.model.Agence;
import geca.dareliconsulting.smarttoutch.com.geca.model.BusinessManager;
import geca.dareliconsulting.smarttoutch.com.geca.model.CompanyProvider;
import geca.dareliconsulting.smarttoutch.com.geca.model.Competence;
import geca.dareliconsulting.smarttoutch.com.geca.model.Customer;
import geca.dareliconsulting.smarttoutch.com.geca.model.Fonction;
import geca.dareliconsulting.smarttoutch.com.geca.model.MyProfession;
import geca.dareliconsulting.smarttoutch.com.geca.model.OperationDetail;
import geca.dareliconsulting.smarttoutch.com.geca.model.OperationType;
import geca.dareliconsulting.smarttoutch.com.geca.model.Profession;
import geca.dareliconsulting.smarttoutch.com.geca.model.ProfessionFamily;
import geca.dareliconsulting.smarttoutch.com.geca.model.Question;
import geca.dareliconsulting.smarttoutch.com.geca.model.Sector;
import geca.dareliconsulting.smarttoutch.com.geca.model.Skill;
import geca.dareliconsulting.smarttoutch.com.geca.model.Technician;
import geca.dareliconsulting.smarttoutch.com.geca.model.Training;
import geca.dareliconsulting.smarttoutch.com.geca.model.TrainingObject;
import geca.dareliconsulting.smarttoutch.com.geca.model.TrainingType;
import geca.dareliconsulting.smarttoutch.com.geca.model.TypeSkill;
import geca.dareliconsulting.smarttoutch.com.geca.model.WorkUnit;
import geca.dareliconsulting.smarttoutch.com.geca.singleton.Singleton;

/**
 * Created by Smart on 28/04/2015.
 */
public class Utils {

    private static String DID_LOAD_TECHNICIANS = "DID_LOAD_TECHNICIANS";
    private static boolean NOT_PICTURED_LOADED = false;
    private static String NOT_PICTURED = "";
    static int currentpicture = 0;
    static ArrayList<Technician> technicians = new ArrayList<>();
    private static final int MY_SOCKET_TIMEOUT_MS = 30000; // 30 seconds

    // Load Data for add Fiche
    public static void loadDatasInObjcets(Context context) throws JSONException {
        JSONObject root = new JSONObject(Utils.readFile(Constants.DATA_FILE_NAME, context));
        JSONArray sectorsJA = root.getJSONArray("datas");
        Log.e("data", root.toString());
        ArrayList<Sector> sectors = new ArrayList<>();
        for (int i = 0; i < sectorsJA.length(); i++) {
            JSONObject sectorJO = sectorsJA.getJSONObject(i);
            String labelSector = sectorJO.optString("labelSector");
            int sectorId = sectorJO.optInt("id");
            ArrayList<ProfessionFamily> activityFamilies = new ArrayList<ProfessionFamily>();
            JSONArray activityFamiliesJA = sectorJO.optJSONArray("familyProfessions");
            if (activityFamiliesJA != null) {
                for (int j = 0; j < activityFamiliesJA.length(); j++) {
                    JSONObject activityFamilyJO = activityFamiliesJA.optJSONObject(j);
                    int afId = activityFamilyJO.optInt("id");
                    String afLabel = activityFamilyJO.optString("label");
                    ArrayList<Profession> professions = new ArrayList<>();
                    JSONArray professionsJA = activityFamilyJO.optJSONArray("listProfessions");

                    if (professionsJA != null) {
                        for (int k = 0; k < professionsJA.length(); k++) {
                            JSONObject professionJO = professionsJA.getJSONObject(k);
                            int prId = professionJO.optInt("id");
                            String prLabel = professionJO.optString("professionLabel");
                            ArrayList<TypeSkill> typeSkills = new ArrayList<>();
                            JSONArray typeSkillsJA = professionJO.optJSONArray("typesSkills");
                            if (typeSkillsJA != null) {
                                for (int l = 0; l < typeSkillsJA.length(); l++) {
                                    JSONObject typeSkillJO = typeSkillsJA.getJSONObject(l);
                                    int typeSkillId = typeSkillJO.optInt("idTypeSkill");
                                    String typeSkillLabel = typeSkillJO.optString("typeSkillLabel");
                                    ArrayList<Skill> skills = new ArrayList<>();
                                    JSONArray skillsJA = typeSkillJO.optJSONArray("skills");
                                    if (skillsJA != null) {
                                        for (int m = 0; m < skillsJA.length(); m++) {
                                            JSONObject skillJO = skillsJA.getJSONObject(m);
                                            int idSkill = skillJO.optInt("idSkill");
                                            String skillLabel = skillJO.optString("skillLabel");
                                            ArrayList<Question> questions = new ArrayList<>();
                                            JSONArray questionsJA = skillJO.optJSONArray("listQuestions");
                                            if (questionsJA != null) {
                                                for (int n = 0; n < questionsJA.length(); n++) {
                                                    JSONObject questionJO = questionsJA.optJSONObject(n);
                                                    int idQuestion = questionJO.optInt("id");
                                                    int typeSkill = questionJO.optInt("typeSkill");
                                                    int skill = questionJO.optInt("skill");
                                                    String questionText = questionJO.optString("questionText");
                                                    boolean questionIsTrue = questionJO.optBoolean("questionIsTrue");
                                                    String comment = questionJO.optString("comment");
                                                    Question question = new Question(idQuestion, typeSkill, skill, questionText, questionIsTrue, comment);
                                                    questions.add(question);
                                                }
                                            }
                                            ArrayList<WorkUnit> workunits = new ArrayList<>();
                                            JSONArray workunitsJA = skillJO.optJSONArray("listUnitWorks");
                                            if (workunitsJA != null) {
                                                for (int o = 0; o < workunitsJA.length(); o++) {
                                                    JSONObject workunitJO = workunitsJA.optJSONObject(o);
                                                    int idWorkunit = workunitJO.optInt("id");
                                                    int typeSkill = workunitJO.optInt("typeSkill");
                                                    int skill = workunitJO.optInt("skill");
                                                    String workUnitLabel = workunitJO.optString("workUnitLabel");
                                                    boolean certification = true, possibleRecovery = true;
                                                    if (workunitJO.has("certification")) {
                                                        certification = (workunitJO.optString("certification").equalsIgnoreCase("oui"));
                                                    }
                                                    if (workunitJO.has("possibleRecovery")) {
                                                        possibleRecovery = (workunitJO.optString("possibleRecovery").equalsIgnoreCase("oui"));
                                                    }
                                                    Long workUnitCostUniformity = workunitJO.optLong("workUnitCostUniformity");
                                                    Long workUnitInternCostUniformity = workunitJO.optLong("internalCost");
                                                    WorkUnit workunit = new WorkUnit(idWorkunit, typeSkill, skill, workUnitLabel, workUnitCostUniformity);
                                                    if (workunitJO.has("impactType")) {
                                                        workunit.setImpactType(workunitJO.optString("impactType"));
                                                    }
                                                    if (workunitJO.has("recoveryNowCost")) {
                                                        workunit.setImmediatRecoveryPrice(workunitJO.optLong("recoveryNowCost"));
                                                    }
                                                    if (workunitJO.has("recoveryDelay")) {
                                                        workunit.setRecoveryTime(workunitJO.optInt("recoveryDelay"));
                                                    }
                                                    workunit.setCertification(certification);
                                                    workunit.setPossibleRecovery(possibleRecovery);
                                                    workunit.setNonConformityInternCount(workUnitInternCostUniformity);
                                                    workunits.add(workunit);
                                                }
                                            }
                                            Skill skill = new Skill(idSkill, skillLabel, questions, workunits);
                                            if (Singleton.getInstance().getCurrentNote() != null) {
                                                for (TypeSkill typeSkill : Singleton.getInstance().getCurrentNote().getTypeSkills()) {
                                                    for (Skill skill1 : typeSkill.getSkills()) {
                                                        if(skill.getId() == skill1.getId()){
                                                            skill.setScore(skill1.getScore());
                                                        }
                                                    }
                                                }
                                            }

                                            skills.add(skill);
                                        }
                                        if (skills.size() > 0) {
                                            TypeSkill typeSkill = new TypeSkill(typeSkillId, typeSkillLabel, skills);
                                            typeSkills.add(typeSkill);
                                        }
                                    }
                                }
                            }
                            if (typeSkills.size() > 0) {
                                Profession profession = new Profession(prId, prLabel, typeSkills);
                                professions.add(profession);
                            }
                        }
                    }
                    if (professions.size() > 0) {
                        ProfessionFamily activityFamily = new ProfessionFamily(afId, afLabel, professions);
                        activityFamilies.add(activityFamily);
                    }
                }
            }
            if (activityFamilies.size() > 0) {
                Sector sector = new Sector(sectorId, labelSector, activityFamilies);
                sectors.add(sector);
            }
        }
        Singleton.getInstance().setSectors(sectors);
        JSONArray aAgences = root.getJSONArray("agences");
        Agence.deleteAll(Agence.class);
        for (int ia = 0; ia < aAgences.length(); ia++) {
            (new Agence(aAgences.getJSONObject(ia).getInt("id"), aAgences.getJSONObject(ia).getString("label"))).save();
        }

        JSONArray acampanys = root.getJSONArray("companiesProviders");
        CompanyProvider.deleteAll(CompanyProvider.class);
        for (int icp = 0; icp < acampanys.length(); icp++) {
            (new CompanyProvider(acampanys.getJSONObject(icp).getInt("id"), acampanys.getJSONObject(icp).getString("raisonSociale"))).save();
        }

        JSONArray afunctions = root.getJSONArray("functions");
        Fonction.deleteAll(Fonction.class);
        for (int ifs = 0; ifs < afunctions.length(); ifs++) {
            (new Fonction(afunctions.getJSONObject(ifs).getInt("fonctionId"), afunctions.getJSONObject(ifs).getString("fonctionLabel"))).save();
        }

        JSONArray aresponsableaffaires = root.getJSONArray("responsablesAffaires");
        BusinessManager.deleteAll(BusinessManager.class);
        for (int irs = 0; irs < aresponsableaffaires.length(); irs++) {
            (new BusinessManager(aresponsableaffaires.getJSONObject(irs).getInt("id"), aresponsableaffaires.getJSONObject(irs).getString("Name"))).save();
        }

        JSONArray abandonesList = root.getJSONArray("abandonesList");
        AbandonReason.deleteAll(AbandonReason.class);
        for (int ib = 0; ib < abandonesList.length(); ib++) {
            (new AbandonReason(abandonesList.getJSONObject(ib).optInt("id"), abandonesList.getJSONObject(ib).optString("label"))).save();
        }

        JSONArray trainingsList = root.getJSONArray("trainingsList");
        Training.deleteAll(Training.class);
        for (int irf = 0; irf < trainingsList.length(); irf++) {
            Training training = new Training(trainingsList.getJSONObject(irf).optInt("id"), trainingsList.getJSONObject(irf).optString("sujet"));
            training.save();
        }

        JSONArray skills = root.getJSONArray("skills");
        Competence.deleteAll(Competence.class);
        for (int m = 0; m < skills.length(); m++) {
            (new Competence(skills.getJSONObject(m).optInt("id"), skills.getJSONObject(m).optString("skillLabel"))).save();
        }

        JSONArray aCustomers = root.getJSONArray("customers");
        Customer.deleteAll(Customer.class);
        for (int ic = 0; ic < aCustomers.length(); ic++) {
            (new Customer(aCustomers.getJSONObject(ic).getInt("id"), aCustomers.getJSONObject(ic).getString("customerName"), aCustomers.getJSONObject(ic).getString("customerSocial"))).save();
        }
        JSONArray aProfessions = root.getJSONArray("professions");
        MyProfession.deleteAll(MyProfession.class);
        for (int ic = 0; ic < aProfessions.length(); ic++) {
            (new MyProfession(aProfessions.getJSONObject(ic).getInt("id"), aProfessions.getJSONObject(ic).getString("label"))).save();
        }
        if (root.has("operations")) {
            JSONArray operationTypes = root.getJSONArray("operations");
            OperationType.deleteAll(OperationType.class);
            for (int m = 0; m < operationTypes.length(); m++) {
                (new OperationType(operationTypes.getJSONObject(m).optString("type"), operationTypes.getJSONObject(m).optString("amount"))).save();
            }
        }

        if (root.has("operationsDetails")) {
            JSONArray operationDetails = root.getJSONArray("operationsDetails");
            OperationDetail.deleteAll(OperationDetail.class);
            for (int m = 0; m < operationDetails.length(); m++) {
                (new OperationDetail(operationDetails.getJSONObject(m).optInt("id"), operationDetails.getJSONObject(m).optString("description"))).save();
            }
        }
        if (root.has("actionPlans")) {
            JSONArray actionPlans = root.getJSONArray("actionPlans");
            ActionPlan.deleteAll(ActionPlan.class);
            for (int m = 0; m < actionPlans.length(); m++) {
                (new ActionPlan(actionPlans.getJSONObject(m).optInt("id"), actionPlans.getJSONObject(m).optString("name"))).save();
            }
        }
        if (root.has("trainingTypes")) {
            JSONArray trainingTypes = root.getJSONArray("trainingTypes");
            TrainingType.deleteAll(TrainingType.class);
            for (int m = 0; m < trainingTypes.length(); m++) {
                (new TrainingType(trainingTypes.getJSONObject(m).optInt("id"), trainingTypes.getJSONObject(m).optString("name"))).save();
            }
        }
        if (root.has("trainingObjects")) {
            JSONArray trainingObjects = root.getJSONArray("trainingObjects");
            TrainingObject.deleteAll(TrainingObject.class);
            for (int m = 0; m < trainingObjects.length(); m++) {
                (new TrainingObject(trainingObjects.getJSONObject(m).optInt("id"), trainingObjects.getJSONObject(m).optString("name"))).save();
            }
        }
    }

    static void LoadPicturetechnicians(final Context context, String json, final OnTechnicianLoadedListener onTechnicianLoadedListener) throws JSONException {
        Context mcontext = context;
        JSONArray techniciansJA = new JSONArray(json);
        technicians = new ArrayList<>();
        for (int i = 0; i < techniciansJA.length(); i++) {
            JSONObject technicianJO = techniciansJA.getJSONObject(i);
            String imageUrl = technicianJO.optString("picture");
            int id = technicianJO.optInt("id");
            String name = technicianJO.optString("name");
            String surname = technicianJO.optString("surname");
            String email = technicianJO.optString("email");
            String dateStart = technicianJO.optString("dateStart");
            String dateEnd = technicianJO.optString("dateEnd");
            int fonction_id = technicianJO.optInt("fonction_id");
            String phone = technicianJO.optString("phone");
            String function = technicianJO.optString("fonction");
            String socialSecurityNumber = technicianJO.optString("socialSecurityNumber");
            String registrationNumber = technicianJO.optString("matricule");
            int companyId = technicianJO.optInt("company_id");
            int agenceId = technicianJO.optInt("agence_id");
            int businessResponsableid = technicianJO.optInt("busnissResponsable");
            String formation = technicianJO.optString("formation");
            String experience = technicianJO.optString("experience");
            Technician technician;
            List<Technician> techs = Technician.find(Technician.class, "server_id = ?", "" + id);
            if (techs != null && techs.size() > 0) {
                technician = techs.get(0);
            } else {
                technician = new Technician();
            }
            technician.setEndDate(dateEnd);
            technician.setStartDate(dateStart);
            technician.setName(name);
            technician.setSurName(surname);
            technician.setPhone(phone);
            technician.setEmail(email);
            technician.setBusinessResponsibleId(businessResponsableid);
            technician.setEdit(false);
            technician.setFunction(function);
            technician.setFunctionId(fonction_id);
            technician.setLocalImage(imageUrl);
            technician.setServerId(id);
            technician.setRegistrationNumber(registrationNumber);
            technician.setCompanyId(companyId);
            technician.setAgencyId(agenceId);
            technician.setSocialSecurityNumber(socialSecurityNumber);
            technician.setFormation(formation);
            technician.setExperience(experience);

            technicians.add(technician);
        }
        if (technicians.size() > 0) {
            ImageLoader imageLoader = ImageLoader.getInstance();
            imageLoader.init(ImageLoaderConfiguration.createDefault(mcontext));
            loadNextpicture(imageLoader, context, onTechnicianLoadedListener);
        } else {
            onTechnicianLoadedListener.technicianLoadingCompleted(true);
            SharedPreferences.Editor editor = context.getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE).edit();
            editor.putBoolean(DID_LOAD_TECHNICIANS, true);
        }


    }

    static boolean idExist(ArrayList<Integer> serverIds, int id) {
        for (Integer i : serverIds)
            if (i == id)
                return true;
        return false;
    }

    static void loadNextpicture(final ImageLoader imageLoader, final Context context, final OnTechnicianLoadedListener onTechnicianLoadedListener) {
        if (currentpicture < technicians.size()) {
            final Technician technician = technicians.get(currentpicture);
            if (technician.getLocalImage().contains("not-pictured.jpg") && NOT_PICTURED_LOADED) {
                currentpicture++;
                technician.setLocalImage(NOT_PICTURED);
                loadNextpicture(imageLoader, context, onTechnicianLoadedListener);
            } else {
                imageLoader.loadImage(technician.getLocalImage(), new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        // Do whatever you want with Bitmap
                        if (loadedImage != null) {
                            currentpicture++;
                            if (technician.getLocalImage().contains("not-pictured.jpg")) {
                                NOT_PICTURED = saveBitmapToFile(loadedImage);
                                NOT_PICTURED_LOADED = true;
                            }
                            technician.setLocalImage(saveBitmapToFile(loadedImage));
                            loadNextpicture(imageLoader, context, onTechnicianLoadedListener);
                        } else {
                            technician.setLocalImage(null);
                            currentpicture++;
                            loadNextpicture(imageLoader, context, onTechnicianLoadedListener);
                        }
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        technician.setLocalImage(null);
                        currentpicture++;
                        loadNextpicture(imageLoader, context, onTechnicianLoadedListener);
                    }
                });
            }
            /*imageLoader.loadImage(technician.getLocalImage(), new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    // Do whatever you want with Bitmap
                    if (loadedImage != null) {
                        currentpicture++;
                        technician.setLocalImage(saveBitmapToFile(loadedImage));
                        loadNextpicture(imageLoader, context, onTechnicianLoadedListener);
                    } else {
                        technician.setLocalImage(null);
                        currentpicture++;
                        loadNextpicture(imageLoader, context, onTechnicianLoadedListener);
                    }
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    technician.setLocalImage(null);
                    currentpicture++;
                    loadNextpicture(imageLoader, context, onTechnicianLoadedListener);
                }
            });*/
        } else {
            for (int i = 0; i < technicians.size(); i++) {
                technicians.get(i).save();
            }
            if (currentpicture == technicians.size()) {
                currentpicture = 0;
                onTechnicianLoadedListener.technicianLoadingCompleted(true);
            }
            SharedPreferences.Editor editor = context.getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE).edit();
            editor.putBoolean(DID_LOAD_TECHNICIANS, true);
        }
    }


    public static Typeface getHelveticaFont(Context context) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), Constants.FONT_PATH_HELVETICA);
        return typeface;
    }

    public static void loadDataSytsem(final Context context, final OnDataSystemLoadedListener onDataSystemLoadedListener) {

        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.API_BASE_URL(context) + "infosystem",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            if (Utils.checkFileInternalAndroid(Constants.DATA_FILE_NAME, context)) {
                                Utils.deleteInternalStorageByName(Constants.DATA_FILE_NAME, context);
                            }
                            FileOutputStream fos = context.openFileOutput(Constants.DATA_FILE_NAME, Context.MODE_PRIVATE);
                            fos.write(response.getBytes());
                            fos.close();
                            Utils.loadDatasInObjcets(context);
                            onDataSystemLoadedListener.dataSystemLoadingCompleted(true);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                            onDataSystemLoadedListener.dataSystemLoadingCompleted(false);

                        } catch (IOException e) {
                            e.printStackTrace();
                            onDataSystemLoadedListener.dataSystemLoadingCompleted(false);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            onDataSystemLoadedListener.dataSystemLoadingCompleted(false);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        onDataSystemLoadedListener.dataSystemLoadingCompleted(false);
                    }
                }
        );
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    public static void loadTechnicians(final Context context, final OnTechnicianLoadedListener onTechnicianLoadedListener) {

        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.API_BASE_URL(context) + "list/technicians",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("technician response", response);
                        try {
                            Utils.LoadPicturetechnicians(context, response, onTechnicianLoadedListener);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            onTechnicianLoadedListener.technicianLoadingCompleted(false);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("technicians error", error.toString());
                        onTechnicianLoadedListener.technicianLoadingCompleted(false);
                    }
                }
        );
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    /*
       Method  for read the content of the File internel by name
     */
    public static String readFile(String filename, Context context) {
        try {
            FileInputStream fis = context.openFileInput(filename);
            InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
            BufferedReader bufferedReader = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line).append("\n");
            }
            return sb.toString();
        } catch (FileNotFoundException e) {
            return "";
        } catch (UnsupportedEncodingException e) {
            return "";
        } catch (IOException e) {
            return "";
        }
    }

    /*
    Delete the File in internel by name
    @params  File_Name string
     */
    public static boolean deleteInternalStorageByName(String filename, Context context) {
        File dir = context.getFilesDir();
        File file = new File(dir, filename);
        return file.delete();
    }

    /**
     * File CheckFileIn INternal Android
     */
    public static boolean checkFileInternalAndroid(String fname, Context context) {

        File file = context.getFileStreamPath(fname);
        return file.exists();

    }

    public interface OnDataSystemLoadedListener {
        void dataSystemLoadingCompleted(boolean success);
    }

    public interface OnTechnicianLoadedListener {
        void technicianLoadingCompleted(boolean success);
    }

    /////
    public static void enableDisableView(View view, boolean enabled) {
        view.setEnabled(enabled);

        if (view instanceof ViewGroup) {
            ViewGroup group = (ViewGroup) view;

            for (int idx = 0; idx < group.getChildCount(); idx++) {
                enableDisableView(group.getChildAt(idx), enabled);
            }
        }
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    public static boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public static String saveBitmapToFile(Bitmap bitmap) {
        FileOutputStream out = null;
        String filename = null;
        try {
            File f = new File(Environment.getExternalStorageDirectory(), "GECA"
            );
            if (!f.exists()) {
                f.mkdirs();
            }
            filename = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GECA/" + UUID.randomUUID().toString() + ".jpg";
            out = new FileOutputStream(filename);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 20, out);
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return filename;
    }

    public static String saveBitmapToFile(String path) {
        File imgFile = new File(path);
        if (imgFile.exists()) {
            Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            return Utils.saveBitmapToFile(bitmap);
        }
        return null;
    }

    public static Bitmap loadFullBitmapFromFile(String filename) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(filename, options);
        return bitmap;
    }

    public static Bitmap loadSmallBitmapFromFile(String filename) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filename, options);
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, 120, 80);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(filename, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }


    public static boolean allTechnicianIsSyn() {
        boolean isallsyn = true;
        List<Technician> technicians = Technician.listAll(Technician.class);
        for (int i = 0; i < technicians.size(); i++) {
            if (technicians.get(i).getServerId() == 0) {
                isallsyn = false;
            }
        }

        return isallsyn;
    }


    public static void refreshDatasystem(Context context) throws JSONException {
        JSONObject root = new JSONObject(Utils.readFile(Constants.DATA_FILE_NAME, context));
        JSONArray sectorsJA = root.getJSONArray("datas");
        ArrayList<Sector> sectors = new ArrayList<>();
        for (int i = 0; i < sectorsJA.length(); i++) {
            JSONObject sectorJO = sectorsJA.getJSONObject(i);
            String labelSector = sectorJO.optString("labelSector");
            int sectorId = sectorJO.optInt("id");
            ArrayList<ProfessionFamily> activityFamilies = new ArrayList<ProfessionFamily>();
            JSONArray activityFamiliesJA = sectorJO.optJSONArray("familyProfessions");
            if (activityFamiliesJA != null) {
                for (int j = 0; j < activityFamiliesJA.length(); j++) {
                    JSONObject activityFamilyJO = activityFamiliesJA.optJSONObject(j);
                    int afId = activityFamilyJO.optInt("id");
                    String afLabel = activityFamilyJO.optString("label");
                    ArrayList<Profession> professions = new ArrayList<>();
                    JSONArray professionsJA = activityFamilyJO.optJSONArray("listProfessions");

                    if (professionsJA != null) {
                        for (int k = 0; k < professionsJA.length(); k++) {
                            JSONObject professionJO = professionsJA.getJSONObject(k);
                            int prId = professionJO.optInt("id");
                            String prLabel = professionJO.optString("professionLabel");
                            ArrayList<TypeSkill> typeSkills = new ArrayList<>();
                            JSONArray typeSkillsJA = professionJO.optJSONArray("typesSkills");
                            if (typeSkillsJA != null) {
                                for (int l = 0; l < typeSkillsJA.length(); l++) {
                                    JSONObject typeSkillJO = typeSkillsJA.getJSONObject(l);
                                    int typeSkillId = typeSkillJO.optInt("idTypeSkill");
                                    String typeSkillLabel = typeSkillJO.optString("typeSkillLabel");
                                    ArrayList<Skill> skills = new ArrayList<>();
                                    JSONArray skillsJA = typeSkillJO.optJSONArray("skills");
                                    if (skillsJA != null) {
                                        for (int m = 0; m < skillsJA.length(); m++) {
                                            JSONObject skillJO = skillsJA.getJSONObject(m);
                                            int idSkill = skillJO.optInt("idSkill");
                                            String skillLabel = skillJO.optString("skillLabel");
                                            ArrayList<Question> questions = new ArrayList<>();
                                            JSONArray questionsJA = skillJO.optJSONArray("listQuestions");
                                            if (questionsJA != null) {
                                                for (int n = 0; n < questionsJA.length(); n++) {
                                                    JSONObject questionJO = questionsJA.optJSONObject(n);
                                                    int idQuestion = questionJO.optInt("id");
                                                    int typeSkill = questionJO.optInt("typeSkill");
                                                    int skill = questionJO.optInt("skill");
                                                    String questionText = questionJO.optString("questionText");
                                                    boolean questionIsTrue = questionJO.optBoolean("questionIsTrue");
                                                    String comment = questionJO.optString("comment");
                                                    Question question = new Question(idQuestion, typeSkill, skill, questionText, questionIsTrue, comment);
                                                    questions.add(question);
                                                }
                                            }
                                            ArrayList<WorkUnit> workunits = new ArrayList<>();
                                            JSONArray workunitsJA = skillJO.optJSONArray("listUnitWorks");
                                            if (workunitsJA != null) {
                                                for (int o = 0; o < workunitsJA.length(); o++) {
                                                    JSONObject workunitJO = workunitsJA.optJSONObject(o);
                                                    int idWorkunit = workunitJO.optInt("id");
                                                    int typeSkill = workunitJO.optInt("typeSkill");
                                                    int skill = workunitJO.optInt("skill");
                                                    String workUnitLabel = workunitJO.optString("workUnitLabel");
                                                    boolean certification = true, possibleRecovery = true;
                                                    if (workunitJO.has("certification")) {
                                                        certification = (workunitJO.optString("certification").equalsIgnoreCase("oui"));
                                                    }
                                                    if (workunitJO.has("possibleRecovery")) {
                                                        possibleRecovery = (workunitJO.optString("possibleRecovery").equalsIgnoreCase("oui"));
                                                    }
                                                    Long workUnitCostUniformity = workunitJO.optLong("workUnitCostUniformity");
                                                    Long workUnitInternCostUniformity = workunitJO.optLong("internalCost");
                                                    WorkUnit workunit = new WorkUnit(idWorkunit, typeSkill, skill, workUnitLabel, workUnitCostUniformity);
                                                    if (workunitJO.has("impactType")) {
                                                        workunit.setImpactType(workunitJO.optString("impactType"));
                                                    }
                                                    if (workunitJO.has("recoveryNowCost")) {
                                                        workunit.setImmediatRecoveryPrice(workunitJO.optLong("recoveryNowCost"));
                                                    }
                                                    if (workunitJO.has("recoveryDelay")) {
                                                        workunit.setRecoveryTime(workunitJO.optInt("recoveryDelay"));
                                                    }
                                                    workunit.setCertification(certification);
                                                    workunit.setPossibleRecovery(possibleRecovery);
                                                    workunit.setNonConformityInternCount(workUnitInternCostUniformity);
                                                    workunits.add(workunit);
                                                }
                                            }
                                            Skill skill = new Skill(idSkill, skillLabel, questions, workunits);
                                            skills.add(skill);
                                        }
                                        if (skills.size() > 0) {
                                            TypeSkill typeSkill = new TypeSkill(typeSkillId, typeSkillLabel, skills);
                                            typeSkills.add(typeSkill);
                                        }
                                    }
                                }
                            }
                            if (typeSkills.size() > 0) {
                                Profession profession = new Profession(prId, prLabel, typeSkills);
                                professions.add(profession);
                            }
                        }
                    }
                    if (professions.size() > 0) {
                        ProfessionFamily activityFamily = new ProfessionFamily(afId, afLabel, professions);
                        activityFamilies.add(activityFamily);
                    }

                }
            }
            if (activityFamilies.size() > 0) {
                Sector sector = new Sector(sectorId, labelSector, activityFamilies);
                sectors.add(sector);
            }
        }
        Singleton.getInstance().setSectors(sectors);
    }

}
