package me.nereo.multi_image_selector;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.File;
import java.util.ArrayList;

public class MultiImageSelectorActivity extends AppCompatActivity implements MultiImageSelectorFragment.Callback {

    public static final String EXTRA_SELECT_COUNT = "max_select_count";

    public static final String EXTRA_SELECT_MODE = "select_count_mode";

    public static final String EXTRA_SHOW_CAMERA = "show_camera";

    public static final String EXTRA_RESULT = "select_result";

    public static final String EXTRA_DEFAULT_SELECTED_LIST = "default_list";

    public static final int MODE_SINGLE = 0;

    public static final int MODE_MULTI = 1;

    public static final int MODE_MULTI_STRICT = 2;


    private ArrayList<String> resultList = new ArrayList<>();
    private Button mSubmitButton;
    private int mDefaultCount;
    private int mode;
    private boolean isShow;
    private static final int MY_PERMISSIONS_REQUEST_PHOTOS = 44;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_default);

        Intent intent = getIntent();
        mDefaultCount = intent.getIntExtra(EXTRA_SELECT_COUNT, 9);
        mode = intent.getIntExtra(EXTRA_SELECT_MODE, MODE_MULTI);
        isShow = intent.getBooleanExtra(EXTRA_SHOW_CAMERA, true);
        if (mode == MODE_MULTI && intent.hasExtra(EXTRA_DEFAULT_SELECTED_LIST)) {
            resultList = intent.getStringArrayListExtra(EXTRA_DEFAULT_SELECTED_LIST);
        }

        ArrayList<String> permissions = new ArrayList<>();// String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
        boolean hasCameraPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
        boolean hasReadStoragePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
        if (hasCameraPermission && hasReadStoragePermission) {
            addMultiImageSelectorFragment();
        } else {
            if (!hasCameraPermission) {
                permissions.add(Manifest.permission.CAMERA);
            }
            if (!hasReadStoragePermission) {
                permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            ActivityCompat.requestPermissions(this, permissions.toArray(new String[permissions.size()]), MY_PERMISSIONS_REQUEST_PHOTOS);
        }

        findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("here","cancel");
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        mSubmitButton = (Button) findViewById(R.id.commit);
        if (resultList == null || resultList.size() <= 0) {
            mSubmitButton.setText("En cours");
            mSubmitButton.setEnabled(false);
        } else {
            mSubmitButton.setText("En cours(" + resultList.size() + "/" + mDefaultCount + ")");
            mSubmitButton.setEnabled(true);
        }
        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (resultList != null && resultList.size() > 0) {
                    Intent data = new Intent();
                    data.putStringArrayListExtra(EXTRA_RESULT, resultList);
                    setResult(RESULT_OK, data);
                    finish();
                }
            }
        });
    }

    private void addMultiImageSelectorFragment() {
        Bundle bundle = new Bundle();
        bundle.putInt(MultiImageSelectorFragment.EXTRA_SELECT_COUNT, mDefaultCount);
        bundle.putInt(MultiImageSelectorFragment.EXTRA_SELECT_MODE, mode);
        bundle.putBoolean(MultiImageSelectorFragment.EXTRA_SHOW_CAMERA, isShow);
        bundle.putStringArrayList(MultiImageSelectorFragment.EXTRA_DEFAULT_SELECTED_LIST, resultList);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.image_grid, Fragment.instantiate(this, MultiImageSelectorFragment.class.getName(), bundle))
                .commitAllowingStateLoss();
    }

    @Override
    public void onSingleImageSelected(String path) {
        Intent data = new Intent();
        resultList.add(path);
        data.putStringArrayListExtra(EXTRA_RESULT, resultList);
        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    public void onImageSelected(String path) {
        if (!resultList.contains(path)) {
            resultList.add(path);
        }
        Log.e("Result size", resultList.size() + "");
        if (resultList.size() > 0) {
            if (mode == MODE_MULTI || mode == MODE_MULTI_STRICT) {
                if (resultList.size() == mDefaultCount) {
                    mSubmitButton.setText("Terminé(" + resultList.size() + "/" + mDefaultCount + ")");
                    mSubmitButton.setEnabled(true);
                }else if (resultList.size() < mDefaultCount) {
                    mSubmitButton.setText("En cours(" + resultList.size() + "/" + mDefaultCount + ")");
                    if(mode == MODE_MULTI_STRICT)
                        mSubmitButton.setEnabled(false);
                    else
                        mSubmitButton.setEnabled(true);
                }
            } else if (!mSubmitButton.isEnabled()) {
                mSubmitButton.setText("Terminé(" + resultList.size() + "/" + mDefaultCount + ")");
                mSubmitButton.setEnabled(true);

            }
        }
    }

    @Override
    public void onImageUnselected(String path) {
        if (resultList.contains(path)) {
            resultList.remove(path);
            mSubmitButton.setText("En cours(" + resultList.size() + "/" + mDefaultCount + ")");
            mSubmitButton.setEnabled(false);
        } else {
            mSubmitButton.setText("En cours(" + resultList.size() + "/" + mDefaultCount + ")");
        }
        if (resultList.size() == 0) {
            mSubmitButton.setText("En cours");
            mSubmitButton.setEnabled(false);
        }
    }

    @Override
    public void onCameraShot(File imageFile) {
        if (imageFile != null) {
            resultList.add(imageFile.getAbsolutePath());
            if (mode == MODE_MULTI || mode == MODE_MULTI_STRICT) {
                if (resultList.size() == mDefaultCount || resultList.size() == mDefaultCount+1 ){
                    Intent data = new Intent();
                    data.putStringArrayListExtra(EXTRA_RESULT, resultList);
                    setResult(RESULT_OK, data);
                    finish();
                }else{
                    mSubmitButton.setText("En cours(" + resultList.size() + "/" + mDefaultCount + ")");
                    if(mode == MODE_MULTI)
                        mSubmitButton.setEnabled(true);
                }
            } else if (!mSubmitButton.isEnabled()) {
                Intent data = new Intent();
                data.putStringArrayListExtra(EXTRA_RESULT, resultList);
                setResult(RESULT_OK, data);
                finish();

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_PHOTOS) {
            try {
                boolean hasCameraPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
                boolean hasReadStoragePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
                Log.d("hasCameraPermission", "-- " + String.valueOf(hasCameraPermission));
                Log.d("ReadStoragePermission", "-- " + String.valueOf(hasReadStoragePermission));
                if (hasCameraPermission && hasReadStoragePermission) {
                    addMultiImageSelectorFragment();
                } else {
                    finish();
                }
            } catch (Exception e) {
                e.printStackTrace();
                finish();
            }
        }
    }
}
